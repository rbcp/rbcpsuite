/////////////////////////////////////////////////////////////////////////////////////////
//
// COPYRIGHT (C) 2012 Evans & Sutherland Computer Corporation
// All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////////////
//
// Class Description: Static DS API class.
//
////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;


namespace ES.Ds.Common
{

    public delegate void DsAPIConnectCallBack(bool bAPIStatus);
    public delegate void DsAddClassCommandCallBack(short nObjID, short nCmdIndex);
    public delegate void DsAddObjectPostCreateCallBack(short nObjID);
    public delegate void DsAddObjectPreDeleteCallBack(short nObjID);
    public delegate void DsAddObjectPostDeleteCallBack(short nObjID);
    public delegate void DsAddClassLockedCallBack(short nClassID);
    public delegate void DsAddEnumLockedCallBack(short nEnumID);
    public delegate void DsAddObjectAttrUpdateCallBack(short nObjID, short nAttrIndex);
    public delegate void DsAddClassAttrUpdateCallBack(short nObjID, short nAttrIndex, short nElemIndex);
    public delegate void DsMessageReceivedCallBack(IntPtr messagesource, DsMesgLevel nMesgLevel, DsMesgCategory nMesgCategory, IntPtr message);
    public delegate void DsAddObjectAddChildCallBack(short nObjID, short nChildID);
    public delegate void DsAddObjectRemoveChildCallBack(short nObjID, short nChildID);
    public delegate void DsAddPreClearTablesCallBack(short nClearTblType);
    public delegate void DsAddPreInitOrSyncCallBack(short nClearTblType);

    [SuppressUnmanagedCodeSecurity()]
    public static class DsAPI
    {

        const string sPath = "";

        public const short OM_CLEAR_TBLS_INIT = 0;
        public const short OM_CLEAR_TBLS_SYNC = 1;

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsConnect([MarshalAs(UnmanagedType.LPWStr)] string sPluginName, DsAPIConnectCallBack DsAPIFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsDisconnect();

        /*
         * Classes
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsCreateClass([MarshalAs(UnmanagedType.LPWStr)] string className, DsClassType classType, out short classID, [MarshalAs(UnmanagedType.LPWStr)] string shaderName);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassAttr(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string attrName, DsAttr attrType, DsAttrFlag attrFlags, out short attrIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassEnumAttr(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string attrName, short enumID, DsAttrFlag attrFlags, out short attrIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassAttrArray(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string attrName, DsAttr elemType, DsAttrFlag attrFlags, out short attrIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsSetClassAttrDefMinMax(short nClassID, short attrIndex, DsAttrDef attrDef, DsAttribute attr);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassCommand(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string commandName, out short commandIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsLockClass(short nClassID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassCommandCallBack(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sCmdName, DsAddClassCommandCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveClassCommandCallBack(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sCmdName, DsAddClassCommandCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassLockedCallBack(DsAddClassLockedCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveClassLockedCallBack(DsAddClassLockedCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetClassName(short nClassID, StringBuilder szRetName, int nNameMaxSize);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetClassID([MarshalAs(UnmanagedType.LPWStr)] string sClassName, out short nClassID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassNumAttrs(short nClassID, out short nNumAttrs);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrType(short nClassID, short nAttrIndex, out DsAttr eType);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrArrayType(short nClassID, short nAttrIndex, out DsAttr eArrayType);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrQualifierBits(short nClassID, short nAttrIndex, out DsAttrFlag attrFlags);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrFlags(short nClassID, short nAttrIndex, out DsAttrFlag attrFlags);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetAttrAllowsController(DsAttr eAttrType, out DsAttrAllowsControllers eAllows);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrReadOnly(short nClassID, short nAttrIndex, out byte byIsReadOnly);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrDefault(short nClassID, short nAttrIndex, IntPtr attrArg);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrMax(short nClassID, short nAttrIndex, IntPtr attrArg);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrMin(short nClassID, short nAttrIndex, IntPtr attrArg);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetClassAttrName(short nClassID, short nAttrIndex,
            StringBuilder sAttrName, int nNameMaxSize);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrIndex(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sAttrName, out short nRetIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassCommandIndex(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sCmdName, out short nRetIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassNumCommands(short nClassID, out short nNumCmds);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetClassCommandName(short nClassID, short nCmdIndex,
            StringBuilder sCmdName, int nNameMaxSize);

        /*
         * Enums
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddEnumLockedCallBack(DsAddEnumLockedCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveEnumLockedCallBack(DsAddEnumLockedCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetEnumID([MarshalAs(UnmanagedType.LPWStr)] string sEnumName, out short nEnumID);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetEnumName(short nEnumID, StringBuilder szRetName, int nNameMaxSize);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetEnumNumItems(short nEnumID, out short nNumItems);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetEnumItemName(short nEnumID, short nItemIndex, StringBuilder szItemName, int nNameMaxSize);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrEnumID(short nClassID, short nAttrIndex, out short nEnumID);

        /*
         * General
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsExecuteObjectCommand(short nObjectID, short nCmdIndex);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsSendStringCommand([MarshalAs(UnmanagedType.LPWStr)] string sCommand);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsSendStringCommandBuffer([MarshalAs(UnmanagedType.LPWStr)] string sCommand);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsSendScriptCommands([MarshalAs(UnmanagedType.LPWStr)] string sScriptName, [MarshalAs(UnmanagedType.LPWStr)] string sCommands, float fTimecodeOffset);

        /*
         * Objects
         */

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsCreateObject([MarshalAs(UnmanagedType.LPWStr)] string sObjectName, short nClassID, out short nRetObjectID, DsObjectType eObjectType = DsObjectType.Undefined);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsCloneObject([MarshalAs(UnmanagedType.LPWStr)] string sObjectName, short nObjecttocloneID, out short nRetObjectID, DsObjectType eObjectType = DsObjectType.Undefined);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectPostCreateCallBack(DsAddObjectPostCreateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectPostCreateCallBack(DsAddObjectPostCreateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectPreDeleteCallBack(DsAddObjectPreDeleteCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectPreDeleteCallBack(DsAddObjectPreDeleteCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectPostDeleteCallBack(DsAddObjectPostDeleteCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectPostDeleteCallBack(DsAddObjectPostDeleteCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectClassID(short nObjectID, out short nRetID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassType(short nClassID, out DsClassType eRetType);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetObjectID([MarshalAs(UnmanagedType.LPWStr)] string sObjectName, out short nRetObjectID);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetObjectName(short nObjectID, StringBuilder szRetName, int nNameMaxSize);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectAttrUpdateCallBack(DsAddObjectAttrUpdateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectAttrUpdateCallBack(DsAddObjectAttrUpdateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddClassAttrUpdateCallBack(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sAttrName, DsAddClassAttrUpdateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveClassAttrUpdateCallBack(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string sAttrName, DsAddClassAttrUpdateCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectAttr(short nObjectID, short nAttrIndex, IntPtr attrArg);

        [DllImport(sPath + "DigistarClient.dll")]
        private static extern int DsSetObjectAttr(short nObjectID, short nAttrIndex, IntPtr attrArg, IntPtr controllerArg);

        [DllImport(sPath + "DigistarClient.dll")]
        private static extern int DsSetObjectAttrController(short nObjectID, short nAttrIndex, IntPtr attrArg, IntPtr controllerArg);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsStopObjectAttrController(short nObjectID, short nAttrIndex, short nElementIndex = -1);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectArraySize(short nObjectID, short nAttrIndex, out short nRetArraySize);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectArrayElem(short nObjectID, short nAttrIndex, short nElemIndex, IntPtr attrArg);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectArrayElemType(short nObjectID, short nAttrIndex, out DsAttr elemType);

        [DllImport(sPath + "DigistarClient.dll")]
        private static extern int DsSetObjectArrayElem(short nObjectID, short nAttrIndex, short nElemIndex, IntPtr attrArg, IntPtr pController);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAllocObjectArray(short nObjectID, short nAttrIndex, short nElementCount, IntPtr sLocaleName, IntPtr pAttrArray);

        /*
         * General
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsSubscribeToMessages(short nMessageLevel, bool bDebug, [MarshalAs(UnmanagedType.LPWStr)] string sLocaleName, DsMessageReceivedCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsUnsubscribeToMessages();

        /*
         * Object Hierarchy
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectChild(short nObjectID, short nNewChildID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectChildAfter(short nObjectID, short nNewChildID, short nAfterObjID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectAddChildCallBack(DsAddObjectAddChildCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectAddChildCallBack(DsAddObjectAddChildCallBack callbackFunc);

        /*
         * Connection
         */

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddPreClearTblsCallBack(DsAddPreClearTablesCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemovePreClearTblsCallBack(DsAddPreClearTablesCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddPreInitOrSyncCallBack(DsAddPreInitOrSyncCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemovePreInitOrSyncCallBack(DsAddPreInitOrSyncCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddPostInitOrSyncCallBack(DsAddPreInitOrSyncCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemovePostInitOrSyncCallBack(DsAddPreInitOrSyncCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsAddObjectRemoveChildCallBack(DsAddObjectRemoveChildCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsRemoveObjectRemoveChildCallBack(DsAddObjectRemoveChildCallBack callbackFunc);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectCount(ref short nRetCount);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectType(short nObjectID, out DsObjectType objectType);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectNumChildren(short nObjectID, out short nNumChildren);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectChildIDs(short nObjectID, short nMaxNum, [In, Out] short[] sIDArray, out short nNumWritten);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectNumParents(short nObjectID, out short nNumParents);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectParentIDs(short nObjectID, short nMaxNum, [In, Out] short[] sIDArray, out short nNumWritten);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectWorldPosition(short nObjectID, bool bLeftHanded,
            out Vector3D vObjectPos);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetObjectWorldPositionWithOffset(short nObjectID, bool bLeftHanded,
            out Vector3D vObjectPos);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsSendMessage(DsMesgLevel nMesgLevel, [MarshalAs(UnmanagedType.LPWStr)] string sMessage);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetControlInfoObject(short nObjectID, short nAttrIndex, out short nControlInfoObjectID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsTerrainGetHeightAboveSurface(short nBodyObjectID, short nObjectID, DsTerrainSurfaceType eSurface, out double dHeightMeters);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsTerrainGetPostion(short nBodyObjectID, double dLatitude, double dLongitude, double dHeightMeters, DsTerrainSurfaceType eSurface, out Vector3D vdLocalPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsTerrainGetLocation(short nBodyObjectID, Vector3D vdLocalPosition, DsTerrainSurfaceType eSurface, out double dLatitude, out double dLongitude, out double dHeightMeters);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsTerrainGetSurfaceHeight(short nBodyObjectID, double dLatitude, double dLongitude, DsTerrainSurfaceType eSurface, out double dHeightMeters);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsTerrainGetSurfaceNormal(short nBodyObjectID, double dLatitude, double dLongitude, DsTerrainSurfaceType eSurface, out Vector3D vdSurfaceNormal);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosCelestialToCartesian(double dRA, double dDeclination, double dDistance, out Vector3D vdPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosSphericalToCartesian(double dAzimuth, double dElevation, double dDistance, out Vector3D vdPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosEclipticToCartesian(double dLongitude, double dLatitude, double dDistance, out Vector3D vdPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosGalacticToCartesian(double dLongitude, double dLatitude, double dDistance, out Vector3D vdPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosPolarToCartesian(double dLatitude, double dLongitude, double dDistance, out Vector3D vdPosition);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosCartesianToCelestial(Vector3D vdPosition, out double dRightAscension, out double dDeclination, out double dDistance);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosCartesianToSpherical(Vector3D vdPosition, out double dAzimuth, out double dElevation, out double dDistance);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosCartesianToEcliptic(Vector3D vdPosition, out double dLongitude, out double dLatitude, out double dDistance);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsPosCartesianToGalactic(Vector3D vdPosition, out double dLongitude, out double dLatitude, out double dDistance);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsAttCartesianToCelestial(
                        double dHeading, double dPitch, double dRoll,
                        out double dRA, out double dDec, out double dHeadingCelestial);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern void DsAttCelestialToCartesian(
                        double dRA, double dDec, double dHeadingCelestial,
                        out double dHeading, out double dPitch, out double dRoll);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsDateToJulianDate(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec, out double dJulianDate);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsJulianDateToDate(double dJulianDate, out int nYear, out int nMonth, out int nDay, out int nHour, out int nMin, out int nSec);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsVectorParsecsToMeters(Vector3D vdParsecs, out Vector3D vdMeters);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetModelClassID([MarshalAs(UnmanagedType.LPWStr)] string sFilename, out short nClassID);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetErrorCodeString(int errorCode, StringBuilder retErrString, int nMaxStringLength);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsClassHasGroup(short nClassID, short nGroupIndex, out bool bHasGroup);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsClassHasGroupByName(short nClassId, [MarshalAs(UnmanagedType.LPWStr)] string pGroupName, out bool bHasGroup);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        public static extern int DsGetClassAttrGroupInfo(short nClassID, short nGroupIndex, StringBuilder pGroupName, out short nAttrIndexS, out short nAttrIndexE);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrGroupIndex(short nClassId, short nAttrIndex, out short nGroupIndex);

        [DllImport(sPath + "DigistarClient.dll")]
        public static extern int DsGetClassAttrGroupIndexByName(short nClassID, [MarshalAs(UnmanagedType.LPWStr)] string pGroupName, out short nGroupIndex);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        static extern int DsGetModelTextures([MarshalAs(UnmanagedType.LPWStr)] string pFilePath, out int nTextureCount, StringBuilder retErrString, int nMaxStringLength);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]
        private static extern int DsGetModelTextures2([MarshalAs(UnmanagedType.LPWStr)] string pFilePath, StringBuilder retTextureString, int nMaxStringLength);

        [DllImport(sPath + "DigistarClient.dll", CharSet = CharSet.Auto)]

        public static extern int DsGetModelTextureCount([MarshalAs(UnmanagedType.LPWStr)] string pFilePath, out int nTextureCount);

        public static int DsGetModelTextureArraySized(string filePath, out string[] textures)
        {
            int nCount;
            int hr = DsGetModelTextureCount(filePath, out nCount);
            int nbuffSize = DsConstants.MAX_PATH_LENGTH * nCount;
            StringBuilder szbuffer = new StringBuilder(nbuffSize);
            if (hr == 0 && nCount != 0)
                hr = DsGetModelTextures2(filePath, szbuffer, nbuffSize);

            textures = szbuffer.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            return hr;
        }

        public static int DsGetModelTextureArray(string filePath, out string[] textures)
        {
            int nbuffSize = DsConstants.MAX_PATH_LENGTH * DsConstants.MAX_ARRAY_SIZE;
            int nCount = 0;
            StringBuilder szbuffer = new StringBuilder(nbuffSize);
            int hr = DsGetModelTextures(filePath, out nCount, szbuffer, nbuffSize);

            textures = szbuffer.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            return hr;
        }


        public static int DsSendScriptCommandsNice(string sScriptName, string sCommands, float fTimecodeOffset)
        {
            int splitIndex = 0;
            int startIndex = 0;
            int maxSendSize = DsConstants.MAX_COMMANDBUFFER_LENGTH / 2 - 2;

            do
            {
                    if (sCommands.Length < maxSendSize)
                        return DsSendScriptCommands(sScriptName, sCommands, fTimecodeOffset);

                    splitIndex = sCommands.LastIndexOf("\r\n", maxSendSize, maxSendSize);
                    string commandSubset = sCommands.Substring(startIndex, splitIndex);
                    DsAPI.DsSendScriptCommands(sScriptName, commandSubset, fTimecodeOffset);
                    sCommands = sCommands.Substring(splitIndex);
             }
            while (sCommands.Length > 0);

            //some unexpected terrible error happened
            return -1;

        }
        

        public static int DsGetObjectAttr(short nObjectID, short nAttrIndex, DsAttribute attribute)
        {
            int nSize = Marshal.SizeOf(attribute);

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attribute, buffer, false);

            // Fill the unmanaged buffer with the attribute data from the API
            int nReturn = DsAPI.DsGetObjectAttr(nObjectID, nAttrIndex, buffer);

            // Copy the unmanaged buffer back to the attribute
            Marshal.PtrToStructure(buffer, attribute);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            // If this is an array, then update it too!
            if (attribute is DsArrayAttribute)
            {
                DsAttr elemType = DsAttr.Bool;
                DsAPI.DsGetObjectArrayElemType(nObjectID, nAttrIndex, out elemType);

                var arrayAttr = attribute as DsArrayAttribute;
                arrayAttr.ElementType = elemType;

                // Resize to the current size read above
                arrayAttr.Resize();

                short nIndex = 0;
                if (arrayAttr.InternalItems.Length > 0)
                {
                    foreach (var attr in arrayAttr.InternalItems)
                    {
                        DsGetObjectArrayElem(nObjectID, nAttrIndex, nIndex, attr);
                        nIndex++;
                    }
                }
            }

            return nReturn;
        }

        /// <summary>
        /// Returns the class attribute min value
        /// </summary>
        /// <param name="nClassID"></param>
        /// <param name="nAttrIndex"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static int DsGetClassAttrMin(short nClassID, short nAttrIndex, DsAttribute attribute)
        {
            if (attribute is DsArrayAttribute)
                return -1;

            int nSize = Marshal.SizeOf(attribute);
            int nReturn;

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attribute, buffer, false);

            // Fill the unmanaged buffer with the attribute data from the API
            nReturn = DsAPI.DsGetClassAttrMin(nClassID, nAttrIndex, buffer);

            // Copy the unmanaged buffer back to the attribute
            Marshal.PtrToStructure(buffer, attribute);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            return nReturn;
        }


        /// <summary>
        /// Returns the class attribute max value
        /// </summary>
        /// <param name="nClassID"></param>
        /// <param name="nAttrIndex"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static int DsGetClassAttrMax(short nClassID, short nAttrIndex, DsAttribute attribute)
        {
            if (attribute is DsArrayAttribute)
                return -1;

            int nSize = Marshal.SizeOf(attribute);
            int nReturn;

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attribute, buffer, false);

            // Fill the unmanaged buffer with the attribute data from the API
            nReturn = DsAPI.DsGetClassAttrMax(nClassID, nAttrIndex, buffer);

            // Copy the unmanaged buffer back to the attribute
            Marshal.PtrToStructure(buffer, attribute);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            return nReturn;
        }



        /// <summary>
        /// Returns the class attribute default value
        /// </summary>
        /// <param name="nClassID"></param>
        /// <param name="nAttrIndex"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static int DsGetClassAttrDefault(short nClassID, short nAttrIndex, DsAttribute attribute)
        {
            if (attribute is DsArrayAttribute)
                return -1;

            int nSize = Marshal.SizeOf(attribute);
            int nReturn;

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attribute, buffer, false);

            // Fill the unmanaged buffer with the attribute data from the API
            nReturn = DsAPI.DsGetClassAttrDefault(nClassID, nAttrIndex, buffer);

            // Copy the unmanaged buffer back to the attribute
            Marshal.PtrToStructure(buffer, attribute);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            return nReturn;
        }

        /// <summary>
        /// This updates a Ds attribute witht the data from an attribute index
        /// </summary>
        /// <param name="nObjectID">The Ds object</param>
        /// <param name="nAttrIndex">The Ds attribute</param>
        /// <param name="nElemIndex">The array element index</param>
        /// <param name="attribute">The DsAttribute to update.</param>
        /// <returns>DsAPIStatus of success.</returns>
        public static int DsGetObjectArrayElem(short nObjectID, short nAttrIndex, short nElemIndex, DsAttribute attribute)
        {
            int nSize = Marshal.SizeOf(attribute);

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attribute, buffer, false);

            // Fill the unmanaged buffer with the attribute data from the API
            int nReturn = DsAPI.DsGetObjectArrayElem(nObjectID, nAttrIndex, nElemIndex, buffer);

            // Copy the unmanaged buffer back to the attribute
            Marshal.PtrToStructure(buffer, attribute);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            return nReturn;
        }

        public static int DsSetObjectAttr(short nObjectID, short nAttrIndex, DsAttribute attrArg, bool bAllowFullArrayUpdate)
        {
            // Update the array by updating each index.
            if (attrArg is DsArrayAttribute)
            {
                // Do not update if we are not allowing a full array update
                if (!bAllowFullArrayUpdate)
                    return 0;

                DsAttr elemType = DsAttr.Bool;
                DsAPI.DsGetObjectArrayElemType(nObjectID, nAttrIndex, out elemType);
                short nArraySize = 0;
                DsAPI.DsGetObjectArraySize(nObjectID, nAttrIndex, out nArraySize);

                var arrayAttr = attrArg as DsArrayAttribute;
                if (arrayAttr.ElementType != elemType)
                    return (int)DsAPIError.InvalidArg;

                // Resize the array if it is not the same
                if (arrayAttr.InternalItems.Length != nArraySize)
                    DsAPI.DsAllocObjectArray(nObjectID, nAttrIndex, (short)arrayAttr.InternalItems.Length, IntPtr.Zero, IntPtr.Zero);

                // Send every array element!!
                if (arrayAttr.InternalItems.Length > 0)
                {
                    short nIndex = 0;
                    foreach (var attr in arrayAttr.InternalItems)
                    {
                        int nRet = DsSetObjectArrayElem(nObjectID, nAttrIndex, nIndex, attr);
                        if (nRet != (int)DsAPIError.None)
                            return nRet;
                        nIndex++;
                    }
                }
                return 0;
            }
            else
            {

                int nRet = 0;

                // Allocate an unmanaged buffer and copy the attribute data into the buffer
                int nSize = Marshal.SizeOf(attrArg);
                IntPtr buffer = Marshal.AllocHGlobal(nSize);
                Marshal.StructureToPtr(attrArg, buffer, false);

                // Set the object attribute
                nRet = DsSetObjectAttr(nObjectID, nAttrIndex, buffer, IntPtr.Zero);

                // Free the unmanaged buffer
                Marshal.FreeHGlobal(buffer);

                return nRet;
            }
        }

        public static int DsSetObjectArrayElem(short nObjectID, short nAttrIndex, short nElemIndex, DsAttribute attrArg)
        {
            int nRet = 0;

            // Allocate an unmanaged buffer and copy the attribute data into the buffer
            int nSize = Marshal.SizeOf(attrArg);
            IntPtr buffer = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(attrArg, buffer, false);

            // Set the object attribute
            nRet = DsSetObjectArrayElem(nObjectID, nAttrIndex, nElemIndex, buffer, IntPtr.Zero);

            // Free the unmanaged buffer
            Marshal.FreeHGlobal(buffer);

            return nRet;
        }


        /// <summary>
        /// Returns a C# type based on the DsAttr enumeration
        /// </summary>
        /// <param name="attribType"></param>
        /// <author>JMG</author>       
        public static Type GetTypeFromDsAttrType(DsAttr eAttrType)
        {
            switch (eAttrType)
            {
                case DsAttr.Angle:
                    return typeof(DsAngleAttribute);

                case DsAttr.Angle2:
                    return typeof(DsAngle2Attribute);

                case DsAttr.Angle3:
                    return typeof(DsAngle3Attribute);

                case DsAttr.Attitude:
                    return typeof(DsAttitudeAttribute);

                case DsAttr.AudioPathname:
                    return typeof(DsAudioPathNameAttribute);

                case DsAttr.Bool:
                    return typeof(DsBoolAttribute);

                case DsAttr.Bool2:
                    return typeof(DsBool2Attribute);

                case DsAttr.Bool3:
                    return typeof(DsBool3Attribute);

                case DsAttr.Color:
                    return typeof(DsColorAttribute);

                case DsAttr.Datetime:
                    return typeof(DsDateTimeAttribute);

                case DsAttr.Distance:
                    return typeof(DsDistanceAttribute);

                case DsAttr.Distance2:
                    return typeof(DsDistance2Attribute);

                case DsAttr.Distance3:
                    return typeof(DsDistance3Attribute);

                case DsAttr.Double:
                    return typeof(DsDoubleAttribute);

                case DsAttr.Dvector2:
                    return typeof(DsDVector2Attribute);

                case DsAttr.Dvector3:
                    return typeof(DsDVector3Attribute);

                case DsAttr.Dvector4:
                    return typeof(DsDVector4Attribute);

                case DsAttr.SummedDelta:
                    return typeof(DsSummedDeltaAttribute);

                case DsAttr.SummedDelta2:
                    return typeof(DsSummedDelta2Attribute);

                case DsAttr.SummedDelta3:
                    return typeof(DsSummedDelta3Attribute);

                case DsAttr.Enum:
                    return typeof(DsEnumAttribute);

                case DsAttr.Float:
                    return typeof(DsFloatAttribute);

                case DsAttr.Font:
                    return typeof(DsFontAttribute);

                case DsAttr.Fvector2:
                    return typeof(DsFVector2Attribute);

                case DsAttr.Fvector3:
                    return typeof(DsFVector3Attribute);

                case DsAttr.HourAngle:
                    return typeof(DsHourAngleAttribute);

                case DsAttr.ImagePathname:
                    return typeof(DsImagePathNameAttribute);

                case DsAttr.Int16:
                    return typeof(DsInt16Attribute);

                case DsAttr.Int32:
                    return typeof(DsInt32Attribute);

                case DsAttr.Ivector2:
                    return typeof(DsIVector2Attribute);

                case DsAttr.Ivector3:
                    return typeof(DsIVector3Attribute);

                case DsAttr.JsPathname:
                    return typeof(DsJsPathNameAttribute);

                case DsAttr.ModelName:
                    return typeof(DsModelNameAttribute);

                case DsAttr.ModelPathname:
                    return typeof(DsModelPathNameAttribute);

                case DsAttr.Morph:
                    return typeof(DsMorphAttribute);

                case DsAttr.Name:
                    return typeof(DsNameAttribute);

                case DsAttr.SystemObject:
                    return typeof(DsSystemObjectAttribute);

                case DsAttr.Directory:
                    return typeof(DsDirectoryAttribute);

                case DsAttr.Pathname:
                    return typeof(DsPathNameAttribute);

                case DsAttr.Percent:
                    return typeof(DsPercentAttribute);

                case DsAttr.Percent2:
                    return typeof(DsPercent2Attribute);

                case DsAttr.Percent3:
                    return typeof(DsPercent3Attribute);

                case DsAttr.Position:
                    return typeof(DsPositionAttribute);

                case DsAttr.Scale:
                    return typeof(DsScaleAttribute);

                case DsAttr.ScriptPathname:
                    return typeof(DsScriptPathNameAttribute);

                case DsAttr.Showclockmode:
                    return typeof(DsShowClockModeAttribute);

                case DsAttr.String:
                    return typeof(DsStringAttribute);

                case DsAttr.CommandString:
                    return typeof(DsCommandStringAttribute);

                case DsAttr.TerrainPathname:
                    return typeof(DsTerrainPathNameAttribute);

                case DsAttr.TextureName:
                    return typeof(DsTextureNameAttribute);

                case DsAttr.TerrainName:
                    return typeof(DsTerrainNameAttribute);

                case DsAttr.Timecode:
                    return typeof(DsTimecodeAttribute);

                case DsAttr.Uint32:
                    return typeof(DsUint32Attribute);

                case DsAttr.Uint8:
                    return typeof(DsUint8Attribute);

                case DsAttr.VideoPathname:
                    return typeof(DsVideoPathNameAttribute);

                case DsAttr.Array:
                    return typeof(DsArrayAttribute);

                case DsAttr.SlidesetPathname:
                    return typeof(DsSlidesetPathNameAttribute);

                case DsAttr.RangeMap:
                    return typeof(DsRangeMapAttribute);

                case DsAttr.ColorTransfer:
                    return typeof(DsColorTransferAttribute);

                case DsAttr.Handle:
                    return typeof(DsHandleAttribute);

                default:
                    return null;
            }
        }


        /// <summary>
        /// Returns DsAttr enumeration based on the C# type
        /// </summary>
        /// <param name="attribType"></param>
        /// <author>JMG</author>       
        public static DsAttr GetDsAttrTypeFromType(Type type)
        {
            if (type == typeof(DsAngleAttribute)) return DsAttr.Angle;

            else if (type == typeof(DsAngle2Attribute)) return DsAttr.Angle2;

            else if (type == typeof(DsAngle3Attribute)) return DsAttr.Angle3;

            else if (type == typeof(DsAttitudeAttribute)) return DsAttr.Attitude;

            else if (type == typeof(DsAudioPathNameAttribute)) return DsAttr.AudioPathname;

            else if (type == typeof(DsBoolAttribute)) return DsAttr.Bool;

            else if (type == typeof(DsBool2Attribute)) return DsAttr.Bool2;

            else if (type == typeof(DsBool3Attribute)) return DsAttr.Bool3;

            else if (type == typeof(DsColorAttribute)) return DsAttr.Color;

            else if (type == typeof(DsDateTimeAttribute)) return DsAttr.Datetime;

            else if (type == typeof(DsDistanceAttribute)) return DsAttr.Distance;

            else if (type == typeof(DsDistance2Attribute)) return DsAttr.Distance2;

            else if (type == typeof(DsDistance3Attribute)) return DsAttr.Distance3;

            else if (type == typeof(DsDoubleAttribute)) return DsAttr.Double;

            else if (type == typeof(DsDVector2Attribute)) return DsAttr.Dvector2;

            else if (type == typeof(DsDVector3Attribute)) return DsAttr.Dvector3;

            else if (type == typeof(DsDVector4Attribute)) return DsAttr.Dvector4;

            else if (type == typeof(DsSummedDeltaAttribute)) return DsAttr.SummedDelta;

            else if (type == typeof(DsSummedDelta2Attribute)) return DsAttr.SummedDelta2;

            else if (type == typeof(DsSummedDelta3Attribute)) return DsAttr.SummedDelta3;

            else if (type == typeof(DsEnumAttribute)) return DsAttr.Enum;

            else if (type == typeof(DsFloatAttribute)) return DsAttr.Float;

            else if (type == typeof(DsFontAttribute)) return DsAttr.Font;

            else if (type == typeof(DsFVector2Attribute)) return DsAttr.Fvector2;

            else if (type == typeof(DsFVector3Attribute)) return DsAttr.Fvector3;

            else if (type == typeof(DsHourAngleAttribute)) return DsAttr.HourAngle;

            else if (type == typeof(DsImagePathNameAttribute)) return DsAttr.ImagePathname;

            else if (type == typeof(DsInt16Attribute)) return DsAttr.Int16;

            else if (type == typeof(DsInt32Attribute)) return DsAttr.Int32;

            else if (type == typeof(DsIVector2Attribute)) return DsAttr.Ivector2;

            else if (type == typeof(DsIVector3Attribute)) return DsAttr.Ivector3;

            else if (type == typeof(DsJsPathNameAttribute)) return DsAttr.JsPathname;

            else if (type == typeof(DsModelNameAttribute)) return DsAttr.ModelName;

            else if (type == typeof(DsModelPathNameAttribute)) return DsAttr.ModelPathname;

            else if (type == typeof(DsMorphAttribute)) return DsAttr.Morph;

            else if (type == typeof(DsNameAttribute)) return DsAttr.Name;

            else if (type == typeof(DsSystemObjectAttribute)) return DsAttr.SystemObject;

            else if (type == typeof(DsDirectoryAttribute)) return DsAttr.Directory;

            else if (type == typeof(DsPathNameAttribute)) return DsAttr.Pathname;

            else if (type == typeof(DsPercentAttribute)) return DsAttr.Percent;

            else if (type == typeof(DsPercent2Attribute)) return DsAttr.Percent2;

            else if (type == typeof(DsPercent3Attribute)) return DsAttr.Percent3;
            
            else if (type == typeof(DsPositionAttribute)) return DsAttr.Position;

            else if (type == typeof(DsScaleAttribute)) return DsAttr.Scale;

            else if (type == typeof(DsScriptPathNameAttribute)) return DsAttr.ScriptPathname;

            else if (type == typeof(DsShowClockModeAttribute)) return DsAttr.Showclockmode;

            else if (type == typeof(DsStringAttribute)) return DsAttr.String;

            else if (type == typeof(DsCommandStringAttribute)) return DsAttr.CommandString;

            else if (type == typeof(DsTerrainPathNameAttribute)) return DsAttr.TerrainPathname;

            else if (type == typeof(DsTextureNameAttribute)) return DsAttr.TextureName;

            else if (type == typeof(DsTerrainNameAttribute)) return DsAttr.TerrainName;

            else if (type == typeof(DsTimecodeAttribute)) return DsAttr.Timecode;

            else if (type == typeof(DsUint32Attribute)) return DsAttr.Uint32;

            else if (type == typeof(DsUint8Attribute)) return DsAttr.Uint8;

            else if (type == typeof(DsVideoPathNameAttribute)) return DsAttr.VideoPathname;

            else if (type == typeof(DsArrayAttribute)) return DsAttr.Array;

            else if (type == typeof(DsSlidesetPathNameAttribute)) return DsAttr.SlidesetPathname;

            else if (type == typeof(DsRangeMapAttribute)) return DsAttr.RangeMap;

            else if (type == typeof(DsColorTransferAttribute)) return DsAttr.ColorTransfer;

            else if (type == typeof(DsHandleAttribute)) return DsAttr.Handle;

            return DsAttr.Max;
        }


        /// <summary>
        /// Creates an attribute based on the DsAttr
        /// </summary>
        /// <param name="attribType"></param>
        /// <author>BJB</author>       
        public static DsAttribute DsCreateAttributeFromType(DsAttr eAttrType)
        {
            switch (eAttrType)
            {
                case DsAttr.Angle:
                    return new DsAngleAttribute();

                case DsAttr.Angle2:
                    return new DsAngle2Attribute();

                case DsAttr.Angle3:
                    return new DsAngle3Attribute();

                case DsAttr.Attitude:
                    return new DsAttitudeAttribute();

                case DsAttr.AudioPathname:
                    return new DsAudioPathNameAttribute();

                case DsAttr.Bool:
                    return new DsBoolAttribute();

                case DsAttr.Bool2:
                    return new DsBool2Attribute();

                case DsAttr.Bool3:
                    return new DsBool3Attribute();

                case DsAttr.Color:
                    return new DsColorAttribute();

                case DsAttr.Datetime:
                    return new DsDateTimeAttribute();

                case DsAttr.Distance:
                    return new DsDistanceAttribute();

                case DsAttr.Distance2:
                    return new DsDistance2Attribute();

                case DsAttr.Distance3:
                    return new DsDistance3Attribute();

                case DsAttr.Double:
                    return new DsDoubleAttribute();

                case DsAttr.Dvector2:
                    return new DsDVector2Attribute();

                case DsAttr.Dvector3:
                    return new DsDVector3Attribute();

                case DsAttr.Dvector4:
                    return new DsDVector4Attribute();

                case DsAttr.SummedDelta:
                    return new DsSummedDeltaAttribute();

                case DsAttr.SummedDelta2:
                    return new DsSummedDelta2Attribute();

                case DsAttr.SummedDelta3:
                    return new DsSummedDelta3Attribute();

                case DsAttr.Enum:
                    return new DsEnumAttribute();

                case DsAttr.Float:
                    return new DsFloatAttribute();

                case DsAttr.Font:
                    return new DsFontAttribute();

                case DsAttr.Fvector2:
                    return new DsFVector2Attribute();

                case DsAttr.Fvector3:
                    return new DsFVector3Attribute();

                case DsAttr.HourAngle:
                    return new DsHourAngleAttribute();

                case DsAttr.ImagePathname:
                    return new DsImagePathNameAttribute();

                case DsAttr.Int16:
                    return new DsInt16Attribute();

                case DsAttr.Int32:
                    return new DsInt32Attribute();

                case DsAttr.Ivector2:
                    return new DsIVector2Attribute();

                case DsAttr.Ivector3:
                    return new DsIVector3Attribute();

                case DsAttr.JsPathname:
                    return new DsJsPathNameAttribute();

                case DsAttr.ModelName:
                    return new DsModelNameAttribute();

                case DsAttr.ModelPathname:
                    return new DsModelPathNameAttribute();

                case DsAttr.Morph:
                    return new DsMorphAttribute();

                case DsAttr.Name:
                    return new DsNameAttribute();

                case DsAttr.SystemObject:
                    return new DsSystemObjectAttribute();

                case DsAttr.Directory:
                    return new DsDirectoryAttribute();

                case DsAttr.Pathname:
                    return new DsPathNameAttribute();

                case DsAttr.Percent:
                    return new DsPercentAttribute();

                case DsAttr.Percent2:
                    return new DsPercent2Attribute();

                case DsAttr.Percent3:
                    return new DsPercent3Attribute();

                case DsAttr.Position:
                    return new DsPositionAttribute();

                case DsAttr.Scale:
                    return new DsScaleAttribute();

                case DsAttr.ScriptPathname:
                    return new DsScriptPathNameAttribute();

                case DsAttr.Showclockmode:
                    return new DsShowClockModeAttribute();

                case DsAttr.String:
                    return new DsStringAttribute();

                case DsAttr.CommandString:
                    return new DsCommandStringAttribute();

                case DsAttr.TerrainPathname:
                    return new DsTerrainPathNameAttribute();

                case DsAttr.TextureName:
                    return new DsTextureNameAttribute();

                case DsAttr.TerrainName:
                    return new DsTerrainNameAttribute();

                case DsAttr.Timecode:
                    return new DsTimecodeAttribute();

                case DsAttr.Uint32:
                    return new DsUint32Attribute();

                case DsAttr.Uint8:
                    return new DsUint8Attribute();

                case DsAttr.VideoPathname:
                    return new DsVideoPathNameAttribute();

                case DsAttr.Array:
                    return new DsArrayAttribute();

                case DsAttr.SlidesetPathname:
                    return new DsSlidesetPathNameAttribute();

                case DsAttr.RangeMap:
                    return new DsRangeMapAttribute();

                case DsAttr.ColorTransfer:
                    return new DsColorTransferAttribute();

                case DsAttr.Handle:
                    return new DsHandleAttribute();

                default:
                    throw new Exception("DsCreateAttributeFromType - Could not create unknown attribute type");
            }
        }

        /// <summary>
        /// This allows to return the DsAttr inline, so that in can be used in LINQ queries.
        /// </summary>
        /// <param name="nClassID"></param>
        /// <param name="nAttrIndex"></param>
        /// <returns></returns>
        public static DsAttr DsGetClassAttrArrayType(short nClassID, short nAttrIndex)
        {
            DsAttr arrayType;
            if (DsGetClassAttrArrayType(nClassID, nAttrIndex, out arrayType) == (int)DsAPIError.None)
                return arrayType;
            else return DsAttr.Undefined;
        }

    }

}
