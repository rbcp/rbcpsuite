﻿/////////////////////////////////////////////////////////////////////////////////////////
//
// COPYRIGHT (C) 2010 Evans & Sutherland Computer Corporation
// All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Collections.ObjectModel;
using System.Diagnostics;  // for ReadOnlyCollection
using System.Collections.Specialized;
using System.Collections;
using System.Xml.Serialization;
using System.Globalization;
using System.Web.Script.Serialization;


namespace ES.Ds.Common
{
    /// <summary>
    /// VectorIndexType type enumeration
    /// </summary>
    /// <author>KBW</author>
    public enum VectorIndexType
    {
        X,  // Lat
        Y,  // Lon
        Z,  // Altitude
        Heading,
        Timezone,
        Max
    }

    /// <summary>
    /// ColorIndexType type enumeration
    /// </summary>
    /// <author>KBW</author>
    public enum ColorIndexType
    {
        Red,
        Green,
        Blue,
        Alpha,
    }

    /// <summary>
    /// CelestialPartType type enumeration
    /// </summary>
    /// <author>KBW</author>
    public enum CelestialPartType
    {
        RA, 
        Declination,
    }

    /// <summary>
    /// RAPartType type enumeration
    /// </summary>
    /// <author>KBW</author>
    public enum RAPartType
    {
        Hours, 
        Minutes,
        Seconds,
    }

    /// <summary>
    /// DecPartType type enumeration
    /// </summary>
    /// <author>KBW</author>
    public enum DecPartType
    {
        Degrees,
        Minutes,
        Seconds,
    }

    /// <summary>
    /// This is a .NET property attribute that says whether a .NET property in
    /// a class is a property of a DsAttribute. If this is on a property
    /// then this value is allowed to be shown in the GUI as a data from a DsAttribute
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public sealed class DsAttributePropertyAttribute : Attribute
    {
        private readonly string m_displayName;

        // Constructs the attribute
        public DsAttributePropertyAttribute(string displayName) 
        {
            m_displayName = displayName;
            Type = null;
        }

        /// <summary>
        /// The name that should be displayed to the user for this property.
        /// </summary>
        public string DisplayName
        {
            get { return m_displayName; } 
        }

        /// <summary>
        /// If this is null, then 
        /// any DsAttribute-derived type will
        /// allow this as visible.
        /// If this is not null,
        /// Then only if the derived type is this
        /// then it is allowed to be shown.
        /// </summary>
        public Type Type { get; set; }

    }

    /// <summary>
    /// This describes an attriute that only gives information about it.
    /// </summary>
    [global::System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    sealed class DsAttributeInfoAttribute : Attribute
    {
        // This is a positional argument
        public DsAttributeInfoAttribute()
        {
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAttribute
    {
        private DsAttr m_eType = DsAttr.Undefined;
        protected const double INHERIT_COLOR_SIGNAL = -1.0;
        protected const double INHERIT_INTENSITY_SIGNAL = -1.0;
        protected const double INHERIT_MORPH_SIGNAL = -101.0;

        protected static NumberFormatInfo m_usNumberFormat = CultureInfo.GetCultureInfo("en-US").NumberFormat;

        public DsAttribute(DsAttr type)
        { m_eType = type; }

        /// <summary>
        /// Implementation for UpdateUI()
        /// </summary>
        partial void UpdateUI_Impl();
        
        /// <summary>
        /// This triggers the UI to refresh the attribute values.
        /// </summary>
        protected void UpdateUI()
        {
            UpdateUI_Impl();
        }

        /// <summary>
        /// This triggers the attribute to be updated on the host, due to a property change in the attribute.
        /// This does not do anything, if the attribute has a Confirm or a Static flag,
        /// or if this is a preference, and AutoApplyPreferenceChanges == false
        /// This allows passing in a propertyName, in case a visual update is requested
        /// event though the host does not get an update.
        /// </summary>
        protected void UpdateHost(params string [] propertyNames)
        {
            UpdateHost(false, false, propertyNames);
        }

        /// <summary>
        /// This triggers the attribute to be updated on the host also, with the added option
        /// to force the array attributes to perform a full update, meaning that the current
        /// array size also gets applied and all elements get updated on the host from the UI.
        /// </summary>
        /// <param name="bAllowFullArrayUpdate"></param>
        /// <param name="propertyNames"></param>
        protected void UpdateHost(bool bAllowFullArrayUpdate, params string[] propertyNames)
        {
            UpdateHost(false, bAllowFullArrayUpdate, propertyNames);
        }

        /// <summary>
        /// This triggers tha attribute to be hpdated on the hose, due to a major change, or Apply() call
        /// on the attribute. 
        /// This does not do anything, if the attribute has a Confurm or a Static flag,
        /// or if this is a preference and AutoApplyPreferenceChanges == false
        /// This allows passing in a propertyName, in case a visual update is requested.
        /// </summary>
        /// <param name="propertyNames"></param>
        protected void Apply_UpdateHost(params string[] propertyNames)
        {
            UpdateHost(true, true, propertyNames);
        }
        /// <summary>
        /// This updates data to the host.
        /// </summary>
        /// <param name="bFromApplyCall">This provides info whether this is getting called from the Apply() call.</param>
        /// <param name="bAllowFullArrayUpdate">If this is an array, whether we allow an update to the full array instead of just the elements.</param>
        /// <param name="propertyNames">The source property names of the object that are affected</param>
        partial void UpdateHost(bool bFromApplyCall, bool bAllowFullArrayUpdate, params string[] propertyNames);

        protected virtual void OnPropertyChanged(string sName)
        { }

        #region Properties

        /////////////////////////////////////////////////////////////////////////////////
        // PROPERTIES
        /////////////////////////////////////////////////////////////////////////////////                

        /// <summary>
        /// Gets the type of the attribute
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeInfo]
        public DsAttr Type
        {
            get { return m_eType; }
        }

        /// <summary>
        /// Gets whether or not this attribute is to be inherited from its parent.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeInfo]
        public virtual bool Inherited
        {
            get { return false; }
        }
        #endregion

        #region Private Member Functions

        /////////////////////////////////////////////////////////////////////////////////
        // PRIVATE MEMBER FUNCTIONS
        /////////////////////////////////////////////////////////////////////////////////

        #endregion               
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsBoolAttribute : DsAttribute, INotifyPropertyChanged
    {        
        private byte m_byValue = 0;        

        public DsBoolAttribute()
            : base(DsAttr.Bool)
        { }                 

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public bool Value
        {
            get { return Convert.ToBoolean(m_byValue); }
            set
            {
                if (m_byValue != Convert.ToByte(value))
                {
                    m_byValue = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !Value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotValue")]
        public bool NotValue
        {
            get { return !Convert.ToBoolean(m_byValue); }
            set
            {
                if (m_byValue != Convert.ToByte(!value))
                {
                    m_byValue = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsBool2Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private byte[] m_byVector = { 0, 0 };

        public DsBool2Attribute()
            : base(DsAttr.Bool2)
        { }

        /// <summary>
        /// Gets/sets the X value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public bool X
        {
            get { return Convert.ToBoolean(m_byVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.X] != Convert.ToByte(value))
                {
                    m_byVector[(int)VectorIndexType.X] = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !X value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotX")]
        public bool NotX
        {
            get { return !Convert.ToBoolean(m_byVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.X] != Convert.ToByte(!value))
                {
                    m_byVector[(int)VectorIndexType.X] = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }


        /// <summary>
        /// Gets/sets the Y value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public bool Y
        {
            get { return Convert.ToBoolean(m_byVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Y] != Convert.ToByte(value))
                {
                    m_byVector[(int)VectorIndexType.Y] = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !Y value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotY")]
        public bool NotY
        {
            get { return !Convert.ToBoolean(m_byVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Y] != Convert.ToByte(!value))
                {
                    m_byVector[(int)VectorIndexType.Y] = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }
        
        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsBool3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private byte[] m_byVector = { 0, 0, 0 };

        public DsBool3Attribute()
            : base(DsAttr.Bool3)
        { }

        /// <summary>
        /// Gets/sets the X value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public bool X
        {
            get { return Convert.ToBoolean(m_byVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.X] != Convert.ToByte(value))
                {
                    m_byVector[(int)VectorIndexType.X] = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !X value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotX")]
        public bool NotX
        {
            get { return !Convert.ToBoolean(m_byVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.X] != Convert.ToByte(!value))
                {
                    m_byVector[(int)VectorIndexType.X] = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }


        /// <summary>
        /// Gets/sets the Y value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public bool Y
        {
            get { return Convert.ToBoolean(m_byVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Y] != Convert.ToByte(value))
                {
                    m_byVector[(int)VectorIndexType.Y] = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !Y value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotY")]
        public bool NotY
        {
            get { return !Convert.ToBoolean(m_byVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Y] != Convert.ToByte(!value))
                {
                    m_byVector[(int)VectorIndexType.Y] = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }


        /// <summary>
        /// Gets/sets the Z value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public bool Z
        {
            get { return Convert.ToBoolean(m_byVector[(int)VectorIndexType.Z]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Z] != Convert.ToByte(value))
                {
                    m_byVector[(int)VectorIndexType.Z] = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the !Z value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("NotZ")]
        public bool NotZ
        {
            get { return !Convert.ToBoolean(m_byVector[(int)VectorIndexType.Z]); }
            set
            {
                if (m_byVector[(int)VectorIndexType.Z] != Convert.ToByte(!value))
                {
                    m_byVector[(int)VectorIndexType.Z] = Convert.ToByte(!value);
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsInt16Attribute : DsAttribute, INotifyPropertyChanged
    {        
        private Int16 m_nValue = 22;
        
        public DsInt16Attribute()
            : base(DsAttr.Int16)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public Int16 Value
        {
            get { return m_nValue; }
            set 
            {
                m_nValue = value;
                UpdateHost();
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsInt32Attribute : DsAttribute, INotifyPropertyChanged
    {        
        private Int32 m_nValue = 894512;        

        public DsInt32Attribute()
            : base(DsAttr.Int32)
        { }
        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public Int32 Value
        {
            get { return m_nValue; }
            set
            {
                if (m_nValue != value)
                {
                    m_nValue = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsUint8Attribute : DsAttribute, INotifyPropertyChanged
    {        
        private byte m_byValue = 0;  // c# doesn't have UInt8        

        public DsUint8Attribute()
            : base(DsAttr.Uint8)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public byte Value
        {
            get { return m_byValue; }
            set
            {
                if (m_byValue != value)
                {
                    m_byValue = value;
                    UpdateHost();
                }
            }
       }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsUint32Attribute : DsAttribute, INotifyPropertyChanged
    {        
        private UInt32 m_nValue = 1;        

        public DsUint32Attribute()
            : base(DsAttr.Uint32)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public UInt32 Value
        {
            get { return m_nValue; }
            set
            {
                if (m_nValue != value)
                {
                    m_nValue = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsFloatAttribute : DsAttribute, INotifyPropertyChanged
    {        
        private float m_fValue = 28.123456789f;        

        public DsFloatAttribute()
            : base(DsAttr.Float)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public float Value
        {
            get { return m_fValue; }
            set
            {
                if (m_fValue != value)
                {
                    m_fValue = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDoubleAttribute : DsAttribute, INotifyPropertyChanged
    {        
        private double m_dValue = 18.123456789;        

        public DsDoubleAttribute()
            : base(DsAttr.Double)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return m_dValue; }
            set
            {
                if (m_dValue != value)
                {
                    m_dValue = value;
                    UpdateHost();
                }
            }
       }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAngleAttribute : DsAttribute, INotifyPropertyChanged
    {        
        private double m_dValue = 45.70f;        

        public DsAngleAttribute()
            : base(DsAttr.Angle)
        { }
 
        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return MathUtilities.RadToDeg(m_dValue); }
            set
            {
                if (m_dValue != MathUtilities.DegToLimitedRad(value))
                {
                    m_dValue = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
      }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsHourAngleAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dValue = 0.00f;

        public DsHourAngleAttribute()
            : base(DsAttr.HourAngle)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return MathUtilities.RadToDeg(m_dValue)/15; }
            set
            {
                if (m_dValue != MathUtilities.DegToLimitedRad(value) * 15)
                {
                    m_dValue = MathUtilities.DegToLimitedRad(value) * 15;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsIVector2Attribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private int[] m_nVector  = { 9, 8 };        

        public DsIVector2Attribute()
            : base(DsAttr.Ivector2)
        { }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public int X
        {
            get { return m_nVector[(int)VectorIndexType.X]; }
            set 
            {
                if (m_nVector[(int)VectorIndexType.X] != value)
                {
                    m_nVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public int Y
        {
            get { return m_nVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_nVector[(int)VectorIndexType.Y] != value)
                {
                    m_nVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsFVector2Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private float[] m_fVector = { 678.21f, -14.9f };

        public DsFVector2Attribute()
            : base(DsAttr.Fvector2)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public float X
        {
            get { return m_fVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_fVector[(int)VectorIndexType.X] != value)
                {
                    m_fVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public float Y
        {
            get { return m_fVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_fVector[(int)VectorIndexType.Y] != value)
                {
                    m_fVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDVector2Attribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private double[] m_dVector = { 44.7, -88.9 };        

        public DsDVector2Attribute()
            : base(DsAttr.Dvector2)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    if (PreserveScale && m_dVector[(int)VectorIndexType.X] != 0)
                    {
                        m_dVector[(int)VectorIndexType.Y] = (m_dVector[(int)VectorIndexType.Y] / m_dVector[(int)VectorIndexType.X]) * value;
                    }

                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
       }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    if (PreserveScale && m_dVector[(int)VectorIndexType.Y] != 0)
                    {
                        m_dVector[(int)VectorIndexType.X] = (m_dVector[(int)VectorIndexType.X] / m_dVector[(int)VectorIndexType.Y]) * value;
                    }

                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
       }

        /// <summary>
        /// Allows setting all components at once
        /// </summary>
        /// <param name="dX"></param>
        /// <param name="dY"></param>
        public void SetVector(double dX, double dY)
        {
            m_dVector[(int)VectorIndexType.X] = dX;
            m_dVector[(int)VectorIndexType.Y] = dY;
            UpdateHost();
        }
        
        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        // Preserve Scale
        private bool m_bPreserveScale = false;

        public bool PreserveScale
        {
            get { return m_bPreserveScale; }
            set { m_bPreserveScale = value; }
        }

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDistance2Attribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private double[]         m_dVector      = { 44.7, -88.9 };
        private DsDistanceUnit   m_distanceUnit = DsDistanceUnit.Meter;        

        public DsDistance2Attribute()
            : base(DsAttr.Distance2)
        { }

        /// <summary>
        /// Gets/sets the distance unit.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW,JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DsDistanceUnit DistanceUnit
        {
            get { return m_distanceUnit; }
            set
            {
                if (m_distanceUnit != value)
                {
                    m_distanceUnit = value;
                    UpdateHost("Distance1", "Distance2", "DistanceUnit");
                }
            }
        }

        /// <summary>
        /// Gets/sets the Distance1 value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double Distance1
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.X], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.X] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.X] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Distance2 value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Distance2
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Y], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.Y] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.Y] = dValueMeters;
                    UpdateHost();
                }
            }
        }


        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAngle2Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private double[] m_dVector = { 44.7, -88.9 };

        public DsAngle2Attribute()
            : base(DsAttr.Angle2)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Angle1")]
        [DsAttributeProperty("Angle1")]
        public double Angle1
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Angle2")]
        [DsAttributeProperty("Angle2")]
        public double Angle2
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsIVector3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private int[] m_nVector = { 9, 8, 7 };

        public DsIVector3Attribute()
            : base(DsAttr.Ivector3)
        { }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public int X
        {
            get { return m_nVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_nVector[(int)VectorIndexType.X] != value)
                {
                    m_nVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public int Y
        {
            get { return m_nVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_nVector[(int)VectorIndexType.Y] != value)
                {
                    m_nVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public int Z
        {
            get { return m_nVector[(int)VectorIndexType.Z]; }
            set
            {
                if (m_nVector[(int)VectorIndexType.Z] != value)
                {
                    m_nVector[(int)VectorIndexType.Z] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDistance3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 44.7, -88.9, 44.7 };
        private DsDistanceUnit m_distanceUnit = DsDistanceUnit.Meter;

        public DsDistance3Attribute()
            : base(DsAttr.Distance3)
        { }

        /// <summary>
        /// Gets/sets the distance unit.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW,JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DsDistanceUnit DistanceUnit
        {
            get { return m_distanceUnit; }
            set
            {
                if (m_distanceUnit != value)
                {
                    m_distanceUnit = value;
                    UpdateHost("Distance1", "Distance2", "Distance3", "DistanceUnit");
                }
            }
        }

        /// <summary>
        /// Gets/sets the Distance1 value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double Distance1
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.X], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.X] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.X] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Distance2 value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Distance2
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Y], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.Y] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.Y] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Distance3 value.
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Distance3
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Z], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.Z] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.Z] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsFVector3Attribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private float[] m_fVector = { 678.21f, -14.9f, -0.21f };        

        public DsFVector3Attribute()
            : base(DsAttr.Fvector3)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public float X
        {
            get { return m_fVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_fVector[(int)VectorIndexType.X] != value)
                {
                    m_fVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public float Y
        {
            get { return m_fVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_fVector[(int)VectorIndexType.Y] != value)
                {
                    m_fVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Z value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public float Z
        {
            get { return m_fVector[(int)VectorIndexType.Z]; }
            set
            {
                if (m_fVector[(int)VectorIndexType.Z] != value)
                {
                    m_fVector[(int)VectorIndexType.Z] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDVector3Attribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 13.2, 76.51, -19.23 };        

        public DsDVector3Attribute()
            : base(DsAttr.Dvector3)
        { }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set 
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set 
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return m_dVector[(int)VectorIndexType.Z]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != value)
                {
                    m_dVector[(int)VectorIndexType.Z] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsSummedDeltaAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dValue = 18.123456789;

        public DsSummedDeltaAttribute()
            : base(DsAttr.SummedDelta)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return m_dValue; }
            set
            {
                if (m_dValue != value)
                {
                    m_dValue = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsSummedDelta2Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private double[] m_dVector = { 13.2, 76.51 };

        public DsSummedDelta2Attribute()
            : base(DsAttr.SummedDelta2)
        { }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsSummedDelta3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 13.2, 76.51, -19.23 };

        public DsSummedDelta3Attribute()
            : base(DsAttr.SummedDelta3)
        { }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return m_dVector[(int)VectorIndexType.Z]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != value)
                {
                    m_dVector[(int)VectorIndexType.Z] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAngle3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 44.7, -88.9, 44.7 };

        public DsAngle3Attribute()
            : base(DsAttr.Angle3)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Angle1")]
        [DsAttributeProperty("Angle1")]
        public double Angle1
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Angle2")]
        [DsAttributeProperty("Angle2")]
        public double Angle2
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Angle3")]
        [DsAttributeProperty("Angle3")]
        public double Angle3
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Z]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsFVector4Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private float[] m_fVector = { 678.21f, -14.9f, -0.21f, -0.35f };

        public DsFVector4Attribute()
            : base(DsAttr.Double)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public float X
        {
            get { return m_fVector[0]; }
            set
            {
                if (m_fVector[0] != value)
                {
                    m_fVector[0] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public float Y
        {
            get { return m_fVector[1]; }
            set
            {
                if (m_fVector[1] != value)
                {
                    m_fVector[1] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Z value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public float Z
        {
            get { return m_fVector[2]; }
            set
            {
                if (m_fVector[2] != value)
                {
                    m_fVector[2] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the W value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("W")]
        [DsAttributeProperty("W")]
        public float W
        {
            get { return m_fVector[3]; }
            set
            {
                if (m_fVector[3] != value)
                {
                    m_fVector[3] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDVector4Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private double[] m_dVector = { 678.21d, -14.9d, -0.21d, -0.35d };

        public DsDVector4Attribute()
            : base(DsAttr.Dvector4)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[0]; }
            set
            {
                if (m_dVector[0] != value)
                {
                    m_dVector[0] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[1]; }
            set
            {
                if (m_dVector[1] != value)
                {
                    m_dVector[1] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Z value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return m_dVector[2]; }
            set
            {
                if (m_dVector[2] != value)
                {
                    m_dVector[2] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the W value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("W")]
        [DsAttributeProperty("W")]
        public double W
        {
            get { return m_dVector[3]; }
            set
            {
                if (m_dVector[3] != value)
                {
                    m_dVector[3] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Set all values in the vector at once
        /// </summary>
        /// <param name="dX">X, the first value</param>
        /// <param name="dY">Y, the second value</param>
        /// <param name="dZ">Z, the third value</param>
        /// <param name="dW">W, the fourth value</param>
        public void Set(double? dX, double? dY, double? dZ, double? dW)
        {
            bool bAnythingChanged = false;
            if (dX.HasValue && dX.Value != m_dVector[0]) { m_dVector[0] = dX.Value; bAnythingChanged = true; }
            if (dY.HasValue && dY.Value != m_dVector[1]) { m_dVector[1] = dY.Value; bAnythingChanged = true; }
            if (dZ.HasValue && dZ.Value != m_dVector[2]) { m_dVector[2] = dZ.Value; bAnythingChanged = true; }
            if (dW.HasValue && dW.Value != m_dVector[3]) { m_dVector[3] = dW.Value; bAnythingChanged = true; }
            if (bAnythingChanged)
                UpdateHost();
        }

        /// <summary>
        /// Apply delta to all values in the vector at once
        /// </summary>
        /// <param name="dX">X, the first value</param>
        /// <param name="dY">Y, the second value</param>
        /// <param name="dZ">Z, the third value</param>
        /// <param name="dW">W, the fourth value</param>
        public void Add(double? dX, double? dY, double? dZ, double? dW, double dMin, double dMax)
        {
            bool bAnythingChanged = false;
            if (dX.HasValue && dX.Value != 0.0) { m_dVector[0] = MathUtilities.Clamp(m_dVector[0] + dX.Value, dMin, dMax); bAnythingChanged = true; }
            if (dY.HasValue && dY.Value != 0.0) { m_dVector[1] = MathUtilities.Clamp(m_dVector[1] + dY.Value, dMin, dMax); bAnythingChanged = true; }
            if (dZ.HasValue && dZ.Value != 0.0) { m_dVector[2] = MathUtilities.Clamp(m_dVector[2] + dZ.Value, dMin, dMax); bAnythingChanged = true; }
            if (dW.HasValue && dW.Value != 0.0) { m_dVector[3] = MathUtilities.Clamp(m_dVector[3] + dW.Value, dMin, dMax); bAnythingChanged = true; }
            if (bAnythingChanged)
                UpdateHost();
        }

        /// <summary>
        /// Apply delta to the magnitude of the value.
        /// A positive value increases the magnitude of the value
        /// A negative value decreases the magnitude of the value
        /// </summary>
        private double MagnitudeAdd(double dValue, double dDelta, double dMax)
        {
            double dSign = Math.Sign(dValue);

            if (dSign < 0.0)
                return MathUtilities.Clamp(dValue - dDelta, -dMax, 0.0);
            else
                return MathUtilities.Clamp(dValue + dDelta, 0.0, dMax);
        }

        /// <summary>
        /// Apply delta to all values of the vector magnitude at once.
        /// A positive value increases the magnitude of the vector in that direction
        /// A negative value decreases the magnitude of the vector in that direction.
        /// </summary>
        /// <param name="dX">X, the first value</param>
        /// <param name="dY">Y, the second value</param>
        /// <param name="dZ">Z, the third value</param>
        /// <param name="dW">W, the fourth value</param>
        public void MagnitudeAdd(double? dX, double? dY, double? dZ, double? dW, double dMax)
        {
            bool bAnythingChanged = false;
            if (dX.HasValue && dX.Value != 0.0) { m_dVector[0] = MagnitudeAdd(m_dVector[0], dX.Value, dMax); bAnythingChanged = true; }
            if (dY.HasValue && dY.Value != 0.0) { m_dVector[1] = MagnitudeAdd(m_dVector[1], dY.Value, dMax); bAnythingChanged = true; }
            if (dZ.HasValue && dZ.Value != 0.0) { m_dVector[2] = MagnitudeAdd(m_dVector[2], dZ.Value, dMax); bAnythingChanged = true; }
            if (dW.HasValue && dW.Value != 0.0) { m_dVector[3] = MagnitudeAdd(m_dVector[3], dW.Value, dMax); bAnythingChanged = true; }
            if (bAnythingChanged)
                UpdateHost();
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsStringAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_STRING_LENGTH)]
        private string m_sTheString = "hello world";        

        public DsStringAttribute()
            : base(DsAttr.String)
        { }

        /// <summary>
        /// Gets/sets the raw string value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public string String
        {
            get { return m_sTheString; }
            set
            {
                if (m_sTheString != value)
                {
                    m_sTheString = value;
                    UpdateHost("String", "Value");
                }
            }
        }

        /// <summary>
        /// Gets/sets the value of the string. This formats the vertical | as a \r\n 
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public string Value
        {
            get { return m_sTheString.Replace("|", "\r\n"); }
            set
            {
                string sConvertedValue = value.Replace("\r\n", "|").Replace('\n', '|');
                if (m_sTheString != sConvertedValue)
                {
                    m_sTheString = sConvertedValue;
                    UpdateHost("String", "Value");
                }
            }
        }


        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsCommandStringAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_STRING_LENGTH)]
        private string m_sCommandString = "system message \"Hello world\"";

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sDirectory = "";

        public DsCommandStringAttribute()
            : base(DsAttr.CommandString)
        { }

        /// <summary>
        /// Gets/sets the raw string value
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public string String
        {
            get { return m_sCommandString; }
            set
            {
                if (m_sCommandString != value)
                {
                    m_sCommandString = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the value of the string. This formats the vertical | as a \r\n 
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public string Value
        {
            get { return m_sCommandString.Replace("|", "\r\n"); }
            set
            {
                string sConvertedValue = value.Replace("\r\n", "|").Replace('\n', '|');
                if (m_sCommandString != sConvertedValue)
                {
                    m_sCommandString = sConvertedValue;
                    UpdateHost();
                }
            }
        }


        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_byObjectID =  1;  // if TRUE, nID is an object ID, otherwise its a classID
        private Int16 m_nObjectID = -1;   // either object or class ID, initialized to -1 indicating neither!

        public DsNameAttribute()
            : base(DsAttr.Name)
        { }

        /// <summary>
        /// Gets/sets whether the id is for an object or class (boolean)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public bool IsObject
        {
            get { return Convert.ToBoolean(m_byObjectID); }
            // Don't update the host with this flag -- wait until the object
            // id or class id is received to send the attribute to the host.
            private set { m_byObjectID = Convert.ToByte(value); }
        }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; IsObject = true; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the class id
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ClassID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; IsObject = false; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the name of the object or class, using DsAPI to get it.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public string NameString
        {
            get
            {
                StringBuilder sName = new StringBuilder(DsConstants.MAX_NAME_LENGTH);

                // If this name is represented by an object id, get the object name
                if (IsObject)
                {
                    // Get the object name from the object ID
                    if (m_nObjectID >= 0)
                    {
                        DsAPI.DsGetObjectName(m_nObjectID, sName, DsConstants.MAX_NAME_LENGTH);
                    }
                    return sName.ToString();
                }
                    // Otherwise, the name is represented by a class id, so get the class name
                else
                {
                    // Get the class name from the object ID
                    if (m_nObjectID >= 0)
                    {
                        DsAPI.DsGetClassName(m_nObjectID, sName, DsConstants.MAX_NAME_LENGTH);
                    }
                    return sName.ToString();
                }
            }
            set
            {
                // KBW TODO - What if it's a class ID rather than an object ID?  
                //            This still needs to be addressed.

                Int16 nObjectID = -1;
                DsAPI.DsGetObjectID(value, out nObjectID);

                // For now, if we don't have a valid objectID, don't set it
                if (nObjectID == -1)
                    return;

                // Find the class ID of the object
                Int16 nClassID = -1;
                DsAPI.DsGetObjectClassID(nObjectID, out nClassID);
                // If there isn't a class ID for the object, something went
                // terribly wrong!
                if (nClassID == -1)
                    return;

                // Find the class type of the class ID
                DsClassType eClassType = DsClassType.Undefined;
                DsAPI.DsGetClassType(nClassID, out eClassType);

                // Set the object ID
                m_nObjectID = nObjectID; IsObject = true; UpdateHost();
            }
        }


        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsSystemObjectAttribute : DsAttribute, INotifyPropertyChanged
    {
        private Int16 m_nObjectID = -1;
        private Int16 m_nClassID = -1;
        private DsObjectType m_nObjectType = DsObjectType.Undefined;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_NAME_LENGTH)]
        private string m_sObjectName = "";

        public DsSystemObjectAttribute()
            : base(DsAttr.SystemObject)
        { }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("ObjectID")]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the class id
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("ClassID")]
        public Int16 ClassID
        {
            get { return m_nClassID; }
            set { m_nClassID = value; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the object type
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("ObjectType")]
        public DsObjectType ObjectType
        {
            get { return m_nObjectType; }
            set { m_nObjectType = value; UpdateHost(); }
        }
        /// <summary>
        /// Gets the name of the object
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("ObjectName")]
        public string ObjectName
        {
            get { return m_sObjectName; }
            set { m_sObjectName = value; UpdateHost(); }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsHandleAttribute : DsAttribute, INotifyPropertyChanged
    {
        private Int16 m_nObjectID = -1;   // either object or class ID, initialized to -1 indicating neither!
        private Int16 m_nAttrIndex = -1;
        private Int16 m_nElemIndex = -1;
        private DsAttrPart m_eAttributePart = DsAttrPart.AttrPartNone;
        private DsDistanceUnit m_distanceUnit = DsDistanceUnit.Undefined;


        public DsHandleAttribute()
            : base(DsAttr.Handle)
        { }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>NEH</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the attribute index
        /// </summary>
        /// <author>NEH</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 AttributeIndex
        {
            get { return m_nAttrIndex; }
            set { m_nAttrIndex = value; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the element index
        /// </summary>
        /// <author>NEH</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ElementIndex
        {
            get { return m_nElemIndex; }
            set { m_nElemIndex = value; UpdateHost(); }
        }

        /// <summary>
        /// Gets / sets the attribute part (x, y, z)
        /// </summary>
        public DsAttrPart AttributePart
        {
            get { return m_eAttributePart; }
            set { m_eAttributePart = value; UpdateHost(); }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsTextureNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte   m_byTextureFromObject = 0;    // if TRUE, texture comes from an object instead of a file.
        private Int16  m_nObjectID = -1;		     // if bTextureFromObject is TRUE, this is the object to get texture from.
        private int    m_nTextureProperties = -1;    // texture properties
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";

        public DsTextureNameAttribute()
            : base(DsAttr.TextureName)
        { }        

        /// <summary>
        /// Gets/sets whether the texture is from an object (boolean)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public bool IsTextureFromObject
        {
            get { return Convert.ToBoolean(m_byTextureFromObject); }
            // Don't update the host with this flag -- wait until the object
            // id or texture name is received to send the attribute to the host.
            private set { m_byTextureFromObject = Convert.ToByte(value); }
        }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; IsTextureFromObject = true; m_sName = ""; UpdateUI(); UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the object name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ObjectName")]
        public string ObjectName
        {
            get
            {
                if (IsTextureFromObject)
                {
                    StringBuilder sObjectName = new StringBuilder(DsConstants.MAX_NAME_LENGTH);

                    // Get the object name from the object ID
                    if (m_nObjectID >= 0)
                    {
                        DsAPI.DsGetObjectName(m_nObjectID, sObjectName, DsConstants.MAX_NAME_LENGTH);
                        return sObjectName.ToString();
                    }
                    else return "";
                }
                else
                    return "";
            }
            set
            {
                Int16 nObjectID = -1;
                DsAPI.DsGetObjectID(value, out nObjectID);

                // For now, if we don't have a valid objectID, use the texture path name
                if (nObjectID == -1)
                {
                    IsTextureFromObject = false; 
                    m_nObjectID = -1;
                    UpdateUI();
                    UpdateHost();
                    return;
                }

                // Find the class ID of the object
                Int16 nClassID = -1;
                DsAPI.DsGetObjectClassID(nObjectID, out nClassID);
                // If there isn't a class ID for the object, something went
                // terribly wrong!
                if (nClassID == -1)
                {
                    IsTextureFromObject = false;
                    m_nObjectID = -1;
                    UpdateUI();
                    UpdateHost();
                    return;
                }

                // Find the class type of the class ID
                DsClassType eClassType = DsClassType.Undefined;
                DsAPI.DsGetClassType(nClassID, out eClassType);

                // If the class type is image, set the object ID
                if (eClassType == DsClassType.TextureResource || nClassID == (short)DsClass.DomeVideo)
                {
                    m_nObjectID = nObjectID;
                    IsTextureFromObject = true;
                    m_sName = "";
                }
                else
                {
                    IsTextureFromObject = false;
                    m_nObjectID = -1;
                }
                UpdateUI();
                UpdateHost();

            }
        }

        /// <summary>
        /// Gets/sets the texture name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("TextureName")]
        public string TextureName
        {
            get { return m_sName; }
            set { m_sName = value; IsTextureFromObject = false; m_nObjectID = -1; UpdateUI(); UpdateHost(); }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsTerrainNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_byTerrainFromObject = 0;    // if TRUE, terrain comes from an object instead of a file.
        private Int16 m_nObjectID = -1;		     // if bTerrainFromObject is TRUE, this is the object to get terrain from.
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";

        public DsTerrainNameAttribute()
            : base(DsAttr.TerrainName)
        { }

        /// <summary>
        /// Gets/sets whether the terrain is from an object (boolean)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public bool IsTerrainFromObject
        {
            get { return Convert.ToBoolean(m_byTerrainFromObject); }
            // Don't update the host with this flag -- wait until the object
            // id or terrain name is received to send the attribute to the host.
            private set { m_byTerrainFromObject = Convert.ToByte(value); }
        }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; IsTerrainFromObject = true; m_sName = ""; UpdateUI(); UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the object name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ObjectName")]
        public string ObjectName
        {
            get
            {
                if (IsTerrainFromObject)
                {
                    StringBuilder sObjectName = new StringBuilder(DsConstants.MAX_NAME_LENGTH);

                    // Get the object name from the object ID
                    if (m_nObjectID >= 0)
                    {
                        DsAPI.DsGetObjectName(m_nObjectID, sObjectName, DsConstants.MAX_NAME_LENGTH);
                        return sObjectName.ToString();
                    }
                    else return "";
                }
                else
                    return "";
            }
            set
            {
                Int16 nObjectID = -1;
                DsAPI.DsGetObjectID(value, out nObjectID);

                // For now, if we don't have a valid objectID, use the terrain path name
                if (nObjectID == -1)
                {
                    IsTerrainFromObject = false;
                    m_nObjectID = -1;
                    UpdateUI();
                    UpdateHost();
                    return;
                }

                // Find the class ID of the object
                Int16 nClassID = -1;
                DsAPI.DsGetObjectClassID(nObjectID, out nClassID);
                // If there isn't a class ID for the object, something went
                // terribly wrong!
                if (nClassID == -1)
                {
                    IsTerrainFromObject = false;
                    m_nObjectID = -1;
                    UpdateUI();
                    UpdateHost();
                    return;
                }

                // Find the class type of the class ID
                DsClassType eClassType = DsClassType.Undefined;
                DsAPI.DsGetClassType(nClassID, out eClassType);

                // If the class type is image, set the object ID
                if (eClassType == DsClassType.TerrainResource)
                {
                    m_nObjectID = nObjectID;
                    IsTerrainFromObject = true;
                    m_sName = "";
                }
                else
                {
                    IsTerrainFromObject = false;
                    m_nObjectID = -1;
                }
                UpdateUI();
                UpdateHost();

            }
        }

        /// <summary>
        /// Gets/sets the terrain name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("TerrainName")]
        public string TerrainName
        {
            get { return m_sName; }
            set { m_sName = value; IsTerrainFromObject = false; m_nObjectID = -1; UpdateUI(); UpdateHost(); }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDirectoryAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sDirectory = "";        

        public DsDirectoryAttribute()
            : base(DsAttr.Directory)
        { }        

        /// <summary>
        /// Gets/sets the path
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Directory")]
        [DsAttributeProperty("Directory")]
        public string Directory
        {
            get { return m_sDirectory; }
            set
            {
                if (m_sDirectory != value)
                {
                    m_sDirectory = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsPathNameAttribute()
            : base(DsAttr.Pathname)
        { }        

        /// <summary>
        /// Gets/sets the texture name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("PathName")]
        [DsAttributeProperty("PathName")]
        public string PathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsScriptPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsScriptPathNameAttribute()
            : base(DsAttr.ScriptPathname)
        { }

        /// <summary>
        /// Gets/sets the script name (without path)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("ScriptName")]
        public string ScriptName
        {
            get 
            {
                if (string.IsNullOrEmpty(m_sName))
                    return m_sName;

                return Path.GetFileName(m_sName);
            }
         }

        /// <summary>
        /// Gets/sets the script path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ScriptPathName")]
        [DsAttributeProperty("ScriptPathName")]
        public string ScriptPathName
        {
            get { return m_sName; }
            set
            {
                //HostObject hostObject = GUIManager.ObjectManager.GetHostObject(this);
                //// If the name is different, or if the class is a script, update the 
                //// value.
                //if ((m_sName != value) || (hostObject != null) && (hostObject.ClassID == (int)DsClass.Script))
                //{
                    m_sName = value;
                    UpdateHost();
                //}
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsModelNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_byModelFromObject = 1;  // If TRUE, model comes from an object instead of a file
        private Int16 m_nObjectID = -1;			   // If m_byModelFromObject is TRUE, this is the object to get model from        
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsModelNameAttribute()
            : base(DsAttr.ModelName)
        { }

        /// <summary>
        /// Gets/sets whether the model is from an object (boolean)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public bool IsModelFromObject
        {
            get { return Convert.ToBoolean(m_byModelFromObject); }
            // Don't update the host with this flag -- wait until the object
            // id or model name is received to send the attribute to the host.
            private set { m_byModelFromObject = Convert.ToByte(value); }
        }

        /// <summary>
        /// Gets/sets the object id
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Int16 ObjectID
        {
            get { return m_nObjectID; }
            set { m_nObjectID = value; IsModelFromObject = true; UpdateHost(); }
        }

        /// <summary>
        /// Gets/sets the object name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ObjectName")]
        public string ObjectName
        {
            get
            {
                if (IsModelFromObject)
                {
                    StringBuilder sObjectName = new StringBuilder(DsConstants.MAX_NAME_LENGTH);

                    // Get the object name from the object ID
                    if (m_nObjectID >= 0)
                    {
                        DsAPI.DsGetObjectName(m_nObjectID, sObjectName, DsConstants.MAX_NAME_LENGTH);
                    }
                    return sObjectName.ToString();
                }
                else
                    return "";
            }
            set
            {
                Int16 nObjectID = -1;
                DsAPI.DsGetObjectID(value, out nObjectID);

                // For now, if we don't have a valid objectID, don't set it
                if (nObjectID == -1)
                    return;

                // Find the class ID of the object
                Int16 nClassID = -1;
                DsAPI.DsGetObjectClassID(nObjectID, out nClassID);
                // If there isn't a class ID for the object, something went
                // terribly wrong!
                if (nClassID == -1)
                    return;

                // Find the class type of the class ID
                DsClassType eClassType = DsClassType.Undefined;
                DsAPI.DsGetClassType(nClassID, out eClassType);

                // If the class type is Model, set the object ID
   //slp             if (eClassType == DsClassType.Model)
   //                 m_nObjectID = nObjectID; IsModelFromObject = true; UpdateHost();

                // Otherwise, ?  
            }
        }

        /// <summary>
        /// Gets/sets the model name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ModelName")]
        public string ModelName
        {
            get { return m_sName; }
            set { m_sName = value; IsModelFromObject = false;  UpdateHost(); }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsModelPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";

        public DsModelPathNameAttribute()
            : base(DsAttr.ModelPathname)
        { }

        /// <summary>
        /// Gets/sets the model path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ModelPathName")]
        [DsAttributeProperty("ModelPathName")]
        public string ModelPathName
        {
            get { return m_sName; }
            set 
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAudioPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsAudioPathNameAttribute()
            : base(DsAttr.AudioPathname)
        { }        

        /// <summary>
        /// Gets/sets the audio path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("AudioPathName")]
        [DsAttributeProperty("AudioPathName")]
        public string AudioPathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsVideoPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsVideoPathNameAttribute()
            : base(DsAttr.VideoPathname)
        { }        

        /// <summary>
        /// Gets/sets the video path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("VideoPathName")]
        [DsAttributeProperty("VideoPathName")]
        public string VideoPathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsImagePathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private int m_nTextureProperties = -1;    // texture properties
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";        

        public DsImagePathNameAttribute()
            : base(DsAttr.ImagePathname)
        { }        

        /// <summary>
        /// Gets/sets the image path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("ImagePathName")]
        [DsAttributeProperty("ImagePathName")]
        public string ImagePathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsSlidesetPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";

        public DsSlidesetPathNameAttribute()
            : base(DsAttr.SlidesetPathname)
        { }

        /// <summary>
        /// Gets/sets the image path name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("SlidesetPathName")]
        [DsAttributeProperty("SlidesetPathName")]
        public string SlidesetPathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsTerrainPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sName = "";

        public DsTerrainPathNameAttribute()
            : base(DsAttr.TerrainPathname)
        { }

        /// <summary>
        /// Gets/sets the texture name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("PathName")]
        [DsAttributeProperty("PathName")]
        public string PathName
        {
            get { return m_sName; }
            set
            {
                if (m_sName != value)
                {
                    m_sName = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsJsPathNameAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_byPermanent = 0;    // if TRUE, the JS script does not end at fade/stop/reset
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_PATH_LENGTH)]
        private string m_sPathname = "";
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_JS_PARAMETER_LENGTH)]
        private string m_sParameters = "";

        public DsJsPathNameAttribute()
            : base(DsAttr.JsPathname)
        { }


        /// <summary>
        /// Gets/sets whether the script is to be triggered as permanent
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public bool IsPermanent
        {
            get { return Convert.ToBoolean(m_byPermanent); }
            set 
            { 
                m_byPermanent = Convert.ToByte(value);
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets the path name
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("PathName")]
        [DsAttributeProperty("PathName")]
        public string PathName
        {
            get { return m_sPathname; }
            set
            {
                if (m_sPathname != value)
                {
                    m_sPathname = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the parameters
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Parameters")]
        [DsAttributeProperty("Parameters")]
        public string Parameters
        {
            get { return m_sParameters; }
            set
            {
                if (m_sParameters != value)
                {
                    m_sParameters = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsFontAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DsConstants.MAX_STRING_LENGTH)]
        private string m_sFont = "";        

        public DsFontAttribute()
            : base(DsAttr.Font)
        { }        

        /// <summary>
        /// Gets/sets the font name
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public string Value
        {
            get { return m_sFont; }
            set
            {
                if (m_sFont != value)
                {
                    m_sFont = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsPositionAttribute : DsAttribute, INotifyPropertyChanged
    {        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector  = { 1,  2,  3 };
        private DsDistanceUnit m_distanceUnit = DsDistanceUnit.Meter;
        private DsPositionMode m_positionMode = DsPositionMode.Cartesian;        

        public DsPositionAttribute()
            : base(DsAttr.Position)
        { }

        /// <summary>
        /// Gets/sets the position mode.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        public DsPositionMode PositionMode
        {
            get { return m_positionMode; }
            set
            {
                if (m_positionMode != value)
                {
                    m_positionMode = value;

                    UpdateHost("Azimuth", "DeclinationDegrees", "DeclinationMinutes", "DeclinationSeconds",
                        "TotalDeclinationDegrees", "TotalDeclinationMinutes", "TotalDeclinationSeconds",
                        "Distance", "Elevation", "Forward", "PositionMode",
                        "RAHours", "RAMinutes", "RASeconds",
                        "TotalRAHours", "TotalRAMinutes", "TotalRASeconds",
                        "EclipticLongitude", "EclipticLatitude",
                        "GalacticLongitude", "GalacticLatitude",
                        "Right", "Up",
                        "X", "Y", "Z");
                    // update the host with the new mode
                }
            }
        }

        /// <summary>
        /// Gets/sets the distance unit.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW,JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DsDistanceUnit DistanceUnit
        {
            get { return m_distanceUnit; }
            set
            {
                if (m_distanceUnit != value)
                {
                    m_distanceUnit = value;
                    UpdateHost("X", "Y", "Z", "Distance", "DistanceUnit");
                }
            }
        }

        /// <summary>
        /// Gets/sets X component.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.X], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.X] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.X] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets Y component.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Y], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.Y] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.Y] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets Z component.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Z], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dVector[(int)VectorIndexType.Z] != dValueMeters)
                {
                    m_dVector[(int)VectorIndexType.Z] = dValueMeters;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Get the astronomical value for the specified part type.
        /// </summary>
        /// <param name="astroPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private double GetCelestialPositionPart(CelestialPartType astroPartType)
        {
            // Convert cartesian coordinates into astronomical coordinates...
            Vector3D cartVector = new Vector3D();
            cartVector.dX = m_dVector[(int)VectorIndexType.X];
            cartVector.dY = m_dVector[(int)VectorIndexType.Y];
            cartVector.dZ = m_dVector[(int)VectorIndexType.Z];
            double dRA = 0.0;
            double dDeclination = 0.0;
            double dDistance = 0.0;
            DsAPI.DsPosCartesianToCelestial(cartVector, out dRA, out dDeclination, out dDistance);

            // If distance is close to 0, the conversion is garbage, 
            // so return the extra stored values
            if (Math.Abs(dDistance) < 1e-8)
            {
                dRA = m_dRA;
                dDeclination = m_dDec;
            }

            switch (astroPartType)
            {
                case CelestialPartType.RA: return dRA;
                case CelestialPartType.Declination: return dDeclination;
                default: return 0;
            }
        }

        /// <summary>
        /// Gets the position in astronomical coordinates.
        /// </summary>
        /// <param name="astroPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void GetCelestialPosition_Internal(ref double dRA, ref double dDeclination, ref double dDistance)
        {
            // Convert cartesian coordinates into astronomical coordinates...
            Vector3D cartVector = new Vector3D();
            cartVector.dX = m_dVector[(int)VectorIndexType.X];
            cartVector.dY = m_dVector[(int)VectorIndexType.Y];
            cartVector.dZ = m_dVector[(int)VectorIndexType.Z];
            DsAPI.DsPosCartesianToCelestial(cartVector, out dRA, out dDeclination, out dDistance);

            // If distance is close to 0, the conversion is garbage, 
            // so return the extra stored values
            if (Math.Abs(dDistance) < 1e-8)
            {
                dRA = m_dRA;
                dDeclination = m_dDec;
            }
        }

        /// <summary>
        /// Sets the position given.
        /// </summary>
        /// <param name="dX"></param>
        /// <param name="dY"></param>
        /// <param name="dZ"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetCartesianPosition_Internal(double dX, double dY, double dZ)
        {
            m_dVector[(int)VectorIndexType.X] = dX;
            m_dVector[(int)VectorIndexType.Y] = dY;
            m_dVector[(int)VectorIndexType.Z] = dZ;

            UpdateHost();
        }

        /// <summary>
        /// Sets the Cartesian Position x,y,z and distance unit all together.
        /// </summary>
        /// <param name="dX"></param>
        /// <param name="dY"></param>
        /// <param name="dZ"></param>
        /// <param name="distanceUnit"></param>
        public void SetCartesianPosition(double dX, double dY, double dZ, DsDistanceUnit distanceUnit)
        {
            m_distanceUnit = distanceUnit;
            m_positionMode = DsPositionMode.Cartesian;
            SetCartesianPosition_Internal(
                MathUtilities.ConvertDistanceToMeters(dX, distanceUnit),
                MathUtilities.ConvertDistanceToMeters(dY, distanceUnit),
                MathUtilities.ConvertDistanceToMeters(dZ, distanceUnit));
        }

        /// <summary>
        /// Gets the position in spherical coordinates.
        /// </summary>
        /// <param name="astroPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void GetSphericalPosition_Internal(ref double dAzimuth, ref double dElevation, ref double dDistance)
        {
            // Convert cartesian coordinates into spherical coordinates...
            Vector3D cartVector = new Vector3D();
            cartVector.dX = m_dVector[(int)VectorIndexType.X];
            cartVector.dY = m_dVector[(int)VectorIndexType.Y];
            cartVector.dZ = m_dVector[(int)VectorIndexType.Z];
            DsAPI.DsPosCartesianToSpherical(cartVector, out dAzimuth, out dElevation, out dDistance);

            // If distance is close to 0, the conversion is garbage, 
            // so return the extra stored values
            if (Math.Abs(dDistance) < 1e-8)
            {
                dAzimuth = m_dAz;
                dElevation = m_dEl;
            }
        }

        /// <summary>
        /// Get the right ascension value for the specified part type.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private int GetRightAscension(RAPartType raPartType)
        {
            // Convert right ascension radians into hours, minutes, seconds
            int nHours = 0;
            int nMinutes = 0;
            int nSeconds = 0;
            MathUtilities.RadToHMS(GetCelestialPositionPart(CelestialPartType.RA), ref nHours, ref nMinutes, ref nSeconds);

            switch (raPartType)
            {
                case RAPartType.Hours:   return nHours;   
                case RAPartType.Minutes: return nMinutes; 
                case RAPartType.Seconds: return nSeconds;
                default: return 0;
            }
        }

        /// <summary>
        /// Get the total right ascension value for the specified part type.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>JMG</author>
        private double GetTotalRightAscension(RAPartType raPartType)
        {
            // Convert right ascension radians into hours, minutes, seconds
            double dDegrees = MathUtilities.RadToDeg(GetCelestialPositionPart(CelestialPartType.RA));

            switch (raPartType)
            {
                case RAPartType.Hours: return dDegrees / 15.0;
                case RAPartType.Minutes: return dDegrees * 4.0;
                case RAPartType.Seconds: return dDegrees * 240.0;
                default: return 0;
            }
        }

        /// <summary>
        /// Set the right ascension value for the specified part type.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetRightAscension(RAPartType raPartType, int nValue)
        {
            double dRA = 0.0;
            double dDeclination = 0.0;
            double dDistance = 0.0;

            int nHours = 0;
            int nMinutes = 0;
            int nSeconds = 0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert the right ascension (in radians) into hours, minutes and seconds.
            MathUtilities.RadToHMS(dRA, ref nHours, ref nMinutes, ref nSeconds);
            
            // Overwrite the appropriate part type
            switch (raPartType)
            {
                case RAPartType.Hours:   nHours   = nValue; break;
                case RAPartType.Minutes: nMinutes = nValue; break;
                case RAPartType.Seconds: nSeconds = nValue; break;
                default: break;
            }

            // Convert new right ascension back into radians.
            dRA = MathUtilities.HMSToRad(nHours, nMinutes, nSeconds);

            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRA, dDeclination, dDistance);
         }

        /// <summary>
        /// Set the right ascension value as a decimal.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetRightAscension(double dDecRAHours)
        {
            double dRA = 0.0;
            double dDeclination = 0.0;
            double dDistance = 0.0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert decimal hours into radians
            double dRARad = MathUtilities.HoursToRad(dDecRAHours);

            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRARad, dDeclination, dDistance);
        }


        /// <summary>
        /// Set the right ascension total in the specified part type.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <param name="dValue">The value comming in.</param>
        /// <returns></returns>
        /// <author>JMG</author>
        private void SetTotalRightAscension(RAPartType raPartType, double dValue)
        {
            double dRA = 0.0;
            double dDeclination = 0.0;
            double dDistance = 0.0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert to hours, then to radians
            double dRAHours = 0.0;
            switch (raPartType)
            {
                case RAPartType.Hours: dRAHours = dValue; break;
                case RAPartType.Minutes: dRAHours = dValue / 60.0; break;
                case RAPartType.Seconds: dRAHours = dValue / 3600.0; break;
                default: break;
            }
            double dRARad = MathUtilities.HoursToRad(dRAHours);

            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRARad, dDeclination, dDistance);
        }

        /// <summary>
        /// Gets/sets the right ascension hours.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("RAHours")]
        public double RAHours
        {
            get { return (double)GetRightAscension(RAPartType.Hours); }
            set
            {
                if ((int)value == value)
                    SetRightAscension(RAPartType.Hours, (int)value);
                else
                    SetRightAscension(value);
            }
        }

        /// <summary>
        /// Gets/sets the right ascension minutes.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("RAMinutes")]
        public int RAMinutes
        {
            get { return GetRightAscension(RAPartType.Minutes); }
            set { SetRightAscension(RAPartType.Minutes, value); }
        }

        /// <summary>
        /// Gets/sets the right ascension seconds.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("RASeconds")]
        public int RASeconds
        {
            get { return GetRightAscension(RAPartType.Seconds); }
            set { SetRightAscension(RAPartType.Seconds, value); }
        }

        /// <summary>
        /// Gets/sets the total right ascension hours.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalRAHours
        {
            get { return GetTotalRightAscension(RAPartType.Hours); }
            set { SetTotalRightAscension(RAPartType.Hours, value); }
        }

        /// <summary>
        /// Gets/sets the total right ascension minutes.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalRAMinutes
        {
            get { return GetTotalRightAscension(RAPartType.Minutes); }
            set { SetTotalRightAscension(RAPartType.Minutes, value); }
        }

        /// <summary>
        /// Gets/sets the total right ascension seconds.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalRASeconds
        {
            get { return GetTotalRightAscension(RAPartType.Seconds); }
            set { SetTotalRightAscension(RAPartType.Seconds, value); }
        }

        /// <summary>
        /// Get the declination value for the associated part type.
        /// </summary>
        /// <param name="decPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private double GetDeclination(DecPartType decPartType)
        {
            // Convert declination radians into degrees, minutes, seconds
            double nDegrees = 0;
            int nMinutes = 0;
            int nSeconds = 0;
            MathUtilities.RadToDMS(GetCelestialPositionPart(CelestialPartType.Declination), ref nDegrees, ref nMinutes, ref nSeconds);

            switch (decPartType)
            {
                case DecPartType.Degrees: return nDegrees;
                case DecPartType.Minutes: return (double)nMinutes; 
                case DecPartType.Seconds: return (double)nSeconds;
                default: return 0;
            }
        }

        /// <summary>
        /// Get the total declination value for the associated part type.
        /// </summary>
        /// <param name="decPartType"></param>
        /// <returns></returns>
        /// <author>JMG</author>
        private double GetTotalDeclination(DecPartType decPartType)
        {
            // Convert declination radians into degrees.
            double dDegrees = MathUtilities.RadToDeg(GetCelestialPositionPart(CelestialPartType.Declination));

            switch (decPartType)
            {
                case DecPartType.Degrees: return dDegrees;
                case DecPartType.Minutes: return dDegrees * 60.0;
                case DecPartType.Seconds: return dDegrees * 3600.0;
                default: return 0;
            }
        }

        /// <summary>
        /// Set the declination value for the specified part type.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetDeclination(DecPartType decPartType, double nValue)
        {
            double dRA          = 0;
            double dDeclination = 0;
            double dDistance    = 0;

            double nDegrees   = 0;
            int nMinutes   = 0;
            int nSeconds   = 0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert the declination (in radians) into degrees, minutes and seconds.
            MathUtilities.RadToDMS(dDeclination, ref nDegrees, ref nMinutes, ref nSeconds);

             // Overwrite the appropriate part type
            switch (decPartType)
            {
                case DecPartType.Degrees: nDegrees = nValue; break;
                case DecPartType.Minutes: nMinutes = (int)nValue; break;
                case DecPartType.Seconds: nSeconds = (int)nValue; break;
                default: break;
            }

            dDeclination = MathUtilities.DMSToRad(nDegrees, nMinutes, nSeconds);

            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRA, dDeclination, dDistance);
        }

        /// <summary>
        /// Set the declination value as a decimal.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetDeclination(double dDecDeclination)
        {
            double dRA = 0;
            double dDeclination = 0;
            double dDistance = 0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert decimal declination into radians.
            double dDeclinationRad = MathUtilities.DegToLimitedRad(dDecDeclination);
 
            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRA, dDeclinationRad, dDistance);
        }
        
        /// <summary>
        /// Set the right ascension total in the specified part type.
        /// </summary>
        /// <param name="decPartType"></param>
        /// <param name="dValue">The value comming in.</param>
        /// <returns></returns>
        /// <author>JMG</author>
        private void SetTotalDeclination(DecPartType decPartType, double dValue)
        {
            double dRA = 0.0;
            double dDeclination = 0.0;
            double dDistance = 0.0;

            // Get the current astronomical position.
            GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

            // Convert to degrees, then to radians
            double dDecDegrees = 0.0;
            switch (decPartType)
            {
                case DecPartType.Degrees: dDecDegrees = dValue; break;
                case DecPartType.Minutes: dDecDegrees = dValue / 60.0; break;
                case DecPartType.Seconds: dDecDegrees = dValue / 3600.0; break;
                default: break;
            }
            double dDeclinationRad = MathUtilities.DegToLimitedRad(dDecDegrees);

            // Set the cartesian coordinates (from astronomical radians)
            SetCelestialPosition_Internal(dRA, dDeclinationRad, dDistance);
        }

        /// <summary>
        /// Set the celestial position from radian values.
        /// </summary>
        /// <param name="dRA"></param>
        /// <param name="dDeclination"></param>
        /// <param name="dDistance"></param>
        /// <author>KBW</author>
        private void SetCelestialPosition_Internal(double dRA, double dDeclination, double dDistance)
        {
            // Convert astronomical coordinates (in radians) into cartesian coordinates...
            Vector3D cartVector = new Vector3D();
            DsAPI.DsPosCelestialToCartesian(dRA, dDeclination, dDistance, out cartVector);

            // Save these values in case the distance goes to zero
            // so we can preserve the angles.
            m_dRA = dRA;
            m_dDec = dDeclination;

            // And finally set the cartesian coordinates (if they differ from
            // the coordinates already there).
            if ((m_dVector[(int)VectorIndexType.X] != cartVector.dX) ||
                (m_dVector[(int)VectorIndexType.Y] != cartVector.dY) ||
                (m_dVector[(int)VectorIndexType.Z] != cartVector.dZ))
            {
                m_dVector[(int)VectorIndexType.X] = cartVector.dX;
                m_dVector[(int)VectorIndexType.Y] = cartVector.dY;
                m_dVector[(int)VectorIndexType.Z] = cartVector.dZ;

                UpdateUI();
                UpdateHost();
            }
        }

        /// <summary>
        /// This function sets the celestial position in Hours (RA) / Degrees (Declination). 
        /// </summary>
        /// <param name="dRAHours"></param>
        /// <param name="dDeclination"></param>
        /// <param name="dDistance"></param>
        /// <param name="distanceUnit"></param>
        public void SetCelestialPosition(double dRAHours, double dDeclination, double dDistance, DsDistanceUnit distanceUnit)
        {
            m_distanceUnit = distanceUnit;
            m_positionMode = DsPositionMode.Celestial;
            SetCelestialPosition_Internal(
                MathUtilities.HoursToRad(dRAHours),
                MathUtilities.DegToLimitedRad(dDeclination),
                MathUtilities.ConvertDistanceToMeters(dDistance, distanceUnit));
        }

        /// <summary>
        /// Gets/sets the declination degrees.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("DeclinationDegrees")]
        public double DeclinationDegrees
        {
            get { return GetDeclination(DecPartType.Degrees); }
            set
            {
                if ((int)value == value)
                    SetDeclination(DecPartType.Degrees, value);
                else
                    SetDeclination(value);
            }
        }

        /// <summary>
        /// Gets/sets the declination minutes.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("DeclinationMinutes")]
        public int DeclinationMinutes
        {
            get { return (int)GetDeclination(DecPartType.Minutes); }
            set { SetDeclination(DecPartType.Minutes, value); }
        }

        /// <summary>
        /// Gets/sets the declination seconds.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("DeclinationSeconds")]
        public int DeclinationSeconds
        {
            get { return (int)GetDeclination(DecPartType.Seconds); }
            set { SetDeclination(DecPartType.Seconds, value); }
        }

        /// <summary>
        /// Gets/sets the total declination degrees.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalDeclinationDegrees
        {
            get { return GetTotalDeclination(DecPartType.Degrees); }
            set { SetTotalDeclination(DecPartType.Degrees, value); }
        }

        /// <summary>
        /// Gets/sets the total declination minutes.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalDeclinationMinutes
        {
            get { return GetTotalDeclination(DecPartType.Minutes); }
            set { SetTotalDeclination(DecPartType.Minutes, value); }
        }

        /// <summary>
        /// Gets/sets the total declination seconds.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double TotalDeclinationSeconds
        {
            get { return GetTotalDeclination(DecPartType.Seconds); }
            set { SetTotalDeclination(DecPartType.Seconds, value); }
        }

        /// <summary>
        /// Set the distance value.
        /// </summary>
        /// <param name="raPartType"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        private void SetDistance(double dDistanceIn)
        {
            double dDistance = 0;
            if (m_positionMode == DsPositionMode.Celestial)
            {
                double dRA = 0;
                double dDeclination = 0;

                // Get the current astronomical position.
                GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);

                // Overwrite the distance with the new distance
                dDistance = dDistanceIn;

                // Set the cartesian coordinates (from astronomical radians)
                SetCelestialPosition_Internal(dRA, dDeclination, dDistance);
            }
            else if (m_positionMode == DsPositionMode.Spherical)
            {
                double dAz = 0;
                double dEl = 0;
                // Get the current spherical position.
                GetSphericalPosition_Internal(ref dAz, ref dEl, ref dDistance);

                // Overwrite the distance with the new distance
                dDistance = dDistanceIn;

                // Set the cartesian coordinates (from spherical radians)
                SetSphericalPosition_Internal(dAz, dEl, dDistance);
            }
            else if (m_positionMode == DsPositionMode.Ecliptic)
            {
                double dLon = 0;
                double dLat = 0;
                // Get the current ecliptic position.
                GetEclipticPosition_Internal(ref dLon, ref dLat, ref dDistance);

                // Overwrite the distance with the new distance
                dDistance = dDistanceIn;

                // Set the cartesian coordinates (from ecliptic)
                SetEclipticPosition_Internal(dLon, dLat, dDistance);
            }
            else if (m_positionMode == DsPositionMode.Galactic)
            {
                double dLon = 0;
                double dLat = 0;
                // Get the current galactic position.
                GetGalacticPosition_Internal(ref dLon, ref dLat, ref dDistance);

                // Overwrite the distance with the new distance
                dDistance = dDistanceIn;

                // Set the cartesian coordinates (from galactic)
                SetGalacticPosition_Internal(dLon, dLat, dDistance);
            }
        }

        /// <summary>
        /// Set the cartesian coordinates based on spherical radian values.
        /// </summary>
        /// <param name="dAzimuth"></param>
        /// <param name="dElevation"></param>
        /// <param name="dDistance"></param>
        private void SetSphericalPosition_Internal(double dAzimuth, double dElevation, double dDistance)
        {
            // Convert spherical coordinates (in radians) into cartesian coordinates...
            Vector3D cartVector = new Vector3D();
            DsAPI.DsPosSphericalToCartesian(dAzimuth, dElevation, dDistance, out cartVector);

            // Save these values in case the distance goes to zero
            // so we can preserve the angles.
            m_dAz = dAzimuth;
            m_dEl = dElevation;

            // And finally set the cartesian coordinates (if they differ from
            // the coordinates already there).
            if ((m_dVector[(int)VectorIndexType.X] != cartVector.dX) ||
                (m_dVector[(int)VectorIndexType.Y] != cartVector.dY) ||
                (m_dVector[(int)VectorIndexType.Z] != cartVector.dZ))
            {
                m_dVector[(int)VectorIndexType.X] = cartVector.dX;
                m_dVector[(int)VectorIndexType.Y] = cartVector.dY;
                m_dVector[(int)VectorIndexType.Z] = cartVector.dZ;

                UpdateHost();
            }
        }

        /// <summary>
        /// This function sets the spherical position in degrees. 
        /// </summary>
        /// <param name="dAzimuth"></param>
        /// <param name="dElevation"></param>
        /// <param name="dDistance"></param>
        /// <param name="distanceUnit"></param>
        public void SetSphericalPosition(double dAzimuth, double dElevation, double dDistance, DsDistanceUnit distanceUnit)
        {
            m_distanceUnit = distanceUnit;
            m_positionMode = DsPositionMode.Spherical;
            SetSphericalPosition_Internal(
                MathUtilities.DegToLimitedRad(dAzimuth),
                MathUtilities.DegToLimitedRad(dElevation),
                MathUtilities.ConvertDistanceToMeters(dDistance, distanceUnit));
        }

        /// <summary>
        /// Gets/sets the azimuth.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Azimuth")]
        public double Azimuth
        {
            get
            {
                // Get the coordinates as spherical
                double dAzimuth = 0;
                double dElevation = 0;
                double dDistance = 0;
                GetSphericalPosition_Internal(ref dAzimuth, ref dElevation, ref dDistance);

                // Convert radian azimuth into degrees & return
                return MathUtilities.RadToDeg(dAzimuth);
            }
            set
            {
                // Get the coordinates as spherical
                double dAzimuth = 0;
                double dElevation = 0;
                double dDistance = 0;
                GetSphericalPosition_Internal(ref dAzimuth, ref dElevation, ref dDistance);

                // Overwrite the azimuth with the new value
                SetSphericalPosition_Internal(MathUtilities.DegToLimitedRad(value), dElevation, dDistance);
            }
        }

        /// <summary>
        /// Gets/sets the elevation.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Elevation")]
        public double Elevation
        {
            get
            {
                // Get the coordinates as spherical
                double dAzimuth = 0;
                double dElevation = 0;
                double dDistance = 0;
                GetSphericalPosition_Internal(ref dAzimuth, ref dElevation, ref dDistance);

                // Convert radian elevation into degrees & return
                return MathUtilities.RadToDeg(dElevation);
            }
            set
            {
                // Get the coordinates as spherical
                double dAzimuth = 0;
                double dElevation = 0;
                double dDistance = 0;
                GetSphericalPosition_Internal(ref dAzimuth, ref dElevation, ref dDistance);

                // Overwrite the azimuth with the new value
                SetSphericalPosition_Internal(dAzimuth, MathUtilities.DegToLimitedRad(value), dDistance);
            }
        }

        /// <summary>
        /// Gets/sets the relative right coordinate.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Right
        {
            get { return 0.0; }
            set
            {
                m_dVector[(int)VectorIndexType.X] += MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                UpdateHost();
             }
        }

        /// <summary>
        /// Gets/sets the relative forward coordinate.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Forward
        {
            get { return 0.0; }
            set
            {
                m_dVector[(int)VectorIndexType.Y] += MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets the relative up coordinate.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Up
        {
            get { return 0.0; }
            set
            {
                m_dVector[(int)VectorIndexType.Z] += MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets the position coordinates in ecliptic
        /// </summary>
        /// <param name="dLongitude"></param>
        /// <param name="dLatitude"></param>
        /// <param name="dDistance"></param>
        private void GetEclipticPosition_Internal(ref double dLongitude, ref double dLatitude, ref double dDistance)
        {
            // Convert cartesian coordinates into spherical coordinates...
            Vector3D cartVector = new Vector3D();
            cartVector.dX = m_dVector[(int)VectorIndexType.X];
            cartVector.dY = m_dVector[(int)VectorIndexType.Y];
            cartVector.dZ = m_dVector[(int)VectorIndexType.Z];
            DsAPI.DsPosCartesianToEcliptic(cartVector, out dLongitude, out dLatitude, out dDistance);

            // If distance is close to 0, the conversion is garbage, 
            // so return the extra stored values
            if (Math.Abs(dDistance) < 1e-8)
            {
                dLongitude = m_dELon;
                dLatitude = m_dELat;
            }
        }

        /// <summary>
        /// Sets the position coordinates in ecliptic
        /// </summary>
        /// <param name="dLongitude"></param>
        /// <param name="dLatitude"></param>
        /// <param name="dDistance"></param>
        private void SetEclipticPosition_Internal(double dLongitude, double dLatitude, double dDistance)
        {
            // Convert spherical coordinates (in radians) into cartesian coordinates...
            Vector3D cartVector = new Vector3D();
            DsAPI.DsPosEclipticToCartesian(dLongitude, dLatitude, dDistance, out cartVector);

            // Save these values in case the distance goes to zero
            // so we can preserve the angles.
            m_dELon = dLongitude;
            m_dELat = dLatitude;

            // And finally set the cartesian coordinates (if they differ from
            // the coordinates already there).
            if ((m_dVector[(int)VectorIndexType.X] != cartVector.dX) ||
                (m_dVector[(int)VectorIndexType.Y] != cartVector.dY) ||
                (m_dVector[(int)VectorIndexType.Z] != cartVector.dZ))
            {
                m_dVector[(int)VectorIndexType.X] = cartVector.dX;
                m_dVector[(int)VectorIndexType.Y] = cartVector.dY;
                m_dVector[(int)VectorIndexType.Z] = cartVector.dZ;

                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets the Ecliptic Longitude component
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("EclipticLongitude")]
        public double EclipticLongitude
        {
            get
            {
                // Get the coordinates as ecliptic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetEclipticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Convert radian longitude into degrees & return
                return MathUtilities.RadToDeg(dLongitude);
            }
            set
            {
                // Get the coordinates as ecliptic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetEclipticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Overwrite the longitude with the new value
                SetEclipticPosition_Internal(MathUtilities.DegToRad(value), dLatitude, dDistance);
            }
        }

        /// <summary>
        /// Gets/sets the Ecliptic Latitude component
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("EclipticLatitude")]
        public double EclipticLatitude
        {
            get
            {
                // Get the coordinates as ecliptic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetEclipticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Convert radian latitude into degrees & return
                return MathUtilities.RadToDeg(dLatitude);
            }
            set
            {
                // Get the coordinates as ecliptic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetEclipticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Overwrite the latitude with the new value
                SetEclipticPosition_Internal(dLongitude, MathUtilities.DegToLimitedRad(value), dDistance);
            }
        }


        /// <summary>
        /// Gets the position coordinates in galactic
        /// </summary>
        /// <param name="dLongitude"></param>
        /// <param name="dLatitude"></param>
        /// <param name="dDistance"></param>
        private void GetGalacticPosition_Internal(ref double dLongitude, ref double dLatitude, ref double dDistance)
        {
            // Convert cartesian coordinates into spherical coordinates...
            Vector3D cartVector = new Vector3D();
            cartVector.dX = m_dVector[(int)VectorIndexType.X];
            cartVector.dY = m_dVector[(int)VectorIndexType.Y];
            cartVector.dZ = m_dVector[(int)VectorIndexType.Z];
            DsAPI.DsPosCartesianToGalactic(cartVector, out dLongitude, out dLatitude, out dDistance);

            // If distance is close to 0, the conversion is garbage, 
            // so return the extra stored values
            if (Math.Abs(dDistance) < 1e-8)
            {
                dLongitude = m_dGLon;
                dLatitude = m_dGLat;
            }
        }

        /// <summary>
        /// Sets the position coordinates in galactic
        /// </summary>
        /// <param name="dLongitude"></param>
        /// <param name="dLatitude"></param>
        /// <param name="dDistance"></param>
        private void SetGalacticPosition_Internal(double dLongitude, double dLatitude, double dDistance)
        {
            // Convert spherical coordinates (in radians) into cartesian coordinates...
            Vector3D cartVector = new Vector3D();
            DsAPI.DsPosGalacticToCartesian(dLongitude, dLatitude, dDistance, out cartVector);

            // Save these values in case the distance goes to zero
            // so we can preserve the angles.
            m_dGLon = dLongitude;
            m_dGLat = dLatitude;

            // And finally set the cartesian coordinates (if they differ from
            // the coordinates already there).
            if ((m_dVector[(int)VectorIndexType.X] != cartVector.dX) ||
                (m_dVector[(int)VectorIndexType.Y] != cartVector.dY) ||
                (m_dVector[(int)VectorIndexType.Z] != cartVector.dZ))
            {
                m_dVector[(int)VectorIndexType.X] = cartVector.dX;
                m_dVector[(int)VectorIndexType.Y] = cartVector.dY;
                m_dVector[(int)VectorIndexType.Z] = cartVector.dZ;

                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets the Galactic Longitude component
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("GalacticLongitude")]
        public double GalacticLongitude
        {
            get
            {
                // Get the coordinates as galactic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetGalacticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Convert radian longitude into degrees & return
                return MathUtilities.RadToDeg(dLongitude);
            }
            set
            {
                // Get the coordinates as galactic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetGalacticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Overwrite the longitude with the new value
                SetGalacticPosition_Internal(MathUtilities.DegToRad(value), dLatitude, dDistance);
            }
        }

        /// <summary>
        /// Gets/sets the Galactic Latitude component
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("GalacticLatitude")]
        public double GalacticLatitude
        {
            get
            {
                // Get the coordinates as galactic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetGalacticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Convert radian latitude into degrees & return
                return MathUtilities.RadToDeg(dLatitude);
            }
            set
            {
                // Get the coordinates as galactic
                double dLongitude = 0;
                double dLatitude = 0;
                double dDistance = 0;
                GetGalacticPosition_Internal(ref dLongitude, ref dLatitude, ref dDistance);

                // Overwrite the latitude with the new value
                SetGalacticPosition_Internal(dLongitude, MathUtilities.DegToLimitedRad(value), dDistance);
            }
        }


        /// <summary>
        /// Gets/sets distance component.
        /// 
        /// Note:  Stan would like this to be called Distance in Ds rather
        ///        than Range, as it was in D3.  7/23/08
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Distance")]
        public double Distance
        {
            get
            {
                double dDistance = 0;
                switch ((DsPositionMode)PositionMode)
                {
                    case DsPositionMode.Spherical:
                        // Get the current spherical position
                        double dAzimuth = 0;
                        double dElevation = 0;
                        GetSphericalPosition_Internal(ref dAzimuth, ref dElevation, ref dDistance);
                        break;

                    case DsPositionMode.Celestial:
                        // Get the current astronomical position.
                        double dRA = 0.0;
                        double dDeclination = 0.0;
                        GetCelestialPosition_Internal(ref dRA, ref dDeclination, ref dDistance);
                        break;

                    case DsPositionMode.Ecliptic:
                        // Get the current ecliptic position
                        double dELongitude = 0;
                        double dELatitude = 0;
                        GetEclipticPosition_Internal(ref dELongitude, ref dELatitude, ref dDistance);
                        break;

                    case DsPositionMode.Galactic:
                        // Get the current galactic position
                        double dGLongitude = 0;
                        double dGLatitude = 0;
                        GetGalacticPosition_Internal(ref dGLongitude, ref dGLatitude, ref dDistance);
                        break;

                    default:
                        break;
                }
                return MathUtilities.ConvertMetersToUnit(dDistance, m_distanceUnit);
            }
            set { SetDistance(MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit)); }
        }

        #region GUI-only attribute extensions

        // These values are NOT part of the attribute
        // These store the raw RA/Dec/Az and El values,
        // so that when Distance == 0, we can preserve the angle.
        private double m_dRA = 0.0;
        private double m_dDec = 0.0;
        private double m_dAz = 0.0;
        private double m_dEl = 0.0;
        private double m_dELon = 0.0;
        private double m_dELat = 0.0;
        private double m_dGLon = 0.0;
        private double m_dGLat = 0.0;

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsAttitudeAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 4, 5, 6 };
        private DsAttitudeMode m_attitudeMode = DsAttitudeMode.Cartesian;        

        public DsAttitudeAttribute()
            : base(DsAttr.Attitude)
        { }

        /// <summary>
        /// Returns the Raw Vector stored in radians.
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        public Vector3D RawVector
        {
            get { return new Vector3D() { dX = m_dVector[0], dY = m_dVector[1], dZ = m_dVector[2] }; }
        }


        /// <summary>
        /// The attitude mode. 
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        [XmlIgnore]
        public DsAttitudeMode AttitudeMode
        {
            get { return m_attitudeMode; }
            set
            {
                DsAttitudeMode oldMode = m_attitudeMode;
                DsAttitudeMode newMode = value;

                // If this is orbit or relative mode, do nothing
                // because we are not allowed to change it,
                // or change to it!
                if (oldMode == DsAttitudeMode.Orbit ||
                    newMode == DsAttitudeMode.Orbit ||
                    oldMode == DsAttitudeMode.Relative ||
                    newMode == DsAttitudeMode.Relative)
                {
                    // Update so that the combo box returns back
                    UpdateHost("AttitudeMode");
                    return;
                }

                // If we have changed, then the data needs to be converted
                if (oldMode != newMode)
                {
                    m_attitudeMode = newMode;
                    if (oldMode == DsAttitudeMode.Cartesian &&
                        newMode == DsAttitudeMode.Celestial)
                    {
                        double dH = m_dVector[(int)VectorIndexType.X];
                        double dP = m_dVector[(int)VectorIndexType.Y];
                        double dR = m_dVector[(int)VectorIndexType.Z];
                        double dRA, dDec, dHCel;
                        //Do conversion from cartesian to celestial
                        DsAPI.DsAttCartesianToCelestial(
                            dH, dP, dR, out dRA, out dDec, out dHCel);
                        m_dVector[(int)VectorIndexType.X] = dRA;
                        m_dVector[(int)VectorIndexType.Y] = dDec;
                        m_dVector[(int)VectorIndexType.Z] = dHCel;
                    }
                    else if (oldMode == DsAttitudeMode.Celestial &&
                        newMode == DsAttitudeMode.Cartesian)
                    {
                        double dRA = m_dVector[(int)VectorIndexType.X];
                        double dDec = m_dVector[(int)VectorIndexType.Y];
                        double dHCel = m_dVector[(int)VectorIndexType.Z];
                        double dH, dP, dR;
                        //Do conversion from celestial to cartesian
                        DsAPI.DsAttCelestialToCartesian(
                            dRA, dDec, dHCel, out dH, out dP, out dR);
                        m_dVector[(int)VectorIndexType.X] = dH;
                        m_dVector[(int)VectorIndexType.Y] = dP;
                        m_dVector[(int)VectorIndexType.Z] = dR;
                    }
                    UpdateHost("AttitudeMode", "Heading", "Pitch", "Roll", "RA", "Declination", "HeadingCelestial");
                }
            }
        }

        [XmlAttribute("Heading")]
        [DsAttributeProperty("Heading")]
        public double Heading
        {
            get 
            { 
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]); 
            }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        [XmlAttribute("Pitch")]
        [DsAttributeProperty("Pitch")]
        public double Pitch
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        [XmlAttribute("Roll")]
        [DsAttributeProperty("Roll")]
        public double Roll
        {
            get { return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Z]); }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        [DsAttributeProperty("RA")]
        public double RA
        {
            get 
            { 
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]); 
            }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int RAHours
        {
            get
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                return nH;
            }
            set
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                if (nH != value)
                {
                    nH = value;
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.HMSToRad(nH, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int RAMinutes
        {
            get
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                return nM;
            }
            set
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                if (nM != value)
                {
                    nM = value;
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.HMSToRad(nH, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int RASeconds
        {
            get
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                return nS;
            }
            set
            {
                int nH = 0, nM = 0, nS = 0;
                MathUtilities.RadToHMS(m_dVector[(int)VectorIndexType.X], ref nH, ref nM, ref nS);
                if (nS != value)
                {
                    nS = value;
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.HMSToRad(nH, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalRAHours
        {
            get 
            { 
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]) / 15.0; //Convert to hours from degrees 
            }
            set
            {
                double dDegreesValue = value * 15.0; // Convert to degrees from hours
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalRAMinutes
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]) * 4.0; //Convert to HMS minutes from degrees 
            }
            set
            {
                double dDegreesValue = value / 4.0; // Convert to degrees from HMS minutes
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalRASeconds
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.X]) * 240.0; //Convert to HMS seconds from degrees 
            }
            set
            {
                double dDegreesValue = value / 240.0; // Convert to degrees from HMS seconds
                if (m_dVector[(int)VectorIndexType.X] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.X] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        [DsAttributeProperty("Declination")]
        public double Declination
        {
            get 
            { 
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]); 
            }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(value);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double DeclinationDegrees
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                return nD;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                if (BitConverter.DoubleToInt64Bits(value) != BitConverter.DoubleToInt64Bits(nD))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DMSToRad(value, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]
        [ScriptIgnore]
        public int DeclinationMinutes
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                return nM;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                if (nM != value)
                {
                    nM = value;
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DMSToRad(nD, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int DeclinationSeconds
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                return nS;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Y], ref nD, ref nM, ref nS);
                if (nS != value)
                {
                    nS = value;
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DMSToRad(nD, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalDeclinationMinutes
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]) * 60.0; // Convert degrees to minutes
            }
            set
            {
                double dDegreesValue = value / 60.0; // Convert minutes to degrees.
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalDeclinationSeconds
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Y]) * 3600.0; // Convert degrees to seconds
            }
            set
            {
                double dDegreesValue = value / 3600.0; // Convert seconds to degrees.
                if (m_dVector[(int)VectorIndexType.Y] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.Y] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        [DsAttributeProperty("HeadingCelestial")]
        public double HeadingCelestial
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Z]);
            }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != MathUtilities.DegToLimitedRad(value))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DegToLimitedRad(value);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double HeadingCelestialDegrees
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                return nD;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                if (BitConverter.DoubleToInt64Bits(value) != BitConverter.DoubleToInt64Bits(nD))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DMSToRad(value, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int HeadingCelestialMinutes
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                return nM;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                if (nM != value)
                {
                    nM = value;
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DMSToRad(nD, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public int HeadingCelestialSeconds
        {
            get
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                return nS;
            }
            set
            {
                double nD = 0;
                int nM = 0, nS = 0;
                MathUtilities.RadToDMS(m_dVector[(int)VectorIndexType.Z], ref nD, ref nM, ref nS);
                if (nS != value)
                {
                    nS = value;
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DMSToRad(nD, nM, nS);
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalHeadingCelestialMinutes
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Z]) * 60.0; // Convert degrees to minutes
            }
            set
            {
                double dDegreesValue = value / 60.0; // Convert minutes to degrees.
                if (m_dVector[(int)VectorIndexType.Z] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        public double TotalHeadingCelestialSeconds
        {
            get
            {
                return MathUtilities.RadToDeg(m_dVector[(int)VectorIndexType.Z]) * 3600.0; // Convert degrees to seconds
            }
            set
            {
                double dDegreesValue = value / 3600.0; // Convert seconds to degrees.
                if (m_dVector[(int)VectorIndexType.Z] != MathUtilities.DegToLimitedRad(dDegreesValue))
                {
                    m_dVector[(int)VectorIndexType.Z] = MathUtilities.DegToLimitedRad(dDegreesValue);
                    UpdateUI();
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsScaleAttribute : DsAttribute, INotifyPropertyChanged
    {
        // Should this be limited to positive values (Ufloat or Udouble?)
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 7, 8, 9 };
        private DsDistanceUnit m_distanceUnit = DsDistanceUnit.Meter;

        // Save off the ratios if uniform is desired.
        private double m_dYOverX = 0;
        private double m_dZOverX = 0;
        private double m_dXOverY = 0;
        private double m_dZOverY = 0;
        private double m_dXOverZ = 0;
        private double m_dYOverZ = 0;

        public DsScaleAttribute()
            : base(DsAttr.Scale)
        { }

        /// <summary>
        /// Gets/sets the distance unit.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW,JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DsDistanceUnit DistanceUnit
        {
            get { return m_distanceUnit; }
            set
            {
                if (m_distanceUnit != value)
                {
                    m_distanceUnit = value;
                    UpdateHost("Value", "X", "Y", "Z", "DistanceUnit");
                }
            }
        }

        [XmlIgnore]

        [ScriptIgnore]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.X], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if ((m_dVector[(int)VectorIndexType.X] != dValueMeters) ||
                    (m_dVector[(int)VectorIndexType.Y] != dValueMeters) ||
                    (m_dVector[(int)VectorIndexType.Z] != dValueMeters))
                {
                    m_dVector[(int)VectorIndexType.X] = dValueMeters;
                    m_dVector[(int)VectorIndexType.Y] = dValueMeters;
                    m_dVector[(int)VectorIndexType.Z] = dValueMeters;
                    UpdateHost("Value", "X", "Y", "Z");
                }
            }
        }

        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.X], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                
                if (Uniform && m_dVector[(int)VectorIndexType.X] != 0)
                {
                    m_dVector[(int)VectorIndexType.Y] = m_dYOverX * dValueMeters;
                    m_dVector[(int)VectorIndexType.Z] = m_dZOverX * dValueMeters;
                }

                m_dVector[(int)VectorIndexType.X] = dValueMeters;
                UpdateHost();
            }
        }

        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Y], m_distanceUnit); }
            set 
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);

                if (Uniform && m_dVector[(int)VectorIndexType.Y] != 0)
                {
                    m_dVector[(int)VectorIndexType.X] = m_dXOverY * dValueMeters;
                    m_dVector[(int)VectorIndexType.Z] = m_dZOverY * dValueMeters;
                }

                m_dVector[(int)VectorIndexType.Y] = dValueMeters;
                UpdateHost(); 
            }
        }

        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dVector[(int)VectorIndexType.Z], m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                
                if (Uniform && m_dVector[(int)VectorIndexType.Z] != 0)
                {
                    m_dVector[(int)VectorIndexType.X] = m_dXOverZ * dValueMeters;
                    m_dVector[(int)VectorIndexType.Y] = m_dYOverZ * dValueMeters;
                }

                m_dVector[(int)VectorIndexType.Z] = dValueMeters;
                UpdateHost();
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        // Uniform scale
        private bool m_bUniform = false;

        [XmlAttribute("Uniform")]
        public bool Uniform
        {
            get { return m_bUniform; }
            
            set 
            { 
                m_bUniform = value;

                if (m_bUniform)
                {
                    if (m_dVector[(int)VectorIndexType.X] == 0 &&
                        m_dVector[(int)VectorIndexType.Y] == 0 &&
                        m_dVector[(int)VectorIndexType.Z] == 0)
                    {
                        // Make up a ratio of all the values are 0
                        m_dYOverX = 1;
                        m_dZOverX = 1;
                        m_dXOverY = 1;
                        m_dZOverY = 1;
                        m_dYOverZ = 1;
                    }
                    else
                    {
                        // Set the ratios for use in keeping the values uniform.
                        m_dYOverX = m_dVector[(int)VectorIndexType.Y] / m_dVector[(int)VectorIndexType.X];
                        m_dZOverX = m_dVector[(int)VectorIndexType.Z] / m_dVector[(int)VectorIndexType.X];
                        m_dXOverY = m_dVector[(int)VectorIndexType.X] / m_dVector[(int)VectorIndexType.Y];
                        m_dZOverY = m_dVector[(int)VectorIndexType.Z] / m_dVector[(int)VectorIndexType.Y];
                        m_dXOverZ = m_dVector[(int)VectorIndexType.X] / m_dVector[(int)VectorIndexType.Z];
                        m_dYOverZ = m_dVector[(int)VectorIndexType.Y] / m_dVector[(int)VectorIndexType.Z];
                    }
                }
            
            }
        }
        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsColorAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dColor = { 10, 100, 5 };        

        public DsColorAttribute()
            : base(DsAttr.Color)
        { }        

        /// <summary>
        /// Gets/sets the color
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Color")]
        public SolidColorBrush ColorBrush
        {
            get
            {
                if (m_dColor[(int)ColorIndexType.Red] < 0.0 ||
                    m_dColor[(int)ColorIndexType.Green] < 0.0 ||
                    m_dColor[(int)ColorIndexType.Blue] < 0.0)
                    return null;

                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dColor[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dColor[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dColor[(int)ColorIndexType.Blue] * 2.55f + 0.5f);

                var brush = new SolidColorBrush(Color.FromRgb(red, green, blue));
                if (brush.CanFreeze)
                    brush.Freeze();
                return brush;
            }
            set
            {
                if (value == null)
                {
                    m_dColor[(int)ColorIndexType.Red] = -1.0;
                    m_dColor[(int)ColorIndexType.Green] = -1.0;
                    m_dColor[(int)ColorIndexType.Blue] = -1.0;
                }
                else
                {
                    // Scale for appropriate color range
                    m_dColor[(int)ColorIndexType.Red] = value.Color.R / 2.55f;
                    m_dColor[(int)ColorIndexType.Green] = value.Color.G / 2.55f;
                    m_dColor[(int)ColorIndexType.Blue] = value.Color.B / 2.55f;
                }
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }

        /// <summary>
        /// Gets/sets the color
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Color? ColorValue
        {
            get
            {
                if (m_dColor[(int)ColorIndexType.Red] < 0.0 ||
                    m_dColor[(int)ColorIndexType.Green] < 0.0 ||
                    m_dColor[(int)ColorIndexType.Blue] < 0.0)
                    return null;

                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dColor[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dColor[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dColor[(int)ColorIndexType.Blue] * 2.55f + 0.5f);

                return Color.FromRgb(red, green, blue);
            }
            set
            {
                if (!value.HasValue)
                {
                    m_dColor[(int)ColorIndexType.Red] = -1.0;
                    m_dColor[(int)ColorIndexType.Green] = -1.0;
                    m_dColor[(int)ColorIndexType.Blue] = -1.0;
                }
                else
                {
                    // Scale for appropriate color range
                    m_dColor[(int)ColorIndexType.Red] = value.Value.R / 2.55f;
                    m_dColor[(int)ColorIndexType.Green] = value.Value.G / 2.55f;
                    m_dColor[(int)ColorIndexType.Blue] = value.Value.B / 2.55f;
                }
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }
        /// <summary>
        /// Gets/sets the red component of the color
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Red")]
        [DsAttributeProperty("Red")]
        public double Red
        {
            get
            {
                return m_dColor[(int)ColorIndexType.Red];
            }
            set
            {
                m_dColor[(int)ColorIndexType.Red] = value;
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }
        
        /// <summary>
        /// Gets/sets the green component of the color
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Green")]
        [DsAttributeProperty("Green")]
        public double Green
        {
            get
            {
                return m_dColor[(int)ColorIndexType.Green];
            }
            set
            {
                m_dColor[(int)ColorIndexType.Green] = value;
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }

        /// <summary>
        /// Gets/sets the blue component of the color
        /// </summary>
        /// <author>JMG</author>
        [XmlAttribute("Blue")]
        [DsAttributeProperty("Blue")]
        public double Blue
        {
            get
            {
                return m_dColor[(int)ColorIndexType.Blue];
            }
            set
            {
                m_dColor[(int)ColorIndexType.Blue] = value;
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }

        /// <summary>
        /// Gets/Sets the gray level changing the r,g and b together.
        /// </summary>
        [XmlIgnore]
        [ScriptIgnore]
        public double GrayLevel
        {
            get
            {
                return (m_dColor[0] + m_dColor[1] + m_dColor[2]) / 3.0;
            }
            set
            {
                m_dColor[0] = m_dColor[1] = m_dColor[2] = value;
                UpdateHost("ColorBrush", "Red", "Green", "Blue", "GrayLevel");
            }
        }

        /// <summary>
        /// Gets whether or not this attribute is to be inherited from its parent.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeInfo]
        public override bool Inherited
        {
            get { return m_dColor[(int)ColorIndexType.Red] == INHERIT_COLOR_SIGNAL &&
                m_dColor[(int)ColorIndexType.Green] == INHERIT_COLOR_SIGNAL &&
                m_dColor[(int)ColorIndexType.Blue] == INHERIT_COLOR_SIGNAL;
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsColorAlphaAttribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private double[] m_dColor = { 100, 10, 10, 100 };        

        public DsColorAlphaAttribute()
            : base(DsAttr.Double)
        { }        

        /// <summary>
        /// Gets/sets the color
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Color")]
        [DsAttributeProperty("Color")]
        public SolidColorBrush ColorBrush
        {
            get
            {
                // Scale for appropriate color range
                byte red = (byte)(m_dColor[(int)ColorIndexType.Red] * 2.55f);
                byte green = (byte)(m_dColor[(int)ColorIndexType.Green] * 2.55f);
                byte blue = (byte)(m_dColor[(int)ColorIndexType.Blue] * 2.55f);
                byte alpha = (byte)(m_dColor[(int)ColorIndexType.Alpha] * 2.55f);

                var brush = new SolidColorBrush(Color.FromArgb(alpha, red, green, blue));
                if (brush.CanFreeze)
                    brush.Freeze();
                return brush;
            }
            set
            {
                // Scale for appropriate color range
                m_dColor[(int)ColorIndexType.Red] = value.Color.R / 2.55f;
                m_dColor[(int)ColorIndexType.Green] = value.Color.G / 2.55f;
                m_dColor[(int)ColorIndexType.Blue] = value.Color.B / 2.55f;
                m_dColor[(int)ColorIndexType.Alpha] = value.Color.A / 2.55f;
                UpdateHost();
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsMorphAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dValue = 18.123456789;

        public DsMorphAttribute()
            : base(DsAttr.Morph)
        { }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return m_dValue; }
            set
            {
                if (m_dValue != value)
                {
                    m_dValue = value;
                    UpdateHost("Value");
                }
            }
        }


        /// <summary>
        /// Gets whether or not this attribute is to be inherited from its parent.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeInfo]
        public override bool Inherited
        {
            get { return m_dValue == INHERIT_MORPH_SIGNAL; }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDateTimeAttribute : DsAttribute, INotifyPropertyChanged
    {
        // Store as a Julian (UTC) date
        private double m_dateTime;       

        public DsDateTimeAttribute()
            : base(DsAttr.Datetime)
        {
            // Initialize date time to current local time
            // until the host is overrides it.
            DateTime dt = DateTime.Now.ToUniversalTime();
            double dJulianDateTime = 0;
            DsAPI.DsDateToJulianDate(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, out dJulianDateTime);
            m_dateTime = dJulianDateTime;
        }

        /// <summary>
        /// Gets/sets the current date & time
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("JulianDateTime")]
        [DsAttributeProperty("JulianDateTime")]
        public double JulianDateTime
        {
            get { return m_dateTime; }
            set
            {
                // If the date is different, update it.
                if (m_dateTime != value)
                {
                    m_dateTime = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the year
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Year")]
        public int Year
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nYear;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given year, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(value, nMonth, nDay, nHour, nMin, nSec, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the month
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Month")]
        public int Month
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nMonth;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given month, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(nYear, value, nDay, nHour, nMin, nSec, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the day
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Day")]
        public int Day
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nDay;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given day, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(nYear, nMonth, value, nHour, nMin, nSec, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the hour
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Hour")]
        public int Hour
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nHour;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given year, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(nYear, nMonth, nDay, value, nMin, nSec, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the minute
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Minute")]
        public int Minute
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nMin;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given month, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(nYear, nMonth, nDay, nHour, value, nSec, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the day
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("Second")]
        public int Second
        {
            get
            {
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                return nSec;
            }
            set
            {
                // Get the year, month, hour, min, and second of current date and time
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);

                // Using the given day, create/update the new date/time
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(nYear, nMonth, nDay, nHour, nMin, value, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        /// <summary>
        /// Gets/sets the current (local PC) date & time 
        /// 
        /// Note:  this assumes that the attribute is a valid date and time for the DateTime C# class.  
        /// This will throw an exception if the julian date is less than 1721423.50 (julian date on 1 January 01)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DateTime DateTimeAttr
        {
            get
            {
                // Convert the julian date to a DateTime in UTC
                int nYear, nMonth, nDay, nHour, nMin, nSec;
                DsAPI.DsJulianDateToDate(m_dateTime, out nYear, out nMonth, out nDay, out nHour, out nMin, out nSec);
                DateTime dtUtc = new DateTime(nYear, nMonth, nDay, nHour, nMin, nSec);

                // Return the UTC time
                return dtUtc;
            }
            set
            {      
                DateTime dt = value;

                // Convert the DateTime into a julian date
                double dJulianDateTime = 0;
                DsAPI.DsDateToJulianDate(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, out dJulianDateTime);
                JulianDateTime = dJulianDateTime;
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsDistanceAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dValue = 42500.987654321f;
        private DsDistanceUnit m_distanceUnit = DsDistanceUnit.Meter;        

        public DsDistanceAttribute()
            : base(DsAttr.Distance)
        { }

        /// <summary>
        /// Gets/sets the distance unit.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>KBW,JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public DsDistanceUnit DistanceUnit
        {
            get { return m_distanceUnit; }
            set
            {
                if (m_distanceUnit != value)
                {
                    m_distanceUnit = value;
                    UpdateHost("Distance", "DistanceUnit");
                }
            }
        }

        /// <summary>
        /// Gets/sets the distance
        /// </summary>
        /// <author>KBW</author>
        [DsAttributeProperty("Distance")]
        [XmlAttribute("Distance")]
        public double Distance
        {
            get { return MathUtilities.ConvertMetersToUnit(m_dValue, m_distanceUnit); }
            set
            {
                double dValueMeters = MathUtilities.ConvertDistanceToMeters(value, m_distanceUnit);
                if (m_dValue != dValueMeters)
                {
                    m_dValue = dValueMeters;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsEnumAttribute : DsAttribute, INotifyPropertyChanged
    {
        private UInt32 m_nValue = 0;  // Selected value              

        public DsEnumAttribute()
            : base(DsAttr.Enum)
        { m_nEnumID = -1; }

        /// <summary>
        /// Gets/sets the value
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public UInt32 Value
        {
            get { return m_nValue; }
            set
            {
                if (m_nValue != value)
                {
                    m_nValue = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        private short m_nEnumID = -1;

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsLocationAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dLatitude   = 40.46;    // latitude (in radians?)
        private double m_dLongitude  = -111.53;  // longitude (in radians?)
        private double m_dAltitude   = 3000.00;  // altitude in meters
        private double m_dHeading    = 1.00;     // heading in radians
        private double m_dTimeZone   = 3.00;     // optional time zone offset       

        public DsLocationAttribute()
            : base(DsAttr.Double)
        { }        
 
        /// <summary>
        /// Gets/sets the latitude (in degrees).
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Latitude")]
        [DsAttributeProperty("Latitude")]
        public double Latitude
        {
            get { return MathUtilities.RadToDeg(m_dLatitude); }
            set
            {
                double latitude = MathUtilities.DegToLimitedRad(value);
                if (m_dLatitude != latitude)
                {
                    m_dLatitude = latitude;
                    UpdateHost();
                }
                //OnPropertyChanged("Latitude");
            }
        }
        
        /// <summary>
        /// Gets/sets the latitude (in radians).
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        private double LatitudeInRadians
        {
            get { return m_dLatitude; }
            set
            {
                double latitude = value;
                if (m_dLatitude != latitude)
                {
                    m_dLatitude = latitude;
                    UpdateHost();
                }
                //OnPropertyChanged("Latitude");
            }
        }

        /// <summary>
        /// Gets/sets the latitude degrees.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LatitudeDegrees
        {
            get { return MathUtilities.IsDMSNegative(LatitudeInRadians) ?
                        -MathUtilities.GetDMS(LatitudeInRadians, DsLatLonParts.Degrees) :
                         MathUtilities.GetDMS(LatitudeInRadians, DsLatLonParts.Degrees);
            }
            set { LatitudeInRadians = MathUtilities.GetRadFromDMS(LatitudeInRadians, DsLatLonParts.Degrees, (int)value); }
        }

        /// <summary>
        /// Gets/sets the latitude minutes.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LatitudeMinutes
        {
            get { return MathUtilities.GetDMS(LatitudeInRadians, DsLatLonParts.Minutes); }
            set { LatitudeInRadians = MathUtilities.GetRadFromDMS(LatitudeInRadians, DsLatLonParts.Minutes, (int)value); }
        }

        /// <summary>
        /// Gets/sets the latitude seconds.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LatitudeSeconds
        {
            get { return MathUtilities.GetDMS(LatitudeInRadians, DsLatLonParts.Seconds); }
            set { LatitudeInRadians = MathUtilities.GetRadFromDMS(LatitudeInRadians, DsLatLonParts.Seconds, (int)value); }
        }

        /// <summary>
        /// Gets/sets the longitude (in degrees).
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Longitude")]
        [DsAttributeProperty("Longitude")]
        public double Longitude
        {
            get { return MathUtilities.RadToDeg(m_dLongitude); }
            set
            {
                double longitude = MathUtilities.DegToLimitedRad(value);
                if (m_dLongitude != longitude)
                {
                    m_dLongitude = longitude;
                    UpdateHost();
                }
                //OnPropertyChanged("Longitude");
            }
        }

        /// <summary>
        /// Gets/sets the longitude (in radians).
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        private double LongitudeInRadians
        {
            get { return m_dLongitude; }
            set
            {
                double longitude = value;
                if (m_dLongitude != longitude)
                {
                    m_dLongitude = longitude;
                    UpdateHost();
                }
                //OnPropertyChanged("Longitude");
            }
        }

        /// <summary>
        /// Gets/sets the longitude degrees.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LongitudeDegrees
        {
            get
            {
                return MathUtilities.IsDMSNegative(LongitudeInRadians) ?
                       -MathUtilities.GetDMS(LongitudeInRadians, DsLatLonParts.Degrees) :
                        MathUtilities.GetDMS(LongitudeInRadians, DsLatLonParts.Degrees);
            }
            set { LongitudeInRadians = MathUtilities.GetRadFromDMS(LongitudeInRadians, DsLatLonParts.Degrees, (int)value); }
        }

        /// <summary>
        /// Gets/sets the longitude minutes.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LongitudeMinutes
        {
            get { return MathUtilities.GetDMS(LongitudeInRadians, DsLatLonParts.Minutes); }
            set { LongitudeInRadians = MathUtilities.GetRadFromDMS(LongitudeInRadians, DsLatLonParts.Minutes, (int)value); }
        }

        /// <summary>
        /// Gets/sets the longitude seconds.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double LongitudeSeconds
        {
            get { return MathUtilities.GetDMS(LongitudeInRadians, DsLatLonParts.Seconds); }
            set { LongitudeInRadians = MathUtilities.GetRadFromDMS(LongitudeInRadians, DsLatLonParts.Seconds, (int)value); }
        }

        /// <summary>
        /// Gets/sets the altitude.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Altitude")]
        [DsAttributeProperty("Altitude")]
        public double Altitude
        {
            get { return m_dAltitude; }
            set
            {
                if (m_dAltitude != value)
                {
                    m_dAltitude = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the heading (radians)
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Heading
        {
            get { return m_dHeading; }
            set
            {
                if (m_dHeading != value)
                {
                    m_dHeading = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the heading (in degrees)
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Heading")]
        [DsAttributeProperty("HeadingDegrees")]
        public double HeadingDegrees
        {
            get { return MathUtilities.RadToDeg(m_dHeading); }
            set
            {
                if (m_dHeading != MathUtilities.DegToLimitedRad(value))
                {
                    m_dHeading = MathUtilities.DegToLimitedRad(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the time zone.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("TimeZone")]
        [DsAttributeProperty("TimeZone")]
        public double TimeZone
        {
            get { return m_dTimeZone; }
            set
            {
                if (m_dTimeZone != value)
                {
                    m_dTimeZone = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the time zone index.
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public int TimeZoneIndex
        {
            get 
            {
                int nTimeZoneIndex = 0;
                // Find and set the timezone index
                ReadOnlyCollection<TimeZoneInfo> timeZoneList = System.TimeZoneInfo.GetSystemTimeZones();
                int nMinutes = (int)((m_dTimeZone - (int)m_dTimeZone) * 60);
                for (int i = 0; i < timeZoneList.Count; i++)
                {
                    if ((timeZoneList[i].BaseUtcOffset.Hours == (int)m_dTimeZone) &&
                        (timeZoneList[i].BaseUtcOffset.Minutes == nMinutes))
                        nTimeZoneIndex = i;
                }
                return nTimeZoneIndex;
            }
            set 
            {
                 // Find the timezone base UTC offset to send to host
                ReadOnlyCollection<TimeZoneInfo> timeZoneList = System.TimeZoneInfo.GetSystemTimeZones();
                TimeZoneInfo tzInfo = timeZoneList[value];
                int nTzHourOffset = tzInfo.BaseUtcOffset.Hours;
                int nTzMinuteOffset = tzInfo.BaseUtcOffset.Minutes;

                // Set timezone for host
                TimeZone = nTzHourOffset + ((double)nTzMinuteOffset / 60.0);
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsPercentAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dValue = 40.0f;        

        public DsPercentAttribute()
            : base(DsAttr.Percent)
        { }        

        /// <summary>
        /// Gets/sets the percent
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Percent
        {
            get { return m_dValue; }
            set 
            {
                if (m_dValue != value)
                {
                    m_dValue = value;
                    UpdateHost("Value", "Percent");
                }
            }
        }

        /// <summary>
        /// Gets/sets the percent
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return m_dValue; }
            set 
            {
                if (m_dValue != value)
                {
                    m_dValue = value;
                    UpdateHost("Value", "Percent");
                }
            }
        }

        /// <summary>
        /// Gets whether or not this attribute is to be inherited from its parent.
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeInfo]
        public override bool Inherited
        {
            get { return m_dValue == INHERIT_INTENSITY_SIGNAL; }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsPercent2Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        private double[] m_dVector = { 44, 88 };        

        public DsPercent2Attribute()
            : base(DsAttr.Percent2)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsPercent3Attribute : DsAttribute, INotifyPropertyChanged
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private double[] m_dVector = { 22, 44, 88 };

        public DsPercent3Attribute()
            : base(DsAttr.Percent3)
        { }

        /// <summary>
        /// Gets/sets the X value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("X")]
        [DsAttributeProperty("X")]
        public double X
        {
            get { return m_dVector[(int)VectorIndexType.X]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.X] != value)
                {
                    m_dVector[(int)VectorIndexType.X] = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the Y value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Y")]
        [DsAttributeProperty("Y")]
        public double Y
        {
            get { return m_dVector[(int)VectorIndexType.Y]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Y] != value)
                {
                    m_dVector[(int)VectorIndexType.Y] = value;
                    UpdateHost();
                }
            }
        }


        /// <summary>
        /// Gets/sets the Z value.
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Z")]
        [DsAttributeProperty("Z")]
        public double Z
        {
            get { return m_dVector[(int)VectorIndexType.Z]; }
            set
            {
                if (m_dVector[(int)VectorIndexType.Z] != value)
                {
                    m_dVector[(int)VectorIndexType.Z] = value;
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsShowClockModeAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_eShowClockMode = 0; //DsClockMode.Local;
        private float m_fOffset = 0.0f;	// if LISTEN or SEND, this is the time offset in seconds
        private byte m_byAbsolute = 0;	// if LISTEN or SEND and TRUE, time offset is absolute

        public DsShowClockModeAttribute()
            : base(DsAttr.Showclockmode)
        { }        

        /// <summary>
        /// Gets/sets the show clock mode
        /// </summary>
        /// <author>KBW</author>
        public DsClockMode ShowClockMode
        {
            get { return (DsClockMode)m_eShowClockMode; }
            set
            {
                if (m_eShowClockMode != (byte)value)
                {
                    m_eShowClockMode = (byte)value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the show clock mode
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public int IntShowClockMode
        {
            get { return (int)m_eShowClockMode; }
            set 
            {
                if (m_eShowClockMode != (byte)value)
                {
                    m_eShowClockMode = (byte)value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the offset
        /// </summary>
        /// <author>KBW</author>
        public float Offset
        {
            get { return m_fOffset; }
            set
            {
                if (m_fOffset != value)
                {
                    m_fOffset = value;
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets the absolute (boolean)
        /// </summary>
        /// <author>KBW</author>
        public bool Absolute
        {
            get { return Convert.ToBoolean(m_byAbsolute); }
            set 
            {
                if (m_byAbsolute != Convert.ToByte(value))
                {
                    m_byAbsolute = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsTimecodeAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dTimecode = 56.78;        

        public DsTimecodeAttribute()
            : base(DsAttr.Timecode)
        { }        

        /// <summary>
        /// Gets/sets the timecode
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        public double Timecode
        {
            get { return m_dTimecode; }
            set
            {
                if (m_dTimecode != value)
                {
                    m_dTimecode = value;
                    UpdateHost("Value", "TotalMinutes", "TotalHours");
                }
            }
        }

        /// <summary>
        /// Gets/sets the timecode
        /// </summary>
        /// <author>KBW</author>
        [XmlAttribute("Value")]
        [DsAttributeProperty("Value")]
        public double Value
        {
            get { return m_dTimecode; }
            set
            {
                if (m_dTimecode != value)
                {
                    m_dTimecode = value;
                    UpdateHost("Timecode", "TotalMinutes", "TotalHours");
                }
            }
        }

        /// <summary>
        /// Gets/sets the timecode in total minutes
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("TotalMinutes")]
        public double TotalMinutes
        {
            get { return m_dTimecode / DsConstants.dSECONDS_IN_MINUTE; }
            set
            {
                double dValueSeconds = value * DsConstants.dSECONDS_IN_MINUTE;
                if (m_dTimecode != dValueSeconds)
                {
                    m_dTimecode = dValueSeconds;
                    UpdateHost("Value", "Timecode", "TotalHours");
                }
            }
        }
        
        /// <summary>
        /// Gets/sets the timecode value in total hours
        /// </summary>
        /// <author>KBW</author>
        [XmlIgnore]
        [ScriptIgnore]
        [DsAttributeProperty("TotalHours")]
        public double TotalHours
        {
            get { return m_dTimecode / DsConstants.dSECONDS_IN_HOUR; }
            set
            {
                double dValueSeconds = value * DsConstants.dSECONDS_IN_HOUR;
                if (m_dTimecode != dValueSeconds)
                {
                    m_dTimecode = dValueSeconds;
                    UpdateHost("Value", "Timecode", "TotalMinutes");
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsRangeMapAttribute : DsAttribute, INotifyPropertyChanged
    {
        private byte m_bUseRange = 0;
        private double m_dInMin = 0;
        private double m_dInMax = 0;
        private double m_dOutMin = 0;
        private double m_dOutMax = 0;
        private DsRangeEndCond m_eRangeEndCondition = DsRangeEndCond.EndCondNone;

        public DsRangeMapAttribute()
            : base(DsAttr.RangeMap)
        { }

        /// <summary>
        /// Gets/sets the range end condition.
        /// This needs to be here first, 
        /// so that the XmlSerializer sets this first
        /// before anything
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("RangeEndCondition")]
        [DsAttributeProperty("RangeEndCondition")]
        public int RangeEndCondition
        {
            get { return (int)m_eRangeEndCondition; }
            set
            {
                if (m_eRangeEndCondition != (DsRangeEndCond)value)
                {
                    m_eRangeEndCondition = (DsRangeEndCond)value;
                    UpdateHost("RangeEndCondition");
                }
            }
        }

        /// <summary>
        /// Gets/sets UseRange
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("UseRange")]
        [DsAttributeProperty("UseRange")]
        public bool UseRange
        {
            get { return Convert.ToBoolean(m_bUseRange); }
            set
            {
                if (m_bUseRange != Convert.ToByte(value))
                {
                    m_bUseRange = Convert.ToByte(value);
                    UpdateHost();
                }
            }
        }

        /// <summary>
        /// Gets/sets InMin
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("InMin")]
        [DsAttributeProperty("InMin")]
        public double InMin
        {
            get { return m_dInMin; }
            set
            {
                m_dInMin = value;
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets InMin
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("InMax")]
        [DsAttributeProperty("InMax")]
        public double InMax
        {
            get { return m_dInMax; }
            set
            {
                m_dInMax = value;
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets OutMin
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("OutMin")]
        [DsAttributeProperty("OutMin")]
        public double OutMin
        {
            get { return m_dOutMin; }
            set
            {
                m_dOutMin = value;
                UpdateHost();
            }
        }

        /// <summary>
        /// Gets/sets OutMax
        /// </summary>
        /// <author>NEH</author>
        [XmlAttribute("OutMax")]
        [DsAttributeProperty("OutMax")]
        public double OutMax
        {
            get { return m_dOutMax; }
            set
            {
                m_dOutMax = value;
                UpdateHost();
            }
        }        

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsColorTransferAttribute : DsAttribute, INotifyPropertyChanged
    {
        private double m_dInFrom = 0;
        private double m_dInTo = 100;
        private byte m_bInFromInclusive = 1;
        private byte m_bInToInclusive = 1;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private double[] m_dOutFrom = { 0, 0, 0, 100 };
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private double[] m_dOutTo = { 100, 100, 100, 100 };
        private DsColorTransferFunctionType m_eFunction = DsColorTransferFunctionType.ColorTransferFunctionTypeLinear;

        public DsColorTransferAttribute()
            : base(DsAttr.ColorTransfer)
        { }

        /// <summary>
        /// The start of the input range
        /// </summary>
        [XmlAttribute("InFrom")]
        [DsAttributeProperty("InFrom")]
        public double InFrom
        {
            get { return m_dInFrom; }
            set 
            {
                if (m_dInFrom != value)
                {
                    m_dInFrom = value;
                    UpdateHost("InFrom");
                }
            }
        }

        /// <summary>
        /// The end of the input range
        /// </summary>
        [XmlAttribute("InTo")]
        [DsAttributeProperty("InTo")]
        public double InTo
        {
            get { return m_dInTo; }
            set
            {
                if (m_dInTo != value)
                {
                    m_dInTo = value;
                    UpdateHost("InTo");
                }
            }
        }


        /// <summary>
        /// Whether the start of the input range is inclusive
        /// </summary>
        [XmlAttribute("InFromInclusive")]
        [DsAttributeProperty("InFromInclusive")]
        public bool InFromInclusive
        {
            get { return Convert.ToBoolean(m_bInFromInclusive); }
            set
            {
                if (m_bInFromInclusive != Convert.ToByte(value))
                {
                    m_bInFromInclusive = Convert.ToByte(value);
                    UpdateHost("InFromInclusive");
                }
            }
        }

        /// <summary>
        /// Whether the end of the input range is inclusive
        /// </summary>
        [XmlAttribute("InToInclusive")]
        [DsAttributeProperty("InToInclusive")]
        public bool InToInclusive
        {
            get { return Convert.ToBoolean(m_bInToInclusive); }
            set
            {
                if (m_bInToInclusive != Convert.ToByte(value))
                {
                    m_bInToInclusive = Convert.ToByte(value);
                    UpdateHost("InToInclusive");
                }
            }
        }

        /// <summary>
        /// Gets/sets the color bursh of the start of the output range
        /// </summary>
        [XmlAttribute("OutFrom")]
        [DsAttributeProperty("OutFrom")]
        public SolidColorBrush OutFromBrush
        {
            get
            {
                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dOutFrom[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dOutFrom[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dOutFrom[(int)ColorIndexType.Blue] * 2.55f + 0.5f);
                byte alpha = (byte)(m_dOutFrom[(int)ColorIndexType.Alpha] * 2.55f + 0.5f);

                var brush = new SolidColorBrush(Color.FromArgb(alpha, red, green, blue));
                if (brush.CanFreeze)
                    brush.Freeze();
                return brush;
            }
            set
            {
                if (value != null)
                {
                    // Scale for appropriate color range
                    m_dOutFrom[(int)ColorIndexType.Red] = value.Color.R / 2.55f;
                    m_dOutFrom[(int)ColorIndexType.Green] = value.Color.G / 2.55f;
                    m_dOutFrom[(int)ColorIndexType.Blue] = value.Color.B / 2.55f;
                    m_dOutFrom[(int)ColorIndexType.Alpha] = value.Color.A / 2.55f;
                }
                UpdateHost("OutFromBrush", "OutFromValue");
            }
        }


        /// <summary>
        /// Gets/sets the color of the start of the output range
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Color OutFromValue
        {
            get
            {
                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dOutFrom[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dOutFrom[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dOutFrom[(int)ColorIndexType.Blue] * 2.55f + 0.5f);
                byte alpha = (byte)(m_dOutFrom[(int)ColorIndexType.Alpha] * 2.55f + 0.5f);

                return Color.FromArgb(alpha, red, green, blue);
            }
            set
            {
                // Scale for appropriate color range
                m_dOutFrom[(int)ColorIndexType.Red] = value.R / 2.55f;
                m_dOutFrom[(int)ColorIndexType.Green] = value.G / 2.55f;
                m_dOutFrom[(int)ColorIndexType.Blue] = value.B / 2.55f;
                m_dOutFrom[(int)ColorIndexType.Alpha] = value.A / 2.55f;

                UpdateHost("OutFromBrush", "OutFromValue");
            }
        }


        /// <summary>
        /// Gets/sets the color brush of the end of the output range
        /// </summary>
        [XmlAttribute("OutTo")]
        [DsAttributeProperty("OutTo")]
        public SolidColorBrush OutToBrush
        {
            get
            {
                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dOutTo[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dOutTo[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dOutTo[(int)ColorIndexType.Blue] * 2.55f + 0.5f);
                byte alpha = (byte)(m_dOutTo[(int)ColorIndexType.Alpha] * 2.55f + 0.5f);

                var brush = new SolidColorBrush(Color.FromArgb(alpha, red, green, blue));
                if (brush.CanFreeze)
                    brush.Freeze();
                return brush;
            }
            set
            {
                if (value != null)
                {
                    // Scale for appropriate color range
                    m_dOutTo[(int)ColorIndexType.Red] = value.Color.R / 2.55f;
                    m_dOutTo[(int)ColorIndexType.Green] = value.Color.G / 2.55f;
                    m_dOutTo[(int)ColorIndexType.Blue] = value.Color.B / 2.55f;
                    m_dOutTo[(int)ColorIndexType.Alpha] = value.Color.A / 2.55f;
                }
                UpdateHost("OutToBrush", "OutToValue");
            }
        }


        /// <summary>
        /// Gets/sets the color of the end of the output range
        /// </summary>
        /// <author>JMG</author>
        [XmlIgnore]
        [ScriptIgnore]
        public Color OutToValue
        {
            get
            {
                // Scale for appropriate color range, Add half a value, to ensure accuracy in truncation
                byte red = (byte)(m_dOutTo[(int)ColorIndexType.Red] * 2.55f + 0.5f);
                byte green = (byte)(m_dOutTo[(int)ColorIndexType.Green] * 2.55f + 0.5f);
                byte blue = (byte)(m_dOutTo[(int)ColorIndexType.Blue] * 2.55f + 0.5f);
                byte alpha = (byte)(m_dOutTo[(int)ColorIndexType.Alpha] * 2.55f + 0.5f);

                return Color.FromArgb(alpha, red, green, blue);
            }
            set
            {
                // Scale for appropriate color range
                m_dOutTo[(int)ColorIndexType.Red] = value.R / 2.55f;
                m_dOutTo[(int)ColorIndexType.Green] = value.G / 2.55f;
                m_dOutTo[(int)ColorIndexType.Blue] = value.B / 2.55f;
                m_dOutTo[(int)ColorIndexType.Alpha] = value.A / 2.55f;

                UpdateHost("OutToBrush", "OutToValue");
            }
        }

        /// <summary>
        /// Gets/sets the function type.
        /// </summary>
        /// <author>DSR</author>
        [XmlAttribute("Function")]
        [DsAttributeProperty("Function")]
        public int Function
        {
            get { return (int)m_eFunction; }
            set
            {
                if (m_eFunction != (DsColorTransferFunctionType)value)
                {
                    m_eFunction = (DsColorTransferFunctionType)value;
                    UpdateHost("Function");
                }
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion
    }


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public partial class DsArrayAttribute : DsAttribute, IList<DsAttribute>,
        INotifyPropertyChanged
    {
        short m_nSize = 0;			// Current size.  Default 0.
        short m_nCapacity = 0;		// Current capacity. Default 0.
        IntPtr m_pAttrBlock;		// Allocated block of attributes. Default NULL.
        IntPtr m_sNames;
        DsAttr m_eElemType;	// All elems are of this type. From user spec in class.
        short m_nAttrStride;	// Byte size of attribute including following padding.


        // Must be after the array members above!
        [MarshalAs(UnmanagedType.SafeArray, SafeArrayUserDefinedSubType = typeof(DsAttribute))]
        DsAttribute[] m_internalAttrList = null;

        bool m_bUpdateGUIOnly = false;
        short m_nLastSize = -1; // The size of the array the last time the UI was updated.

        public DsArrayAttribute()
            : base(DsAttr.Array)
        { }

        /// <summary>
        /// Ensure that finalizer unregisters property changed events.
        /// </summary>
        ~DsArrayAttribute()
        {
            // Remove property changed events for existing attributes
            if (m_internalAttrList != null)
            {
                foreach (INotifyPropertyChanged notifyInterface in m_internalAttrList.AsEnumerable().OfType<INotifyPropertyChanged>())
                    notifyInterface.PropertyChanged -= OnArrayElementPropertyChanged;
            }
        }

        #region GUI-only attribute extensions

        public event PropertyChangedEventHandler PropertyChanged; // Must be last variable declared!

        #endregion

        #region IList<DsAttribute> Members

        public int IndexOf(DsAttribute item)
        {
            int nIndex = 0;
            foreach (var attr in InternalItems)
            {
                if (attr == item)
                    return nIndex;
                nIndex++;
            }
            return -1;
        }

        public void Insert(int index, DsAttribute item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public DsAttribute this[int index]
        {
            get
            {
                return InternalItems[index];
            }
            set
            {
                // Only change it if it is a correct type.
                if (value.Type == m_eElemType)
                {
                    InternalItems[index] = value;
                    UpdateArrayElemOnHost(index, value);
                }
            }
        }

        #endregion

        #region ICollection<DsAttribute> Members

        public void Add(DsAttribute item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(DsAttribute item)
        {
            return InternalItems.Contains(item);
        }

        public void CopyTo(DsAttribute[] array, int arrayIndex)
        {
            InternalItems.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return InternalItems.Length; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(DsAttribute item)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IEnumerable<DsAttribute> Members

        public IEnumerator<DsAttribute> GetEnumerator()
        {
            return InternalItems.Cast<DsAttribute>().GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return InternalItems.GetEnumerator();
        }

        #endregion

        /// <summary>
        /// This stores a copy of the Ds attribute items, used to generate
        /// and return the data in all of the method listed here.
        /// </summary>
        internal DsAttribute[] InternalItems
        {
            get
            {
                if (m_internalAttrList == null)
                {
                    Resize(m_nSize);
                }

                return m_internalAttrList;
            }
        }

        /// <summary>
        /// This resizes the array to the current internal size.
        /// </summary>
        internal void Resize()
        {
            Resize(m_nSize, false);
        }

        /// <summary>
        /// Resize the array to the new size
        /// </summary>
        /// <param name="nNewSize"></param>
        public void Resize(short nNewSize)
        {
            Resize(nNewSize, true);
        }

        /// <summary>
        /// This resizes the array triggering an update on the Host if needed.
        /// </summary>
        /// <param name="nNewSize"></param>
        internal void Resize(short nNewSize, bool bUpdateHost)
        {
            if (m_internalAttrList == null ||
                m_internalAttrList.Length != nNewSize)
            {
                // Remove property changed events for existing attributes
                if (m_internalAttrList != null)
                {
                    foreach (INotifyPropertyChanged notifyInterface in m_internalAttrList.Cast<INotifyPropertyChanged>())
                        notifyInterface.PropertyChanged -= OnArrayElementPropertyChanged;
                }

                var oldInternalAttrList = m_internalAttrList;
                m_internalAttrList = new DsAttribute[nNewSize];
                for (int i = 0; i < nNewSize; i++)
                {
                    DsAttribute attr = null;

                    // Try to reuse the old internal attribute list, if it has any.
                    // If we can't reuse, then create a new one.
                    if (oldInternalAttrList == null ||
                        i >= oldInternalAttrList.Length)
                        attr = DsAPI.DsCreateAttributeFromType(m_eElemType);
                    else
                        attr = oldInternalAttrList[i];

                    if (attr != null)
                        m_internalAttrList[i] = attr;

                    var notifyInterface = attr as INotifyPropertyChanged;
                    if (notifyInterface != null)
                    {
                        notifyInterface.PropertyChanged += new PropertyChangedEventHandler(OnArrayElementPropertyChanged);
                    }
                }

                if (bUpdateHost)
                    UpdateHost(true, String.Empty, "TheArray", "Lines", "Text", "CommandText");
            }
        }


        public DsAttr ElementType
        {
            get { return m_eElemType; }
            internal set
            {
                //Force the internal attr list to regenerate if the type has changed.
                // by clearing the internal list of attributes.
                if (m_eElemType != value)
                    m_internalAttrList = null;

                m_eElemType = value;
            }
        }

        /// <summary>
        /// When a property of a single array element has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnArrayElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (m_bUpdateGUIOnly)
                return;
            var attr = sender as DsAttribute;
            if (attr != null)
            {
                // Find the index of the property that changed.
                int nIndex = IndexOf(attr);

                if (nIndex >= 0 && nIndex < InternalItems.Length)
                {
                    UpdateArrayElemOnHost(nIndex, attr, e.PropertyName == "applyarrayvalue");
                }
            }
        }

        /// <summary>
        /// This updates the array element on the host.
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="attributeElem"></param>
        private void UpdateArrayElemOnHost(int nIndex, DsAttribute attributeElem)
        {
            UpdateArrayElemOnHost(nIndex, attributeElem, false);
        }


        partial void UpdateArrayElemOnHost(int nIndex, DsAttribute attributeElem, bool bFromApplyCall);

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// When the attributes collection has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        partial void OnCollectionChanged();

        #endregion
    }


}
