﻿using System;
using System.Collections.Generic;

using System.Text;

namespace ES.Ds.Common
{

    public static class DsConstants
    {
        
        public const int    MAX_NAME_LENGTH = 51;
        public const int    MAX_STRING_LENGTH = 256;
        public const int    MAX_PATH_LENGTH = 260;
        public const int    MAX_JS_PARAMETER_LENGTH = 260;
        public const int    MAX_COMMAND_LENGTH = 512;
        public const int    MAX_COMMANDBUFFER_LENGTH = 20380;
        public const int    MAX_ABS_TIMECODE_LENGTH = 12;				// HH:MM:SS.SS + termination
        public const int    MAX_COMMAND_SOURCE_NAME_LENGTH = MAX_NAME_LENGTH + MAX_ABS_TIMECODE_LENGTH;
        public const int    MAX_COMMAND_ECHO_STRING_LENGTH = MAX_COMMAND_SOURCE_NAME_LENGTH + MAX_COMMAND_LENGTH;
        public const int    MAX_ARRAY_SIZE = 1024;					// maximum size of an attribute array in an object.

        public const int    MAX_MESSAGE_LENGTH = 1024;					// maximum size of message sent to message manager
        // Math constants
        public const double dPI    = 3.1415926535897932384626433832795;        // double precision Pi
        public const double d2PI   = dPI * 2.0;                                // double precision Pi * 2
        public const double dPIby2 = dPI / 2.0;                                // double precision pi / 2    
        public const double dPIby4 = dPI / 4.0;                                // double precision pi / 4    
        public const float  fPI    = (float)3.1415926535897932384626433832795; // single precision Pi
        public const float  f2PI   = fPI * 2.0f;                               // single precision Pi * 2
        public const float  fPIby2 = fPI / 2.0f;                               // single precision pi / 2 
   
        //////////////////////////////////////////////////////////////////////////////////////////
        // Directory Constants
        public const string DigistarDir = "D6Software";
        public const string ContentDir = "D6Content";
        public const string DigistarRegistry = @"SOFTWARE\Evans & Sutherland\Digistar 6";
        public const string DigifaceBadRegKey = @"SOFTWARE\Microsoft\Internet Explorer\LowRegistry\Audio\PolicyConfig\PropertyStore";
        public const string DigistarWebAssetsURL = @"https://assets.es.com/Icons/";
        public const string DigistarWebAssets_D5Icon = @"D5/Digistar.png";
        public const string DigistarWebAssets_IconURL = DigistarWebAssetsURL + DigistarWebAssets_D5Icon;
        public const string DigistarWebAssetsCache = @"\\localhost\D6Software\Config\Gui\ImageCache";
        public const string DomecastingLogsDir = @"$Digistar\Logs\Domecasting";
        public const string ScriptExt = ".ds";

        //////////////////////////////////////////////////////////////////////////////////////////
        // Common constants used with astronomical functions
        public const double dSPEED_OF_LIGHT = 299792458; // Meters per sec.
        public const double dMETERS_IN_KILOMETER = 1000.0;
        public const double dMETERS_IN_MILE = 1609.344;
        public const double dMETERS_IN_NAUTICAL_MILE = 1852;
        public const double dMETERS_IN_ASTRONOMICAL_UNIT = 1.49597870691e+11;
        public const double dMETERS_IN_LIGHTYEAR = 9.460730472580e+15;
        public const double dMETERS_IN_PARSEC = 3.08567758128e+16;
        public const double dMETERS_IN_MEGA_PARSEC = 3.08567758128e+22;

        //////////////////////////////////////////////////////////////////////////////////////////
        // Common constants for well known body radii

        public const double dSUN_RADIUS_METERS = 696000 * 1000;
        public const double dMOON_RADIUS_METERS = 1737.4 * 1000;

        public const double dMERCURY_RADIUS_METERS = 2439.7 * 1000;
        public const double dVENUS_RADIUS_METERS = 6051.8 * 1000;
        public const double dEARTH_RADIUS_METERS = 6378137;		// new value for new terrain
        public const double dMARS_RADIUS_METERS = 3396.19 * 1000;  // updated for new terrain (added .19)
        public const double dJUPITER_RADIUS_METERS = 71492 * 1000;
        public const double dSATURN_RADIUS_METERS = 60268 * 1000;
        public const double dURANUS_RADIUS_METERS = 25559 * 1000;
        public const double dNEPTUNE_RADIUS_METERS = 24764 * 1000;

        public const double dPLUTO_RADIUS_METERS = 1188.3 * 1000;
        public const double dCERES_RADIUS_METERS = 470 * 1000;
        public const double dERIS_RADIUS_METERS = 1163 * 1000;
        public const double dHAUMEA_RADIUS_METERS = 1161 * 1000;
        public const double dMAKEMAKE_RADIUS_METERS = 715 * 1000;

        //////////////////////////////////////////////////////////////////////////////////////////
        // Astronomical constants
        //////////////////////////////////////////////////////////////////////////////////////////
        public const double dJD2000 = 2451545.0;				// Julian Day number for epoch J2000.0 (12 noon)
        public const double dJD1991pt25 = 2448349.0625;		// Julian Day number for epoch J1991.25 (1991-4-2 6:30 UT)

        public const double dOBLIQUITY_OF_ECLIPTIC = 23.43929111 * dPI / 180.0;	// The obliquity of the ecliptic of J2000.0 (radians)
        public const double dPRECESSION_PERIOD = 25770.03591;		// Years in the period of Earth's precession

        public const double dMINUTES_IN_DEGREE = 60.0;
        public const double dSECONDS_IN_DEGREE = 60.0 * 60.0;

        public const double dDEGREES_IN_HOUR = 15.0;
        public const double dSECONDS_IN_HOUR = 60.0 * 60.0;

        public const double dSECONDS_IN_MINUTE = 60.0;

        public const double dMINUTES_IN_HOUR = 60.0;

        public const double dHOURS_IN_DAY = 24.0;

        public const double dSECONDS_IN_DAY = dSECONDS_IN_MINUTE * dMINUTES_IN_HOUR * dHOURS_IN_DAY;

        public const double dMINUTES_IN_DAY = dMINUTES_IN_HOUR * dHOURS_IN_DAY;

        public const double dSIDEREAL_DAYS_PER_DAY = 24.0 / 23.93446965;	// a sidereal day is one earth revolution with respect to stars

        public const double dDAYS_PER_SYNODIC_MONTH = 29.530589;		// average period of moon revolution with respect to sun (full moon to full moon)
        public const double dDAYS_PER_SIDEREAL_MONTH = 27.321662;		// average period of moon revolution with respect to stars

        public const double dDAYS_IN_YEAR = 365.25;					// One Julian year in days, used only for precession & proper motion calculations 
        public const double dDAYS_PER_TROPICAL_YEAR = 365.24219;
        public const double dDAYS_PER_SIDEREAL_YEAR = 365.256363;

        public const double dSECONDS_IN_YEAR = dSECONDS_IN_DAY * dDAYS_IN_YEAR;
        public const double dDAYS_IN_1MILLION_YEARS = 1e6 * dDAYS_IN_YEAR;

        public const double dMINIMUM_JULIAN_DATE = dJD2000 - dDAYS_IN_1MILLION_YEARS;
        public const double dMAXIMUM_JULIAN_DATE = dJD2000 + dDAYS_IN_1MILLION_YEARS;

        public const double dMINIMUM_CALENDAR_YEAR = -4712;
        public const double dMAXIMUM_CALENDAR_YEAR = 1002000;

    }

}
