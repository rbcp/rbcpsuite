/////////////////////////////////////////////////////////////////////////////////////////
//
// COPYRIGHT (C) 2010 Evans & Sutherland Computer Corporation
// All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////////////
//
// Class Description: Helper functions for Ds in C#
//
////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Runtime.InteropServices;

namespace ES.Ds.Common
{

    /// <summary>
    /// Different parts of latitude/longitude
    /// </summary>
    public enum DsLatLonParts
    {
        Degrees,
        Minutes,
        Seconds,
    }

    /// <summary>
    /// Represents a 3D vector for Digistar functions.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector3D
    {
        public double dX;
        public double dY;
        public double dZ;

        public double Length()
        {
            return Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
        }

        /// <summary>
        /// Allow implicit conversions to a WPF vector and back
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static implicit operator System.Windows.Media.Media3D.Vector3D(Vector3D v)
        {
            return new System.Windows.Media.Media3D.Vector3D(v.dX, v.dY, v.dZ);
        }

        /// <summary>
        /// Allow casting to a double array
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static explicit operator double[](Vector3D v)
        {
            return new double[3] { v.dX, v.dY, v.dZ };
        }

        /// <summary>
        /// Allow casting from a double array
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static explicit operator Vector3D(double[] a)
        {
            if (a == null || a.Length == 0)
                return new Vector3D() { dX = 0, dY = 0, dZ = 0 };
            else if (a.Length >= 3)
                return new Vector3D() { dX = a[0], dY = a[1], dZ = a[2] };
            else if (a.Length == 2)
                return new Vector3D() { dX = a[0], dY = a[1], dZ = 0 };
            else //if (a.Length == 1)
                return new Vector3D() { dX = a[0], dY = 0, dZ = 0 };
        }

        /// <summary>
        /// Per component addition of the vector.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="dVal"></param>
        /// <returns></returns>
        public static Vector3D operator +(Vector3D v1, Vector3D v2)
        {
            Vector3D newV = v1;
            newV.dX += v2.dX;
            newV.dY += v2.dY;
            newV.dZ += v2.dZ;
            return newV;
        }

        /// <summary>
        /// Per component subraction of the vector.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="dVal"></param>
        /// <returns></returns>
        public static Vector3D operator -(Vector3D v1, Vector3D v2)
        {
            Vector3D newV = v1;
            newV.dX -= v2.dX;
            newV.dY -= v2.dY;
            newV.dZ -= v2.dZ;
            return newV;
        }

        /// <summary>
        /// Unary minus operator
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3D operator -(Vector3D v)
        {
            Vector3D newV;
            newV.dX = -v.dX;
            newV.dY = -v.dY;
            newV.dZ = -v.dZ;
            return newV;
        }

        /// <summary>
        /// Scalar multiplication of the vector.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="dVal"></param>
        /// <returns></returns>
        public static Vector3D operator *(Vector3D v, double dVal)
        {
            Vector3D newV = v;
            newV.dX *= dVal;
            newV.dY *= dVal;
            newV.dZ *= dVal;
            return newV;
        }

        /// <summary>
        /// Normalizes the values in the vector.
        /// </summary>
        public Vector3D Normalized()
        {
            Vector3D v = this;
            double dLen = Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
            if (dLen > 1e-8)
            {
                v.dX /= dLen;
                v.dY /= dLen;
                v.dZ /= dLen;
            }
            return v;
        }

        /// <summary>
        /// Returns the dot product of the two vectors.
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static double Dot(Vector3D v1, Vector3D v2)
        {
            return v1.dX * v2.dX + v1.dY * v2.dY + v1.dZ * v2.dZ;
        }

        /// <summary>
        /// Returns the cross product of the two vectors
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3D Cross(Vector3D v1, Vector3D v2)
        {
            Vector3D v;
            v.dX = v1.dY * v2.dZ - v1.dZ * v2.dY;
            v.dY = v1.dZ * v2.dX - v1.dX * v2.dZ;
            v.dZ = v1.dX * v2.dY - v1.dY * v2.dX;
            return v;
        }
    }

    /// <summary>
    /// Math utilities class for Ds  This class provides math utility functions for the DsGUI.
    /// </summary>
    public partial class MathUtilities
    {
        #region Public Member Functions

        /////////////////////////////////////////////////////////////////////////////////
        // PUBLIC MEMBER FUNCTIONS
        /////////////////////////////////////////////////////////////////////////////////

        #region Hour, Minute, Seconds, Degreee and Radians Conversion

        /// <summary>
        /// Convert decimal seconds to hours, minutes, and seconds (decimal)
        /// (Ported from D3 p54Functions.h)
        /// </summary>
        /// <param name="dSeconds">Seconds</param>
        /// <param name="nHours">Hours reference</param>
        /// <param name="nMinutes">Minutes reference</param>
        /// <returns>Decimal hours, minutes, and seconds</returns>
        /// <author>SLP/KBW</author>
        public static double SecToHMS(double dSeconds, ref int nHours, ref int nMinutes)
        {
            if (dSeconds < 0)  // convert negative seconds to seconds for conversion
                dSeconds *= -1;

            nHours = (int)dSeconds / 3600;
            nMinutes = ((int)dSeconds % 3600) / 60;

            return (dSeconds - ((nHours * 3600.0) + (nMinutes * 60.0)));
        }

        /// <summary>
        /// Convert radians to degrees
        /// (Ported from DsFunctions.h)
        /// </summary>
        /// <param name="dRad"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static double RadToDeg(double dRad)
        {
            return (dRad * 57.295779513082320876798154814105);
        }

        /// <summary>
        /// Convert Degrees to Radians and limit Radians to 2 PI
        /// (Ported from Ds Functions.h)
        /// </summary>
        /// <param name="dDegrees">degrees to be convertered</param>
        /// <returns>radian equivalent, within the range -2 PI and 2 PI</returns>
        /// <author>SLP/KBW</author>
        public static double DegToLimitedRad(double dDegrees)
        {
            int rotations;
            double dRadians;

            dRadians = dDegrees * DsConstants.dPI / 180.0;

            rotations = (int)(dRadians / DsConstants.d2PI);  // determine if angle is greater that 2 PI

            // if it is, subtract off number of full rotations to get angle between -2 PI and 2 PI
            if (rotations != 0)
                dRadians = dRadians - (DsConstants.d2PI * (double)rotations);

            return dRadians;
        }

        /// <summary>
        /// Convert an angle in radians to degrees, minutes, and seconds
        /// (Ported from Ds Functions.h)
        /// </summary>
        /// <param name="dRadians"></param>
        /// <param name="nDegrees"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// <param name="bNegative"></param>
        /// <author>SLP/KBW</author>
        public static void RadToDMS(double dRadians, ref int nDegrees, ref int nMinutes, ref int nSeconds, ref bool bNegative)
        {

            double dDegrees, dTemp;

            // make sure radians is within range of Pi to -Pi
            if ((float)dRadians > DsConstants.fPI)				// kludge necessesary because of rounding errors
                dRadians = -(DsConstants.d2PI - dRadians);

            if ((float)dRadians < -DsConstants.fPI)
                dRadians = DsConstants.d2PI + dRadians;

            if (dRadians < 0)
                bNegative = true;
            else
                bNegative = false;

            dDegrees = RadToDeg(dRadians);	// first convert radians to degrees

            dDegrees = Math.Abs(dDegrees);		// get absolute value of degrees

            nDegrees = (int)dDegrees;		// get whole number part for degrees

            dTemp = (dDegrees - (nDegrees)) * 60.0;

            nMinutes = (int)dTemp;

            nSeconds = (int)Math.Floor(((dTemp - (nMinutes)) * 60) + 0.5);

            if ((nSeconds) == 60)
            {
                (nMinutes)++;
                nSeconds = 0;
            }

            if (nMinutes == 60)
            {
                (nDegrees)++;
                nMinutes = 0;
            }
        }

        /// <summary>
        /// Convert an angle in radians to degrees, minutes, and seconds
        /// (Ported from Ds Functions.h)
        /// </summary>
        /// <param name="dRadians"></param>
        /// <param name="nDegrees"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// <param name="bNegative"></param>
        /// <author>SLP/KBW/JMG</author>
        public static void RadToDMS(double dRadians, ref double nDegrees, ref int nMinutes, ref int nSeconds)
        {
            bool bNegative;
            double dDegrees, dTemp;

            // make sure radians is within range of Pi to -Pi
            if ((float)dRadians > DsConstants.fPI)				// kludge necessesary because of rounding errors
                dRadians = -(DsConstants.d2PI - dRadians);

            if ((float)dRadians < -DsConstants.fPI)
                dRadians = DsConstants.d2PI + dRadians;

            if (dRadians < 0)
                bNegative = true;
            else
                bNegative = false;

            dDegrees = RadToDeg(dRadians);	// first convert radians to degrees

            dDegrees = Math.Abs(dDegrees);		// get absolute value of degrees

            nDegrees = Math.Truncate(dDegrees);		// get whole number part for degrees

            dTemp = (dDegrees - (nDegrees)) * 60.0;

            nMinutes = (int)dTemp;

            nSeconds = (int)Math.Floor(((dTemp - (nMinutes)) * 60) + 0.5);

            if ((nSeconds) == 60)
            {
                (nMinutes)++;
                nSeconds = 0;
            }

            if (nMinutes == 60)
            {
                (nDegrees)++;
                nMinutes = 0;
            }

            if ((bNegative))
            {
                if (nDegrees > 0.0)
                    nDegrees = -nDegrees;
                else if (nDegrees == 0.0)
                    nDegrees = -0.0;
            }
        }

        /// <summary>
        /// Convert an angle in radians to hours, minutes, and seconds
        /// </summary>
        /// <param name="dRadians"></param>
        /// <param name="nHours"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// /// <author>SLP/BJB</author>
        public static void RadToHMS(double dRadians, ref int nHours, ref int nMinutes, ref int nSeconds)
        {
            double dHours, dTemp;

            if (dRadians < 0)
                dRadians = DsConstants.d2PI + dRadians;		// convert angles between 0 and -180 to be between 180 and 360

            dHours = RadToDeg(dRadians) / 15.0;	// first convert radians to hours

            dHours = Math.Abs(dHours);		// get absolute value of hours ???

            nHours = (int)dHours;		// get whole number part for degrees

            dTemp = (dHours - (nHours)) * 60.0;

            nMinutes = (int)dTemp;

            nSeconds = (int)Math.Floor(((dTemp - (nMinutes)) * 60) + 0.5);

            if ((nSeconds) == 60)
            {
                (nMinutes)++;
                nSeconds = 0;
            }

            if (nMinutes == 60)
            {
                (nHours)++;
                nMinutes = 0;
            }

            if (nHours == 24)
                nHours = 0;

            if (dRadians < 0)
                nHours = 0 - nHours;
        }

        /// <summary>
        /// Convert degrees, minutes and seconds into radians.
        /// </summary>
        /// <param name="nHours"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static double DMSToRad(double nDegrees, int nMinutes, int nSeconds)
        {
            return DegToRad(DMSToDeg(nDegrees, nMinutes, nSeconds));
        }

        private static readonly long NegativeZeroBits = BitConverter.DoubleToInt64Bits(-0.0); 

        /// <summary>
        /// Returns true if the double value is the IEEE representation of -0.0
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static bool IsNegativeZero(double x) 
        { 
            return BitConverter.DoubleToInt64Bits(x) == NegativeZeroBits;
        }

        /// <summary>
        /// A variant of the double.Parse function that preserves -0
        /// </summary>
        /// <param name="sVal"></param>
        /// <returns></returns>
        public static double ParsePreserveNegZero(string sVal)
        {
            double dVal = double.Parse(sVal);
            if (dVal == 0.0 && 
                sVal.StartsWith("-"))
                return -0.0;
            return dVal;
        }

        /// <summary>
        /// A variant of the double.TryParse function that preserves -0
        /// </summary>
        /// <param name="sVal"></param>
        /// <returns></returns>
        public static bool TryParsePreserveNegZero(string sVal, out double dVal)
        {
            bool bSuccess = double.TryParse(sVal, out dVal);
            if (bSuccess && 
                dVal == 0.0 && 
                sVal.StartsWith("-"))
                dVal = -0.0;
            return bSuccess;
        }

        /// <summary>
        /// Converts degrees, minutes, and seconds into a decimal degree
        /// </summary>
        /// <param name="nDegrees">degrees</param>
        /// <param name="nMinutes">minutes</param>
        /// <param name="nSeconds">seconds</param>
        /// <returns>decimal equivalent of degrees, minutes & seconds</returns>
        /// <author>SLP/KBW</author>
        public static double DMSToDeg(double nDegrees, int nMinutes, int nSeconds)
        {
            double dDegrees;

            if (nDegrees < 0 || IsNegativeZero(nDegrees))	
                // if a negative angle, subtract minutes and seconds
                dDegrees = (double)nDegrees - (((double)nMinutes + ((double)nSeconds / 60.0)) / 60.0);
            else
                dDegrees = (double)nDegrees + (((double)nMinutes + ((double)nSeconds / 60.0)) / 60.0);

            return dDegrees;
        }

        /// <summary>
        /// Converts degrees, minutes, and seconds into a decimal degree
        /// </summary>
        /// <param name="nDegrees">degrees</param>
        /// <param name="nMinutes">minutes</param>
        /// <param name="nSeconds">seconds</param>
        /// <returns>decimal equivalent of degrees, minutes & seconds</returns>
        /// <author>SLP/KBW</author>
        public static double DMSToDeg(double nDegrees, double nMinutes, double nSeconds)
        {
            double dDegrees;

            if (nDegrees < 0 || IsNegativeZero(nDegrees))
                // if a negative angle, subtract minutes and seconds
                dDegrees = (double)nDegrees - (((double)nMinutes + ((double)nSeconds / 60.0)) / 60.0);
            else
                dDegrees = (double)nDegrees + (((double)nMinutes + ((double)nSeconds / 60.0)) / 60.0);

            return dDegrees;
        }

        /// <summary>
        /// Converts the value into degrees, minutes, seconds, and returns the 
        /// requested part.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="DsLatLonParts"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        public static int GetDMS(double value, DsLatLonParts latLonPart)
        {
            int nDegrees = 0;
            int nMinutes = 0;
            int nSeconds = 0;
            bool bNegative = false;
            MathUtilities.RadToDMS(value, ref nDegrees, ref nMinutes, ref nSeconds, ref bNegative);
            switch (latLonPart)
            {
                case DsLatLonParts.Degrees: return nDegrees;
                case DsLatLonParts.Minutes: return nMinutes;
                case DsLatLonParts.Seconds: return nSeconds;
                default: return 0;
            }
        }

        /// <summary>
        /// Converts the original value into degrees, minutes and seconds.  Then replaces the
        /// designated lat/lon part with the new part value, and converts this into a decimal
        /// equivalent to send to the host as radians.
        /// </summary>
        /// <param name="origValue"></param>
        /// <param name="latLonPart"></param>
        /// <param name="nNewPartValue"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        public static double GetRadFromDMS(double origValue, DsLatLonParts latLonPart, int nNewPartValue)
        {
            int nDegrees = 0;
            int nMinutes = 0;
            int nSeconds = 0;
            bool bNegative = false;
            MathUtilities.RadToDMS(origValue, ref nDegrees, ref nMinutes, ref nSeconds, ref bNegative);
            switch (latLonPart)
            {
                case DsLatLonParts.Degrees: nDegrees = nNewPartValue; break;
                case DsLatLonParts.Minutes: nMinutes = nNewPartValue; break;
                case DsLatLonParts.Seconds: nSeconds = nNewPartValue; break;
                default: break;
            }
            if ((bNegative) && (nDegrees > 0))
                nDegrees = -nDegrees;
            double dDegrees = MathUtilities.DMSToDeg(nDegrees, nMinutes, nSeconds);
            return MathUtilities.DegToLimitedRad(dDegrees);
        }

        /// <summary>
        /// Converts the value into degrees, minutes, seconds, and returns 
        /// whether it is negative.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="DsLatLonParts"></param>
        /// <returns></returns>
        public static bool IsDMSNegative(double value)
        {
            int nDegrees = 0;
            int nMinutes = 0;
            int nSeconds = 0;
            bool bNegative = false;
            MathUtilities.RadToDMS(value, ref nDegrees, ref nMinutes, ref nSeconds, ref bNegative);
            return bNegative;
        }

        /// <summary>
        /// Convert degrees into (double) radians.
        /// </summary>
        /// <param name="dDegrees"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static double DegToRad(double dDegrees)
        {
            return dDegrees * DsConstants.dPI / 180.0;
        }

        /// <summary>
        /// Convert degrees into (float) radians.
        /// </summary>
        /// <param name="fDegrees"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static float DegToRad(float fDegrees)
        {
            return (float)(fDegrees * DsConstants.fPI / 180.0);
        }

        /// <summary>
        /// Convert decimal hours into radians.
        /// </summary>
        /// <param name="dDegrees"></param>
        /// <returns></returns>
        /// <author>KBW</author>
        public static double HoursToRad(double dDecHours)
        {
            double dRad = (dDecHours * (DsConstants.dPI / 180.0)) * 15.0;
            return dRad;
        }

        /// <summary>
        /// Convert hours, minutes and seconds into degrees.
        /// </summary>
        /// <param name="nHours"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static double HMSToDeg(int nHours, int nMinutes, int nSeconds)
        {
            return DMSToDeg((double)nHours, nMinutes, nSeconds) * 15.0;
        }

        /// <summary>
        /// Convert hours, minutes and seconds into radians.
        /// </summary>
        /// <param name="nHours"></param>
        /// <param name="nMinutes"></param>
        /// <param name="nSeconds"></param>
        /// <returns></returns>
        /// <author>SLP/KBW</author>
        public static double HMSToRad(int nHours, int nMinutes, int nSeconds)
        {
            return DegToRad(HMSToDeg(nHours, nMinutes, nSeconds));
        }
        #endregion

        #region Distance Unit conversion

        /// <summary>
        /// This converts parsecs to meters
        /// </summary>
        /// <param name="parsecs">The value in parsecs</param>
        /// <returns>The value in meters</returns>
        public static double ParsecsToMeters(double parsecs)
        {
            return parsecs * DsConstants.dMETERS_IN_PARSEC;
        }

        /// <summary>
        /// Performs a single-precision conversion to meters, exactly as the BIN loader, and Star Shader
        /// does, so that the exact bit-for-bit position is computed correctly
        /// </summary>
        /// <param name="parsecs"></param>
        /// <returns></returns>
        public static float ParsecsToMeters_SinglePrecision(float parsecs)
        {
            const float fPARSEC_IN_METERS = (float)3.08567758128e+16;
            return parsecs * fPARSEC_IN_METERS;
        }

        /// <summary>
        /// This converts meters to parsecs
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in parsecs</returns>
        public static double MetersToParsecs(double meters)
        {
            return meters / DsConstants.dMETERS_IN_PARSEC;
        }

        /// <summary>
        /// This converts AU to meters
        /// </summary>
        /// <param name="AU">The value in AU</param>
        /// <returns>The value in meters</returns>
        public static double AUToMeters(double AU)
        {
            return AU * DsConstants.dMETERS_IN_ASTRONOMICAL_UNIT;
        }

        /// <summary>
        /// This converts meters to AU
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in AU</returns>
        public static double MetersToAU(double meters)
        {
            return meters / DsConstants.dMETERS_IN_ASTRONOMICAL_UNIT;
        }

        /// <summary>
        /// This converts lightYears to meters
        /// </summary>
        /// <param name="lightYears">The value in lightYears</param>
        /// <returns>The value in meters</returns>
        public static double LightYearsToMeters(double lightYears)
        {
            return lightYears * DsConstants.dMETERS_IN_LIGHTYEAR;
        }

        /// <summary>
        /// This converts meters to lightYears
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in lightYears</returns>
        public static double MetersToLightYears(double meters)
        {
            return meters / DsConstants.dMETERS_IN_LIGHTYEAR;
        }

        /// <summary>
        /// This converts miles to meters
        /// </summary>
        /// <param name="miles">The value in miles</param>
        /// <returns>The value in meters</returns>
        public static double MilesToMeters(double miles)
        {
            return miles * DsConstants.dMETERS_IN_MILE;
        }

        /// <summary>
        /// This converts meters to miles
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in miles</returns>
        public static double MetersToMiles(double meters)
        {
            return meters / DsConstants.dMETERS_IN_MILE;
        }

        /// <summary>
        /// This converts kilometers to meters
        /// </summary>
        /// <param name="kilometers">The value in kilometers</param>
        /// <returns>The value in meters</returns>
        public static double KilometersToMeters(double kilometers)
        {
            return kilometers * DsConstants.dMETERS_IN_KILOMETER;
        }

        /// <summary>
        /// This converts meters to kilometers
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in kilometers</returns>
        public static double MetersToKilometers(double meters)
        {
            return meters / DsConstants.dMETERS_IN_KILOMETER;
        }

        /// <summary>
        /// This converts sun radii to meters
        /// </summary>
        /// <param name="parsecs">The value in sun radii</param>
        /// <returns>The value in meters</returns>
        public static double SunRadiiToMeters(double sunRadii)
        {
            return sunRadii * DsConstants.dSUN_RADIUS_METERS;
        }

        /// <summary>
        /// This converts meters to sun radii
        /// </summary>
        /// <param name="meters">The value in meters</param>
        /// <returns>The value in sun radii</returns>
        public static double MetersToSunRadii(double meters)
        {
            return meters / DsConstants.dSUN_RADIUS_METERS;
        }

        
        //////////////////////////////////////////////////////////////////////////////////////////
        // Convert date rate/step from various units to solar days
        public static double ConvertDateUnitToDays(double dDate, DsDateUnit eUnit)
        {
	        switch (eUnit)
	        {
	        case DsDateUnit.Seconds:
		        return dDate / DsConstants.dSECONDS_IN_DAY;

	        case DsDateUnit.Minutes:
                return dDate / DsConstants.dMINUTES_IN_DAY;

	        case DsDateUnit.Hours:
                return dDate / DsConstants.dHOURS_IN_DAY;

	        case DsDateUnit.Sdays:
                return dDate / DsConstants.dSIDEREAL_DAYS_PER_DAY;

	        case DsDateUnit.Days:
		        return dDate;
		
	        case DsDateUnit.Smonths:
                return dDate * DsConstants.dDAYS_PER_SIDEREAL_MONTH;
		
	        case DsDateUnit.Months:
                return dDate * DsConstants.dDAYS_PER_SYNODIC_MONTH;

	        case DsDateUnit.Years:
                return dDate * DsConstants.dDAYS_PER_TROPICAL_YEAR;
		
	        case DsDateUnit.Syears:
                return dDate * DsConstants.dDAYS_PER_SIDEREAL_YEAR;

	        default:
		        return 0;

	        }
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        // Convert date rate/step from solar days to various units
        public static double ConvertDaysToDateUnit(double dDays, DsDateUnit eUnit)
        {
	        switch (eUnit)
	        {
	        case DsDateUnit.Seconds:
		        return dDays * DsConstants.dSECONDS_IN_DAY;

	        case DsDateUnit.Minutes:
                return dDays * DsConstants.dMINUTES_IN_DAY;

	        case DsDateUnit.Hours:
                return dDays * DsConstants.dHOURS_IN_DAY;

	        case DsDateUnit.Sdays:
                return dDays * DsConstants.dSIDEREAL_DAYS_PER_DAY;

	        case DsDateUnit.Days:
		        return dDays;
		
	        case DsDateUnit.Smonths:
                return dDays / DsConstants.dDAYS_PER_SIDEREAL_MONTH;
		
	        case DsDateUnit.Months:
                return dDays / DsConstants.dDAYS_PER_SYNODIC_MONTH;

	        case DsDateUnit.Years:
                return dDays / DsConstants.dDAYS_PER_TROPICAL_YEAR;
		
	        case DsDateUnit.Syears:
                return dDays / DsConstants.dDAYS_PER_SIDEREAL_YEAR;

	        default:
		        return 0;

	        }
        }


        //////////////////////////////////////////////////////////////////////////////////////////
        public static double ConvertMetersToUnit(double dValue, DsDistanceUnit eUnit)
        {																								// (SLP)
            switch (eUnit)
            {
                case DsDistanceUnit.Meter:
                    return dValue;		// no conversion necessary

                case DsDistanceUnit.Kilometer:
                    return dValue /= DsConstants.dMETERS_IN_KILOMETER;

                case DsDistanceUnit.Mile:
                    return dValue /= DsConstants.dMETERS_IN_MILE;

                case DsDistanceUnit.NauticalMile:
                    return dValue /= DsConstants.dMETERS_IN_NAUTICAL_MILE;

                case DsDistanceUnit.AstronomicalUnit:
                    return dValue /= DsConstants.dMETERS_IN_ASTRONOMICAL_UNIT;

                case DsDistanceUnit.LightYear:
                    return dValue /= DsConstants.dMETERS_IN_LIGHTYEAR;

                case DsDistanceUnit.Parsec:
                    return dValue /= DsConstants.dMETERS_IN_PARSEC;

                case DsDistanceUnit.MegaParsec:
                    return dValue /= DsConstants.dMETERS_IN_MEGA_PARSEC;

                case DsDistanceUnit.Rsun:
                    return dValue /= DsConstants.dSUN_RADIUS_METERS;

                case DsDistanceUnit.Rearth:
                    return dValue /= DsConstants.dEARTH_RADIUS_METERS;

                case DsDistanceUnit.Rmoon:
                    return dValue /= DsConstants.dMOON_RADIUS_METERS;

                case DsDistanceUnit.Rmercury:
                    return dValue /= DsConstants.dMERCURY_RADIUS_METERS;

                case DsDistanceUnit.Rvenus:
                    return dValue /= DsConstants.dVENUS_RADIUS_METERS;

                case DsDistanceUnit.Rmars:
                    return dValue /= DsConstants.dMARS_RADIUS_METERS;

                case DsDistanceUnit.Rjupiter:
                    return dValue /= DsConstants.dJUPITER_RADIUS_METERS;

                case DsDistanceUnit.Rsaturn:
                    return dValue /= DsConstants.dSATURN_RADIUS_METERS;

                case DsDistanceUnit.Ruranus:
                    return dValue /= DsConstants.dURANUS_RADIUS_METERS;

                case DsDistanceUnit.Rneptune:
                    return dValue /= DsConstants.dNEPTUNE_RADIUS_METERS;

                case DsDistanceUnit.Rpluto:
                    return dValue /= DsConstants.dPLUTO_RADIUS_METERS;

                default:
                    return 0;
            } // end switch
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        public static double ConvertDistanceToMeters(double dDistance, DsDistanceUnit eUnit)
        {																								// (SLP)
            switch (eUnit)
            {
                case DsDistanceUnit.Meter:
                    return dDistance;		// no conversion necessary

                case DsDistanceUnit.Kilometer:
                    return dDistance *= DsConstants.dMETERS_IN_KILOMETER;

                case DsDistanceUnit.Mile:
                    return dDistance *= DsConstants.dMETERS_IN_MILE;

                case DsDistanceUnit.NauticalMile:
                    return dDistance *= DsConstants.dMETERS_IN_NAUTICAL_MILE;

                case DsDistanceUnit.AstronomicalUnit:
                    return dDistance *= DsConstants.dMETERS_IN_ASTRONOMICAL_UNIT;

                case DsDistanceUnit.LightYear:
                    return dDistance *= DsConstants.dMETERS_IN_LIGHTYEAR;

                case DsDistanceUnit.Parsec:
                    return dDistance *= DsConstants.dMETERS_IN_PARSEC;

                case DsDistanceUnit.MegaParsec:
                    return dDistance *= DsConstants.dMETERS_IN_MEGA_PARSEC;

                case DsDistanceUnit.Rsun:
                    return dDistance *= DsConstants.dSUN_RADIUS_METERS;

                case DsDistanceUnit.Rearth:
                    return dDistance *= DsConstants.dEARTH_RADIUS_METERS;

                case DsDistanceUnit.Rmoon:
                    return dDistance *= DsConstants.dMOON_RADIUS_METERS;

                case DsDistanceUnit.Rmercury:
                    return dDistance *= DsConstants.dMERCURY_RADIUS_METERS;

                case DsDistanceUnit.Rvenus:
                    return dDistance *= DsConstants.dVENUS_RADIUS_METERS;

                case DsDistanceUnit.Rmars:
                    return dDistance *= DsConstants.dMARS_RADIUS_METERS;

                case DsDistanceUnit.Rjupiter:
                    return dDistance *= DsConstants.dJUPITER_RADIUS_METERS;

                case DsDistanceUnit.Rsaturn:
                    return dDistance *= DsConstants.dSATURN_RADIUS_METERS;

                case DsDistanceUnit.Ruranus:
                    return dDistance *= DsConstants.dURANUS_RADIUS_METERS;

                case DsDistanceUnit.Rneptune:
                    return dDistance *= DsConstants.dNEPTUNE_RADIUS_METERS;

                case DsDistanceUnit.Rpluto:
                    return dDistance *= DsConstants.dPLUTO_RADIUS_METERS;

                default:
                    return 0;
            } // end switch
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        public static double ConvertDistanceToUnit(double dDistance, DsDistanceUnit eUnitIn,
                                     DsDistanceUnit eUnitOut)
        {																				// (BJB)
            double dValue;
            if (eUnitIn == eUnitOut)
                return dDistance;

            // Convert the distance to meters first
            dValue = ConvertDistanceToMeters(dDistance, eUnitIn);

            // Convert from meters to the output unit
            dValue = ConvertMetersToUnit(dValue, eUnitOut);

            return dValue;
        }

        #endregion

        #region Misc Math Helper Functions

        /// <summary>
        /// Clamps the value between a min and a max
        /// </summary>
        /// <param name="dValue"></param>
        /// <param name="dMin"></param>
        /// <param name="dMax"></param>
        /// <returns></returns>
        public static double Clamp(double dValue, double dMin, double dMax)
        {
            if (dValue < dMin) return dMin;
            if (dValue > dMax) return dMax;
            return dValue;
        }

        #endregion
        #endregion

    }

}
