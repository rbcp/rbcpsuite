﻿namespace DsFsharp

module DsF =

    open System.Collections.Generic
    open ES.Ds.Common


    // table type for the name and id tables of DsObjects and DsClasses.
    //
    //XXX: o.expire is the only thing preventing us from making this class generic
    //
    type object_table (dsGetID) =
        let _getID = dsGetID
        let _by_name = new Dictionary<string,DsObjectBase * int16 option>()
        let _by_id = new Dictionary<int16,string>()

        member this.expire_id (id:int16) =
            match _by_id.TryGetValue(id) with
            | true, name ->
                _by_id.Remove(id) |> ignore
                match _by_name.TryGetValue(name) with
                | true, (o, idO) ->
                    _by_name.[name] <- (o, None)
                    o.expire()
                | _ -> ()
            | _ -> ()

        member this.expire_all () =
            _by_id.Clear()
            for name in List _by_name.Keys do
                let o, idO = _by_name.[name]
                _by_name.[name] <- (o, None)
                o.expire()

        member this.add name idO (o:DsObjectBase) =
            _by_name.Add(name, (o, idO))
            if idO.IsSome then
                _by_id.Add(idO.Value, name)

        member this.add_id name id =
            match _by_name.TryGetValue(name) with
            | true, (o, idO) ->
                _by_id.[id] <- name
                _by_name.[name] <- (o, Some id)
            | _ -> ()

        member this.remove name =
            match _by_name.TryGetValue(name) with
            | true, (o, Some id) ->
                _by_name.Remove(name) |> ignore
                _by_id.Remove(id) |> ignore
            | true, (o, None) ->
                _by_name.Remove(name) |> ignore
            | _ -> ()

        member this.get_by_name name =
            match _by_name.TryGetValue(name) with
            | true, (o, _) -> Some o
            | _ -> None

        member this.get_by_id (id:int16) =
            match _by_id.TryGetValue(id) with
            | true, name -> this.get_by_name name
            | _ -> None

        member this.get_id name =
            match _by_name.TryGetValue(name) with
            | true, (o, Some id) -> id
            | true, (o, None) ->
                match _getID(name) with
                | Some id ->
                    _by_name.[name] <- (o, Some id)
                    _by_id.[id] <- name
                    id
                | None ->
                    //XXX: use our own exception type
                    raise <| DsAPIErrorException(DsAPIError.OmerrorObjectNotFound,
                                                 "object_table.get_id")
            | _ -> raise <| exn ("Object not in table: "+name)


    // base class for DsObject and DsClass
    //
    and DsObjectBase (name, table) =
        let _name = name
        let _table = table : object_table

        abstract member expire : unit -> unit
        default this.expire () =
            ()

        member this.name
            with get () = _name

        member this.id
            with get () = _table.get_id _name
            and set (id) = _table.add_id _name id

        interface System.IDisposable with
            member this.Dispose () =
                _table.remove _name


    //
    // DsClass
    //

    let private classes = new object_table(DsGetClassID)

    type DsClassAttrType =
        | Scalar of DsAttr
        | Array of DsAttr
        | Enum of int16

    type AttributeConstraint =
        | AttributeDefault of DsAttribute
        | AttributeMinimum of DsAttribute
        | AttributeMaximum of DsAttribute

    type AttrUpdateEvent (dsclass:DsClass, attr:DigistarClassAttribute) as this =
        inherit Event<string * string * int16> ()

        let dsCallbackHandler = new ClassAttrUpdateCallback(fun oid attridx elemidx ->
            let dsobName = DsGetObjectName oid
            let attrName = attr.Name
            this.Trigger((dsobName, attrName, elemidx)))

        member this.Install () =
            DsAddClassAttrUpdateCallBack dsclass.id attr.Name dsCallbackHandler


    and CommandEvent (dsclass:DsClass, command:DigistarClassCommand) as this =
        inherit Event<DsClass * string * string> ()

        let dsCallbackHandler = new ClassCommandCallback(fun oid cmdidx ->
            let dsobName = DsGetObjectName oid
            let commandName = command.Name
            this.Trigger((dsclass, dsobName, commandName)))

        member this.Install () =
            DsAddClassCommandCallBack dsclass.id command.Name dsCallbackHandler


    and DigistarClassAttribute (dsclass, name) as this =
        let onUpdate = new AttrUpdateEvent(dsclass, this)
        let mutable index = None

        member this.expire () =
            index <- None

        member this.Name = name

        member this.Index
            with get () =
                match index with
                | Some i -> i
                | None ->
                    let i = DsGetClassAttrIndex dsclass.id name
                    index <- Some i
                    i

        member this.InstallCallbacks () =
            onUpdate.Install()

        [<CLIEvent>]
        member this.OnUpdate = onUpdate.Publish


    and DigistarClassCommand (dsclass, name) as this =
        let onCommand = new CommandEvent(dsclass, this)
        let mutable index = None

        member this.expire () =
            index <- None

        member this.Name = name

        member this.Index
            with get () =
                match index with
                | Some i -> i
                | None ->
                    let i = DsGetClassCommandIndex dsclass.id name
                    index <- Some i
                    i

        member this.InstallCallbacks () =
            onCommand.Install()

        [<CLIEvent>]
        member this.OnCommand = onCommand.Publish

    and DsClass (name) =
        inherit DsObjectBase (name, classes)
        let _attributes = new Dictionary<string,DigistarClassAttribute>()
        let _commands = new Dictionary<string,DigistarClassCommand>()
        let onLock = new Event<_>()
        let onCreate = new Event<_>()
        //XXX: gross mechanism to help tell the difference between non-existent
        //     and unlocked classes.
        let mutable _idUnlocked = None

        override this.expire () =
            _attributes.Values |> Seq.iter(fun x -> x.expire())
            _commands.Values |> Seq.iter(fun x -> x.expire())
            base.expire()

        member this.GetAttribute (attrName) =
            match _attributes.TryGetValue(attrName) with
            | true, attr -> attr
            | _ ->
                let attr = new DigistarClassAttribute(this, attrName)
                _attributes.Add(attrName, attr)
                attr

        member this.GetCommand (commandName) =
            match _commands.TryGetValue(commandName) with
            | true, command -> command
            | _ ->
                let command = new DigistarClassCommand(this, commandName)
                _commands.Add(commandName, command)
                command

        [<CLIEvent>]
        member this.OnCreate = onCreate.Publish

        member this.TriggerOnCreate id =
            onCreate.Trigger(this, id)

        [<CLIEvent>]
        member this.OnLock = onLock.Publish

        //XXX: may be triggered by Ensure, when it's not a real lock event, therefore
        //     it's not certain that objects need to be created.
        member this.TriggerOnLock () =
            onLock.Trigger(this)
            _attributes.Values |> Seq.iter(fun x -> x.InstallCallbacks())
            _commands.Values |> Seq.iter(fun x -> x.InstallCallbacks())

        member this.Create classType attributes commands =
            let id = DsCreateClass base.name classType
            //XXX: the _idUnlocked mechanism is pretty gross, but something
            //     like this will be necessary unless there is a way in the
            //     API to tell the difference between a non-existent and an
            //     unlocked class.
            _idUnlocked <- Some id
            try
                for attrName, attrType, attrFlags, constraints in attributes do
                    let idx = match attrType with
                              | Scalar t -> DsAddClassAttr id attrName t attrFlags
                              | Array t -> DsAddClassAttrArray id attrName t attrFlags
                              | Enum t -> DsAddClassEnumAttr id attrName t attrFlags
                    for attrconstraint in constraints do
                        match attrconstraint with
                        | AttributeDefault v -> DsSetClassAttrDefMinMax id idx DsAttrDef.Default v
                        | AttributeMinimum v -> DsSetClassAttrDefMinMax id idx DsAttrDef.Minimum v
                        | AttributeMaximum v -> DsSetClassAttrDefMinMax id idx DsAttrDef.Maximum v
                        |> ignore
                commands |> Seq.iter(ignore << DsAddClassCommand id)
            finally
                DsLockClass id
                base.id <- id
                _idUnlocked <- None
            this

        member this.Ensure classType attributes commands =
            //XXX: do some more checks to ensure that the
            //     existing class is what we think it is.

            //XXX: DsGetClassID is not a perfect existence test
            //     because it can also mean not locked
            match DsGetClassID this.name with
            | None ->
                if _idUnlocked.IsNone then
                    this.Create classType attributes commands
                else
                    DsLockClass _idUnlocked.Value
                    base.id <- _idUnlocked.Value
                    _idUnlocked <- None
                    this
            | Some id ->
                base.id <- id
                this.TriggerOnLock()
                this


    let GetClass name =
        match classes.get_by_name name with
        | Some o -> o :?> DsClass
        | None -> let o = new DsClass(name)
                  classes.add o.name None (o :> DsObjectBase)
                  o

    let GetClassByID id =
        match classes.get_by_id id with
        | Some o -> (o :?> DsClass)
        | None -> let cname = DsGetClassName id
                  let c = GetClass cname
                  c.id <- id
                  c

    // MaybeGetClassByID is for callback class lookups, where we only want the
    // object if it is already in the table.
    let MaybeGetClassByID id =
        match classes.get_by_id id with
        | Some o -> Some (o :?> DsClass)
        | None -> None


    //
    // DsObject
    //

    let private objects = new object_table(DsGetObjectID)

    type DsObject (name) =
        inherit DsObjectBase (name, objects)
        let mutable _dsclass = None
        let onCreate = new Event<_>()

        override this.expire () =
            _dsclass <- None
            base.expire()

        member this.dsclass
            with get () =
                match _dsclass with
                | Some c -> c
                | None ->
                    let cid = DsGetObjectClassID this.id
                    let c = GetClassByID cid
                    _dsclass <- Some c
                    c

        member this.GetAttr attr =
            let idx = this.dsclass.GetAttribute(attr).Index
            let attrtype = DsGetClassAttrType this.dsclass.id idx
            DsCreateAttributeFromType attrtype |> DsGetObjectAttr this.id idx

        member this.GetArraySize attr =
            let idx = this.dsclass.GetAttribute(attr).Index
            DsGetObjectArraySize this.id idx

        member this.SetAttr attr value =
            let idx = this.dsclass.GetAttribute(attr).Index
            DsSetObjectAttr this.id idx value true

        member this.SetArrayElement attr elemIndex value =
            let idx = this.dsclass.GetAttribute(attr).Index
            DsSetObjectArrayElem this.id idx elemIndex value

        member this.ExecuteCommand command =
            let idx = this.dsclass.GetCommand(command).Index
            DsExecuteObjectCommand this.id idx

        member this.AddChild (dsob:DsObject) =
            DsAddObjectChild this.id dsob.id

        member this.Create dsclass objecttype =
            _dsclass <- Some dsclass
            base.id <- DsCreateObject this.name dsclass.id objecttype
            this

        member this.Clone name objecttype =
            let cloneid = DsCloneObject name this.id objecttype
            new DsObject(name)

        member this.CloneUnique prefix objecttype =
            let random = System.Random()
            let rec doclone () =
                try
                    let i = random.Next(10000000)
                    let tryname = prefix + i.ToString()
                    (DsCloneObject tryname this.id objecttype, tryname)
                with
                | :? DsAPIErrorException ->
                    doclone ()
            let cloneid, tryname = doclone ()
            new DsObject(tryname)

        [<CLIEvent>]
        member this.OnCreate = onCreate.Publish

        member this.TriggerOnCreate arg =
            onCreate.Trigger(this, arg)


    let GetObject name =
        match objects.get_by_name name with
        | Some o -> o :?> DsObject
        | None -> let o = new DsObject(name)
                  objects.add o.name None (o :> DsObjectBase)
                  o

    let GetObjectByID id =
        match objects.get_by_id id with
        | Some o -> (o :?> DsObject)
        | None -> let name = DsGetObjectName id
                  let o = GetObject name
                  o.id <- id
                  o

    let GetObjectCreateUnique =
        let counters = Dictionary<string,int>() //XXX: maybe this should be part of
                                                //     our table, where we could clear
                                                //     it out on disconnect.
        fun prefix (dsclass:DsClass) objectType ->
            let clsid = dsclass.id
            let mutable id = 0s
            let rec trymake c =
                let name = prefix + c.ToString()
                let error : DsAPIError = enum(DsAPI.DsCreateObject(name, clsid, &id, objectType))
                match error with
                | DsAPIError.None -> (name, c, id)
                | _ -> trymake (c + 1)
            let inner () = match counters.TryGetValue(prefix) with
                           | true, c -> trymake (c+1)
                           | _ -> counters.Add(prefix, -1)
                                  trymake 0
            let (name, c, id) = inner()
            counters.[prefix] <- c
            let o = GetObject name
            o.id <- id
            o

    // MaybeGetObjectByID is for callback object lookups where we only want the
    // object if it is already in the table.
    let MaybeGetObjectByID id =
        match objects.get_by_id id with
        | Some o -> Some (o :?> DsObject)
        | None -> None


    //
    // Connection: Object Table Management
    //

    let private onPreClearTables = new ClearTablesCallback(fun _ ->
        objects.expire_all()
        classes.expire_all())

    let private onClassLocked = new ClassLockedCallback(fun id ->
        match MaybeGetClassByID id with
        | Some c -> c.TriggerOnLock()
        | None -> ())

    let private onObjectCreated = new DsAddObjectPostCreateCallBack(fun id ->
        let cid = DsGetObjectClassID id
        match MaybeGetClassByID cid with
        | Some c -> c.TriggerOnCreate id
        | None -> ()
        match MaybeGetObjectByID id with
        | Some o -> o.TriggerOnCreate id
        | None -> ())

    let private onObjectDeleted = new DsAddObjectPostDeleteCallBack(fun id ->
        // an object recreated after a fadestopreset may get a new id
        objects.expire_id id)


    //
    // Connection Logic
    //

    type private ConnectionState =
        | Connected
        | NotConnected
        | InitPending

    let mutable private connected = NotConnected
    let mutable private onAPIConnectClientCallback = fun (_:bool) -> ()
    let mutable private onAPIReadyClientCallback = fun _ -> ()


    let private onPostInitOrSync = new InitOrSyncCallback(fun clearTableType ->
        // According to my testing, PostInitOrSync normally fires after APIConnected,
        // and only then is it safe to start creating objects.  However, it can also
        // happen that PostInitOrSync fires just before APIConnected (particularly if
        // the plugin was started before Digistar), in which case, it's not safe to
        // start creating objects until after APIConnected has fired.  To deal with
        // this, we consider the connection state as tristate, and introduce an event
        // of our own called APIReady, which may be fired here or after APIConnected,
        // as needed.
        match connected with
        | Connected -> onAPIReadyClientCallback()
        //XXX: clearTableType may also help to inform us whether to go into
        //     InitPending or to stay NotConnected
        | _ -> connected <- InitPending)


    let private onAPIConnect = new DsAPIConnectCallBack(fun c ->
        let connectionState = connected
        if c then
            connected <- Connected
            DsAddPreClearTblsCallBack onPreClearTables // clear class-specific callbacks & IDs
            DsAddClassLockedCallBack onClassLocked // install class-specific callbacks and create user objects based on user classes
            DsAddObjectPostCreateCallBack onObjectCreated // set default values on user objects
            DsAddObjectPostDeleteCallBack onObjectDeleted // remove object from internal tables
        else
            connected <- NotConnected
            // DsAPI callbacks are automatically cleared here, with a couple of exceptions:
            //    SubscribeToMessages, PostInitOrSync.
            classes.expire_all()
            objects.expire_all()
        onAPIConnectClientCallback(c)
        if c && connectionState = InitPending then
            onAPIReadyClientCallback())


    let Connect pluginName onAPIConnectCallback onAPIReadyCallback =
        onAPIConnectClientCallback <- onAPIConnectCallback
        onAPIReadyClientCallback <- onAPIReadyCallback
        DsConnect pluginName onAPIConnect
        DsAddPostInitOrSyncCallBack(onPostInitOrSync)
