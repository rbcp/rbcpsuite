﻿namespace DsFsharp

[<AutoOpen>]
module DsFsharp =

    open System
    open System.Text
    open System.Windows.Media
    open ES.Ds.Common

    //
    // Exceptions
    //

    type DsAPIErrorException (error, apimethod, data) as this =
        inherit Exception(error.ToString())
        do
            this.Data.Add("DsAPIError", error)
            this.Data.Add("method", apimethod)
            data |> Map.iter (fun k v -> this.Data.Add(k, v))
        new (error, apimethod) = DsAPIErrorException(error, apimethod, Map.empty)



    //
    // Delegates
    //

    type APIConnectCallback = DsAPIConnectCallBack
    type MessageReceivedCallback = DsMessageReceivedCallBack

    type ClassAttrUpdateCallback = DsAddClassAttrUpdateCallBack
    type ClassCommandCallback = DsAddClassCommandCallBack
    type ClassLockedCallback = DsAddClassLockedCallBack

    type EnumLockedCallback = DsAddEnumLockedCallBack

    type ObjectCreateCallback = DsAddObjectPostCreateCallBack
    type ObjectDeleteCallback = DsAddObjectPreDeleteCallBack
    type ObjectInitCallback = delegate of int16 -> unit
    type ObjectResetCallback = delegate of int16 -> unit
    type ObjectAttrUpdateCallback = DsAddObjectAttrUpdateCallBack
    type ObjectCommandCallback = delegate of int16 * int16 -> unit
    type ObjectChildCallback = DsAddObjectAddChildCallBack

    type ClearTablesCallback = DsAddPreClearTablesCallBack
    type InitOrSyncCallback = DsAddPreInitOrSyncCallBack

    //
    // DsAPI Wrappers
    //
    let DsConnect pluginName callback =
        let error : DsAPIError = enum(DsAPI.DsConnect(pluginName, callback))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsConnect"))

    let DsDisconnect =
        let error : DsAPIError = enum(DsAPI.DsDisconnect())
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsDisconnect"))

    let DsCreateClass className classType =
        let mutable classID = -1s
        let shaderName = ""
        let error : DsAPIError = enum(DsAPI.DsCreateClass(className, classType, &classID, shaderName))
        match error with
        | DsAPIError.None -> classID
        | _ -> raise (DsAPIErrorException(error, "DsCreateClass",
                                          Map [("className", className);
                                               ("classType", classType.ToString())]))

    let DsAddClassAttr nClassID attrName attrType attrFlags =
        let mutable attrIndex = -1s
        let error : DsAPIError = enum(DsAPI.DsAddClassAttr(nClassID, attrName, attrType, attrFlags, &attrIndex))
        match error with
        | DsAPIError.None -> attrIndex
        | _ -> raise (DsAPIErrorException(error, "DsAddClassAttr"))

    let DsAddClassEnumAttr nClassID attrName enumID attrFlags =
        let mutable attrIndex = -1s
        let error : DsAPIError = enum(DsAPI.DsAddClassEnumAttr(nClassID, attrName, enumID, attrFlags, &attrIndex))
        match error with
        | DsAPIError.None -> attrIndex
        | _ -> raise (DsAPIErrorException(error, "DsAddClassEnumAttr"))

    let DsAddClassAttrArray nClassID attrName elemType attrFlags =
        let mutable attrIndex = -1s
        let error : DsAPIError = enum(DsAPI.DsAddClassAttrArray(nClassID, attrName, elemType, attrFlags, &attrIndex))
        match error with
        | DsAPIError.None -> attrIndex
        | _ -> raise (DsAPIErrorException(error, "DsAddClassAttrArray"))

    let DsSetClassAttrDefMinMax nClassID attrIndex attrDef attr =
        let error : DsAPIError = enum(DsAPI.DsSetClassAttrDefMinMax(nClassID, attrIndex, attrDef, attr))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSetClassAttrDefMinMax"))

    let DsAddClassCommand nClassID commandName =
        let mutable commandIndex = -1s
        let error : DsAPIError = enum(DsAPI.DsAddClassCommand(nClassID, commandName, &commandIndex))
        match error with
        | DsAPIError.None -> commandIndex
        | _ -> raise (DsAPIErrorException(error, "DsAddClassCommand"))

    let DsLockClass classID =
        let error : DsAPIError = enum(DsAPI.DsLockClass(classID))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsLockClass"))

    let DsAddClassCommandCallBack nClassID sCmdName callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddClassCommandCallBack(nClassID, sCmdName, callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddClassCommandCallBack"))

    let DsRemoveClassCommandCallBack nClassID sCmdName callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveClassCommandCallBack(nClassID, sCmdName, callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveClassCommandCallBack"))

    let DsAddClassLockedCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddClassLockedCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddClassLockedCallBack"))

    let DsRemoveClassLockedCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveClassLockedCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveClassLockedCallBack"))

    let DsGetClassName nClassID =
        let nNameMaxSize = DsConstants.MAX_NAME_LENGTH
        let szRetName = StringBuilder(nNameMaxSize)
        let error : DsAPIError = enum(DsAPI.DsGetClassName(nClassID, szRetName, nNameMaxSize))
        match error with
        | DsAPIError.None -> szRetName.ToString()
        | _ -> raise (DsAPIErrorException(error, "DsGetClassName"))

    let DsGetClassID sClassName =
        let mutable nClassID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassID(sClassName, &nClassID))
        match error with
        | DsAPIError.None -> Some nClassID
        | DsAPIError.ClassNotFound -> None
        | DsAPIError.LocalObjman -> None // class was not found or is not locked
        | _ -> raise (DsAPIErrorException(error, "DsGetClassID",
                                          Map [("className", sClassName)]))

    let DsGetClassNumAttrs nClassID =
        let mutable nNumAttrs = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassNumAttrs(nClassID, &nNumAttrs))
        match error with
        | DsAPIError.None -> nNumAttrs
        | _ -> raise (DsAPIErrorException(error, "DsGetClassNumAttrs"))

    let DsGetClassAttrType nClassID nAttrIndex =
        let mutable eType = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrType(nClassID, nAttrIndex, &eType))
        match error with
        | DsAPIError.None -> eType
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrType"))

    let DsGetClassAttrArrayType nClassID nAttrIndex =
        let mutable eArrayType = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrArrayType(nClassID, nAttrIndex, &eArrayType))
        match error with
        | DsAPIError.None -> eArrayType
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrArrayType"))

    let DsGetClassAttrQualifierBits nClassID nAttrIndex =
        let mutable attrFlags = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrQualifierBits(nClassID, nAttrIndex, &attrFlags))
        match error with
        | DsAPIError.None -> attrFlags
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrQualifierBits"))

    let DsGetClassAttrFlags nClassID nAttrIndex =
        let mutable attrFlags = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrFlags(nClassID, nAttrIndex, &attrFlags))
        match error with
        | DsAPIError.None -> attrFlags
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrFlags"))

    let DsGetAttrAllowsController eAttrType =
        let mutable eAllows = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetAttrAllowsController(eAttrType, &eAllows))
        match error with
        | DsAPIError.None -> eAllows
        | _ -> raise (DsAPIErrorException(error, "DsGetAttrAllowsController"))

    let DsGetClassAttrReadOnly nClassID nAttrIndex =
        let mutable byIsReadOnly = byte 0
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrReadOnly(nClassID, nAttrIndex, &byIsReadOnly))
        match error with
        | DsAPIError.None -> byIsReadOnly
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrReadOnly"))

    //let DsGetClassAttrDefault nClassID nAttrIndex attrArg =
    //    let error : DsAPIError = enum(DsAPI.DsGetClassAttrDefault(nClassID, nAttrIndex, attrArg))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrDefault"))

    let DsGetClassAttrMax nClassID nAttrIndex (attrArg:DsAttribute) =
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrMax(nClassID, nAttrIndex, attrArg))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrMax"))

    let DsGetClassAttrMin nClassID nAttrIndex (attrArg:DsAttribute) =
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrMin(nClassID, nAttrIndex, attrArg))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrMin"))

    let DsGetClassAttrName nClassID nAttrIndex =
        let nNameMaxSize = DsConstants.MAX_NAME_LENGTH
        let sAttrName = StringBuilder(nNameMaxSize)
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrName(nClassID, nAttrIndex, sAttrName, nNameMaxSize))
        match error with
        | DsAPIError.None -> sAttrName.ToString()
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrName"))

    let DsGetClassAttrIndex nClassID sAttrName =
        let mutable nRetIndex = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrIndex(nClassID,  sAttrName, &nRetIndex))
        match error with
        | DsAPIError.None -> nRetIndex
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrIndex", Map [("attribute",sAttrName)]))

    let DsGetClassCommandIndex nClassID sCmdName =
        let mutable nRetIndex = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassCommandIndex(nClassID, sCmdName, &nRetIndex))
        match error with
        | DsAPIError.None -> nRetIndex
        | _ -> raise (DsAPIErrorException(error, "DsGetClassCommandIndex"))

    let DsGetClassNumCommands nClassID =
        let mutable nNumCmds = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassNumCommands(nClassID, &nNumCmds))
        match error with
        | DsAPIError.None -> nNumCmds
        | _ -> raise (DsAPIErrorException(error, "DsGetClassNumCommands"))

    let DsGetClassCommandName nClassID nCmdIndex =
        let nNameMaxSize = DsConstants.MAX_NAME_LENGTH
        let sCmdName = StringBuilder(nNameMaxSize)
        let error : DsAPIError = enum(DsAPI.DsGetClassCommandName(nClassID, nCmdIndex, sCmdName, nNameMaxSize))
        match error with
        | DsAPIError.None -> sCmdName.ToString()
        | _ -> raise (DsAPIErrorException(error, "DsGetClassCommandName"))

    let DsAddEnumLockedCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddEnumLockedCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddEnumLockedCallBack"))

    let DsRemoveEnumLockedCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveEnumLockedCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveEnumLockedCallBack"))

    let DsGetEnumID sEnumName =
        let mutable nEnumID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetEnumID(sEnumName, &nEnumID))
        match error with
        | DsAPIError.None -> Some nEnumID
        | DsAPIError.LocalObjman -> None // enum not found
        | _ -> raise (DsAPIErrorException(error, "DsGetEnumID"))

    let DsGetEnumName nEnumID szRetName nNameMaxSize =
        let error : DsAPIError = enum(DsAPI.DsGetEnumName(nEnumID, szRetName, nNameMaxSize))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsGetEnumName"))

    let DsGetEnumNumItems nEnumID =
        let mutable nNumItems = 0s
        let error : DsAPIError = enum(DsAPI.DsGetEnumNumItems(nEnumID, &nNumItems))
        match error with
        | DsAPIError.None -> nNumItems
        | _ -> raise (DsAPIErrorException(error, "DsGetEnumNumItems"))

    let DsGetEnumItemName nEnumID nItemIndex szItemName nNameMaxSize =
        let error : DsAPIError = enum(DsAPI.DsGetEnumItemName(nEnumID, nItemIndex, szItemName, nNameMaxSize))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsGetEnumItemName"))

    let DsGetClassAttrEnumID nClassID nAttrIndex =
        let mutable nEnumID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrEnumID(nClassID, nAttrIndex, &nEnumID))
        match error with
        | DsAPIError.None -> nEnumID
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrEnumID"))

    let DsExecuteObjectCommand nObjectID nCmdIndex =
        let error : DsAPIError = enum(DsAPI.DsExecuteObjectCommand(nObjectID, nCmdIndex))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsExecuteObjectCommand"))

    let DsSendStringCommand sCommand =
        let error : DsAPIError = enum(DsAPI.DsSendStringCommand(sCommand))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSendStringCommand",
                                          Map [("command", sCommand)]))

    let DsSendStringCommandBuffer sCommand =
        let error : DsAPIError = enum(DsAPI.DsSendStringCommandBuffer(sCommand))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSendStringCommandBuffer"))

    let DsSendScriptCommands sScriptName sCommands fTimeCodeOffset =
        let error : DsAPIError = enum(DsAPI.DsSendScriptCommands(sScriptName, sCommands, fTimeCodeOffset))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSendScriptCommands"))

    let DsCreateObject name clsid objecttype =
        let mutable idout = 0s
        let error : DsAPIError = enum(DsAPI.DsCreateObject(name, clsid, &idout, objecttype))
        match error with
        | DsAPIError.None -> idout
        | _ -> raise (DsAPIErrorException(error, "DsCreateObject",
                                          Map [("object", name);
                                               ("classid", clsid.ToString());
                                               ("type", objecttype.ToString())]))

    //XXX: this file needs to be better organized
    let DsGetObjectID sObjectName =
        let mutable nRetObjectID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectID(sObjectName, &nRetObjectID))
        match error with
        | DsAPIError.None -> Some nRetObjectID
        | DsAPIError.LocalObjman -> None // object not found
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectID",
                                          Map [("object", sObjectName)]))

    let DsCloneObject name dsobjecttocloneid objecttype =
        let mutable idout = 0s
        let error : DsAPIError = enum(DsAPI.DsCloneObject(name, dsobjecttocloneid, &idout, objecttype))
        match error with
        | DsAPIError.None -> idout
        | _ -> raise (DsAPIErrorException(error, "DsCloneObject"))

    let DsAddObjectPostCreateCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectPostCreateCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectPostCreateCallBack"))

    let DsRemoveObjectPostCreateCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectPostCreateCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectPostCreateCallBack"))

    let DsAddObjectPreDeleteCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectPreDeleteCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectPreDeleteCallBack"))

    let DsRemoveObjectPreDeleteCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectPreDeleteCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectPreDeleteCallBack"))

    let DsAddObjectPostDeleteCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectPostDeleteCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectPostDeleteCallBack"))

    let DsRemoveObjectPostDeleteCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectPostDeleteCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectPostDeleteCallBack"))

    //let DsAddObjectPreInitCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsAddObjectPreInitCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsAddObjectPreInitCallBack"))

    //let DsRemoveObjectPreInitCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsRemoveObjectPreInitCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectPreInitCallBack"))

    //let DsAddObjectPostInitCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsAddObjectPostInitCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsAddObjectPostInitCallBack"))

    //let DsRemoveObjectPostInitCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsRemoveObjectPostInitCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectPostInitCallBack"))

    let DsGetObjectClassID nObjectID =
        let mutable nRetID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectClassID(nObjectID, &nRetID))
        match error with
        | DsAPIError.None -> nRetID
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectClassID",
                                          Map [("objectID", nObjectID.ToString())]))

    let DsGetClassType nClassID =
        let mutable eRetType = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetClassType(nClassID, &eRetType))
        match error with
        | DsAPIError.None -> eRetType
        | _ -> raise (DsAPIErrorException(error, "DsGetClassType"))

    let DsGetObjectName nObjectID =
        let nNameMaxSize = DsConstants.MAX_NAME_LENGTH
        let szRetName = StringBuilder(nNameMaxSize)
        let error : DsAPIError = enum(DsAPI.DsGetObjectName(nObjectID, szRetName, nNameMaxSize))
        match error with
        | DsAPIError.None -> szRetName.ToString()
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectName"))

    let DsAddObjectAttrUpdateCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectAttrUpdateCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectAttrUpdateCallBack"))

    let DsRemoveObjectAttrUpdateCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectAttrUpdateCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectAttrUpdateCallBack"))

    let DsAddObjectCommandCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectAttrUpdateCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectCommandCallBack"))

    //let DsRemoveObjectCommandCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsRemoveObjectCommandCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectCommandCallBack"))

    let DsAddClassAttrUpdateCallBack nClassID sAttrName callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddClassAttrUpdateCallBack(nClassID, sAttrName, callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddClassAttrUpdateCallBack"))

    let DsRemoveClassAttrUpdateCallBack nClassID sAttrName callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveClassAttrUpdateCallBack(nClassID, sAttrName, callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveClassAttrUpdateCallBack"))

    let DsGetObjectAttr nObjectID nAttrIndex (attribute:DsAttribute) =
        let error : DsAPIError = enum(DsAPI.DsGetObjectAttr(nObjectID, nAttrIndex, attribute))
        match error with
        | DsAPIError.None -> attribute
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectAttr"))

    let DsSetObjectAttr nObjectID nAttrIndex attrArg controllerArg =
        let error : DsAPIError = enum(DsAPI.DsSetObjectAttr(nObjectID, nAttrIndex, attrArg, controllerArg))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSetObjectAttr"))

    //let DsSetObjectAttrController nObjectID nAttrIndex attrArg controllerArg =
    //    let error : DsAPIError = enum(DsAPI.DsSetObjectAttrController(nObjectID, nAttrIndex, attrArg, controllerArg))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsSetObjectAttrController"))

    let DsStopObjectAttrController nObjectID nAttrIndex nElementIndex =
        let error : DsAPIError = enum(DsAPI.DsStopObjectAttrController(nObjectID, nAttrIndex, nElementIndex))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsStopObjectAttrController"))

    //let DsGetObjectArrayElem nObjectID nAttrIndex nElemIndex attrArg =
    //    let error : DsAPIError = enum(DsAPI.DsGetObjectArrayElem(nObjectID, nAttrIndex, nElemIndex, attrArg))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsGetObjectArrayElem"))

    let DsGetObjectArrayElemType nObjectID nAttrIndex =
        let mutable elemType = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetObjectArrayElemType(nObjectID, nAttrIndex, &elemType))
        match error with
        | DsAPIError.None -> elemType
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectArrayElemType"))

    let DsGetObjectArraySize nObjectID nAttrIndex =
        let mutable nElementCount = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectArraySize(nObjectID, nAttrIndex, &nElementCount))
        match error with
        | DsAPIError.None -> nElementCount
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectArraySize"))

    let DsSetObjectArrayElem nObjectID nAttrIndex nElemIndex attrArg =
        let error : DsAPIError = enum(DsAPI.DsSetObjectArrayElem(nObjectID, nAttrIndex, nElemIndex, attrArg))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSetObjectArrayElem"))

    let DsAllocObjectArray nObjectID nAttrIndex (nElementCount:int16) =
        let error : DsAPIError = enum(DsAPI.DsAllocObjectArray(nObjectID, nAttrIndex, nElementCount, IntPtr.Zero, IntPtr.Zero))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAllocObjectArray"))

    let DsSubscribeToMessages nMessageLevel bDebug sLocaleName callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsSubscribeToMessages(nMessageLevel, bDebug, sLocaleName, callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSubscribeToMessages"))

    let DsUnsubscribeToMessages () =
        let error : DsAPIError = enum(DsAPI.DsUnsubscribeToMessages())
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsUnsubscribeToMessages"))

    let DsAddObjectChild nObjectID nNewChildID =
        let error : DsAPIError = enum(DsAPI.DsAddObjectChild(nObjectID, nNewChildID))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectChild"))

    let DsAddObjectChildAfter nObjectID nNewChildID nAfterObjID =
        let error : DsAPIError = enum(DsAPI.DsAddObjectChildAfter(nObjectID, nNewChildID, nAfterObjID))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectChildAfter"))

    let DsAddObjectAddChildCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectAddChildCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectAddChildCallBack"))

    let DsRemoveObjectAddChildCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectAddChildCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectAddChildCallBack"))

    let DsAddPreClearTblsCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddPreClearTblsCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddPreClearTblsCallBack"))

    let DsRemovePreClearTblsCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemovePreClearTblsCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemovePreClearTblsCallBack"))

    let DsAddPostInitOrSyncCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddPostInitOrSyncCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddPostInitOrSyncCallBack"))

    let DsRemovePostInitOrSyncCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemovePostInitOrSyncCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemovePostInitOrSyncCallBack"))

    let DsAddPreInitOrSyncCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddPreInitOrSyncCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddPreInitOrSyncCallBack"))

    let DsRemovePreInitOrSyncCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemovePreInitOrSyncCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemovePreInitOrSyncCallBack"))

    let DsAddObjectRemoveChildCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsAddObjectRemoveChildCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAddObjectRemoveChildCallBack"))

    let DsRemoveObjectRemoveChildCallBack callbackFunc =
        let error : DsAPIError = enum(DsAPI.DsRemoveObjectRemoveChildCallBack(callbackFunc))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsRemoveObjectRemoveChildCallBack"))

    let DsGetObjectCount () =
        let mutable nRetCount = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectCount(&nRetCount))
        match error with
        | DsAPIError.None -> nRetCount
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectCount"))

    let DsGetObjectType nObjectID =
        let mutable objectType = enum(0)
        let error : DsAPIError = enum(DsAPI.DsGetObjectType(nObjectID, &objectType))
        match error with
        | DsAPIError.None -> objectType
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectType"))

    let DsGetObjectNumChildren nObjectID =
        let mutable nNumChildren = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectNumChildren(nObjectID, &nNumChildren))
        match error with
        | DsAPIError.None -> nNumChildren
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectNumChildren"))

    //let DsGetObjectChildIDs

    let DsGetObjectNumParents nObjectID =
        let mutable nNumParents = 0s
        let error : DsAPIError = enum(DsAPI.DsGetObjectNumParents(nObjectID, &nNumParents))
        match error with
        | DsAPIError.None -> nNumParents
        | _ -> raise (DsAPIErrorException(error, "DsGetObjectNumParents"))

    //let DsGetObjectParentIDs

    //let DsGetObjectWorldPosition

    //let DsGetObjectWorldPositionWithOffset

    let DsSendMessage nMesgLevel sMessage =
        let error : DsAPIError = enum(DsAPI.DsSendMessage(nMesgLevel, sMessage))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsSendMessage"))

    let DsGetControlInfoObject nObjectID nAttrIndex =
        let mutable nControlInfoObjectID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetControlInfoObject(nObjectID, nAttrIndex, &nControlInfoObjectID))
        match error with
        | DsAPIError.None -> nControlInfoObjectID
        | _ -> raise (DsAPIErrorException(error, "DsGetControlInfoObject"))

    //let DsPosCelestialToCartesian

    //let DsPosSphericalToCartesian

    //let DsPosPolarToCartesian

    //let DsPosCartesianToCelestial

    //let DsPosCartesianToSpherical

    (*
    let DsAttCartesianToCelestial dHeading dPitch dRoll dRA dDec dHeadingCelestial =
        let error : DsAPIError = enum(DsAPI.DsAttCartesianToCelestial(dHeading, dPitch, dRoll, dRA, dDec, dHeadingCelestial))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAttCartesianToCelestial"))

    let DsAttCelestialToCartesian dRA dDec dHeadingCelestial dHeading dPitch dRoll =
        let error : DsAPIError = enum(DsAPI.DsAttCelestialToCartesian(dRA, dDec, dHeadingCelestial, dHeading, dPitch, dRoll))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsAttCelestialToCartesian"))
    *)

    let DsDateToJulianDate nYear nMonth nDay nHour nMin nSec =
        let mutable dJulianDate = double 0
        let error : DsAPIError = enum(DsAPI.DsDateToJulianDate(nYear, nMonth, nDay, nHour, nMin, nSec, &dJulianDate))
        match error with
        | DsAPIError.None -> dJulianDate
        | _ -> raise (DsAPIErrorException(error, "DsDateToJulianDate"))

    (*
    let DsJulianDateToDate dJulianDate nYear nMonth nDay nHour nMin nSec =
        let error : DsAPIError = enum(DsAPI.DsJulianDateToDate(dJulianDate, nYear, nMonth, nDay, nHour, nMin, nSec))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsJulianDateToDate"))
    *)

    //let DsVectorParsecsToMeters

    let DsGetModelClassID sFilename =
        let mutable nClassID = 0s
        let error : DsAPIError = enum(DsAPI.DsGetModelClassID(sFilename, &nClassID))
        match error with
        | DsAPIError.None -> nClassID
        | _ -> raise (DsAPIErrorException(error, "DsGetModelClassID"))

    let DsGetErrorCodeString errorCode retErrString nMaxStringLength =
        let error : DsAPIError = enum(DsAPI.DsGetErrorCodeString(errorCode, retErrString, nMaxStringLength))
        match error with
        | DsAPIError.None -> ()
        | _ -> raise (DsAPIErrorException(error, "DsGetErrorCodeString"))

    let DsClassHasGroup nClassID nGroupIndex =
        let mutable bHasGroup = false
        let error : DsAPIError = enum(DsAPI.DsClassHasGroup(nClassID, nGroupIndex, &bHasGroup))
        match error with
        | DsAPIError.None -> bHasGroup
        | _ -> raise (DsAPIErrorException(error, "DsClassHasGroup"))

    let DsClassHasGroupByName nClassId pGroupName =
        let mutable bHasGroup = false
        let error : DsAPIError = enum(DsAPI.DsClassHasGroupByName(nClassId, pGroupName, &bHasGroup))
        match error with
        | DsAPIError.None -> bHasGroup
        | _ -> raise (DsAPIErrorException(error, "DsClassHasGroupByName"))

    //let DsGetClassAttrGroupInfo nClassID nGroupIndex pGroupName nAttrIndexS nAttrIndexE =
    //    let error : DsAPIError = enum(DsAPI.DsGetClassAttrGroupInfo(nClassID nGroupIndex pGroupName nAttrIndexS nAttrIndexE))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrGroupInfo"))

    let DsGetClassAttrGroupIndex nClassId nAttrIndex =
        let mutable nGroupIndex = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrGroupIndex(nClassId, nAttrIndex, &nGroupIndex))
        match error with
        | DsAPIError.None -> nGroupIndex
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrGroupIndex"))

    let DsGetClassAttrGroupIndexByName nClassID pGroupName =
        let mutable nGroupIndex = 0s
        let error : DsAPIError = enum(DsAPI.DsGetClassAttrGroupIndexByName(nClassID, pGroupName, &nGroupIndex))
        match error with
        | DsAPIError.None -> nGroupIndex
        | _ -> raise (DsAPIErrorException(error, "DsGetClassAttrGroupIndexByName"))

    //let DsAddObjectPostResetCallBack callbackFunc =
    //    let error : DsAPIError = enum(DsAPI.DsAddObjectPostResetCallBack(callbackFunc))
    //    match error with
    //    | DsAPIError.None -> ()
    //    | _ -> raise (DsAPIErrorException(error, "DsAddObjectPostResetCallBack"))

    let AngleAttribute v =
        let a = DsAngleAttribute()
        a.Value <- v
        a

    let Angle2Attribute v1 v2 =
        let a = DsAngle2Attribute()
        a.Angle1 <- v1
        a.Angle2 <- v2
        a

    let Angle3Attribute v1 v2 v3 =
        let a = DsAngle3Attribute()
        a.Angle1 <- v1
        a.Angle2 <- v2
        a.Angle3 <- v3
        a

    //let AttitudeAttribute v =
    //    let a = DsAttitudeAttribute()
    //    a

    let AudioPathNameAttribute v =
        let a = DsAudioPathNameAttribute()
        a.AudioPathName <- v
        a

    let BoolAttribute v =
        let a = DsBoolAttribute()
        a.Value <- v
        a

    let ColorAttribute color =
        let brush = new SolidColorBrush(color)
        let a = DsColorAttribute()
        a.ColorBrush <- brush
        a

    let ColorAttributeFromBrush brush =
        let a = DsColorAttribute()
        a.ColorBrush <- brush
        a

    //let DateTimeAttribute v =
    //    let a = DsDateTimeAttribute()
    //    a

    let DistanceAttribute v vunit =
        let a = DsDistanceAttribute()
        a.Distance <- v
        a.DistanceUnit <- vunit
        a

    let Distance2Attribute v1 v2 vunit =
        let a = DsDistance2Attribute()
        a.Distance1 <- v1
        a.Distance2 <- v2
        a.DistanceUnit <- vunit
        a

    let Distance3Attribute v1 v2 v3 vunit =
        let a = DsDistance3Attribute()
        a.Distance1 <- v1
        a.Distance2 <- v2
        a.Distance3 <- v3
        a

    let DoubleAttribute v =
        let a = DsDoubleAttribute()
        a.Value <- v
        a

    let DVector2Attribute x y =
        let a = DsDVector2Attribute()
        a.X <- x
        a.Y <- y
        a

    let DVector3Attribute x y z =
        let a = DsDVector3Attribute()
        a.X <- x
        a.Y <- y
        a.Z <- z
        a

    let SummedDeltaAttribute x =
        let a = DsSummedDeltaAttribute()
        a.Value <- x
        a

    let SummedDelta2Attribute x y =
        let a = DsSummedDelta2Attribute()
        a.X <- x
        a.Y <- y
        a

    let SummedDelta3Attribute x y z =
        let a = DsSummedDelta3Attribute()
        a.X <- x
        a.Y <- y
        a.Z <- z
        a

    let EnumAttribute v =
        let a = DsEnumAttribute()
        a.Value <- v
        a

    let FloatAttribute v =
        let a = DsFloatAttribute()
        a.Value <- v
        a

    let FontAttribute v =
        let a = DsFontAttribute()
        a.Value <- v
        a

    let FVector2Attribute x y =
        let a = DsFVector2Attribute()
        a.X <- x
        a.Y <- y
        a

    let FVector3Attribute x y z =
        let a = DsFVector3Attribute()
        a.X <- x
        a.Y <- y
        a.Z <- z
        a

    let HourAngleAttribute v =
        let a = DsHourAngleAttribute()
        a.Value <- v
        a

    let ImagePathNameAttribute v =
        let a = DsImagePathNameAttribute()
        a.ImagePathName <- v
        a

    let Int16Attribute v =
        let a = DsInt16Attribute()
        a.Value <- v
        a

    let Int32Attribute v =
        let a = DsInt32Attribute()
        a.Value <- v
        a

    let IVector2Attribute x y =
        let a = DsIVector2Attribute()
        a.X <- x
        a.Y <- y
        a

    let IVector3Attribute x y z =
        let a = DsIVector3Attribute()
        a.X <- x
        a.Y <- y
        a.Z <- z
        a

    let JsPathNameAttribute pathname parameters permanent =
        let a = DsJsPathNameAttribute()
        a.PathName <- pathname
        a.Parameters <- parameters
        a.IsPermanent <- permanent
        a

    //let ModelNameAttribute v =
    //    let a = DsModelNameAttribute()
    //    a

    let ModelPathNameAttribute v =
        let a = DsModelPathNameAttribute()
        a.ModelPathName <- v
        a

    let MorphAttribute v =
        let a = DsMorphAttribute()
        a.Value <- v
        a

    let NameAttribute v =
        let a = DsNameAttribute()
        a.NameString <- v
        a

    //let SystemObjectAttribute v =
    //    let a = DsSystemObjectAttribute()
    //    a

    let DirectoryAttribute v =
        let a = DsDirectoryAttribute()
        a.Directory <- v
        a

    let PathNameAttribute v =
        let a = DsPathNameAttribute()
        a.PathName <- v
        a

    let PercentAttribute v =
        let a = DsPercentAttribute()
        a.Percent <- v
        a

    let CartesianPositionAttribute x y z (unit:DsDistanceUnit) =
        let a = DsPositionAttribute()
        a.SetCartesianPosition(x, y, z, unit)
        a

    let CelestialPositionAttribute ra dec dist (unit:DsDistanceUnit) =
        let a = DsPositionAttribute()
        a.SetCelestialPosition(ra, dec, dist, unit)
        a

    let SphericalPositionAttribute az el dist (unit:DsDistanceUnit) =
        let a = DsPositionAttribute()
        a.SetSphericalPosition(az, el, dist, unit)
        a

    let ScaleAttribute x y z (unit:DsDistanceUnit) =
        let a = DsScaleAttribute()
        a.X <- x
        a.Y <- y
        a.Z <- z
        a.DistanceUnit <- unit
        a

    let ScriptPathNameAttribute v =
        let a = DsScriptPathNameAttribute()
        a.ScriptPathName <- v
        a

    //let ShowClockModeAttribute v =
    //    let a = DsShowClockModeAttribute()
    //    a

    let StringAttribute v =
        let a = DsStringAttribute()
        // a.Value translates '|' into '\r\n'
        a.String <- v
        a

    let CommandStringAttribute v =
        let a = DsCommandStringAttribute()
        // a.Value translates '|' into '\r\n'
        a.String <- v
        a

    let TerrainPathNameAttribute v =
        let a = DsTerrainPathNameAttribute()
        a.PathName <- v
        a

    //let TextureNameAttribute v =
    //    let a = DsTextureNameAttribute()
    //    a

    let TimecodeAttribute v =
        let a = DsTimecodeAttribute()
        a.Timecode <- v
        a

    let Uint32Attribute v =
        let a = DsUint32Attribute()
        a.Value <- v
        a

    let Uint8Attribute v =
        let a = DsUint8Attribute()
        a.Value <- v
        a

    let VideoPathNameAttribute v =
        let a = DsVideoPathNameAttribute()
        a.VideoPathName <- v
        a

    //let ArrayAttribute v =
    //    let a = DsArrayAttribute()
    //    a

    let SlidesetPathNameAttribute v =
        let a = DsSlidesetPathNameAttribute()
        a.SlidesetPathName <- v
        a

    //let RangeMapAttribute v =
    //    let a = DsRangeMapAttribute()
    //    a

    let HandleAttribute v =
        let a = DsHandleAttribute()
        a

    let DsCreateAttributeFromType (eAttrType:DsAttr) : DsAttribute =
        match eAttrType with
        | DsAttr.Angle -> DsAngleAttribute() :> DsAttribute
        | DsAttr.Angle2 -> DsAngle2Attribute() :> DsAttribute
        | DsAttr.Angle3 -> DsAngle3Attribute() :> DsAttribute
        | DsAttr.Attitude -> DsAttitudeAttribute() :> DsAttribute
        | DsAttr.AudioPathname -> DsAudioPathNameAttribute() :> DsAttribute
        | DsAttr.Bool -> DsBoolAttribute() :> DsAttribute
        | DsAttr.Color -> DsColorAttribute() :> DsAttribute
        | DsAttr.Datetime -> DsDateTimeAttribute() :> DsAttribute
        | DsAttr.Distance -> DsDistanceAttribute() :> DsAttribute
        | DsAttr.Distance2 -> DsDistance2Attribute() :> DsAttribute
        | DsAttr.Distance3 -> DsDistance3Attribute() :> DsAttribute
        | DsAttr.Double -> DsDoubleAttribute() :> DsAttribute
        | DsAttr.Dvector2 -> DsDVector2Attribute() :> DsAttribute
        | DsAttr.Dvector3 -> DsDVector3Attribute() :> DsAttribute
        | DsAttr.SummedDelta -> DsSummedDeltaAttribute() :> DsAttribute
        | DsAttr.SummedDelta2 -> DsSummedDelta2Attribute() :> DsAttribute
        | DsAttr.SummedDelta3 -> DsSummedDelta3Attribute() :> DsAttribute
        | DsAttr.Enum -> DsEnumAttribute() :> DsAttribute
        | DsAttr.Float -> DsFloatAttribute() :> DsAttribute
        | DsAttr.Font -> DsFontAttribute() :> DsAttribute
        | DsAttr.Fvector2 -> DsFVector2Attribute() :> DsAttribute
        | DsAttr.Fvector3 -> DsFVector3Attribute() :> DsAttribute
        | DsAttr.HourAngle -> DsHourAngleAttribute() :> DsAttribute
        | DsAttr.ImagePathname -> DsImagePathNameAttribute() :> DsAttribute
        | DsAttr.Int16 -> DsInt16Attribute() :> DsAttribute
        | DsAttr.Int32 -> DsInt32Attribute() :> DsAttribute
        | DsAttr.Ivector2 -> DsIVector2Attribute() :> DsAttribute
        | DsAttr.Ivector3 -> DsIVector3Attribute() :> DsAttribute
        | DsAttr.JsPathname -> DsJsPathNameAttribute() :> DsAttribute
        | DsAttr.ModelName -> DsModelNameAttribute() :> DsAttribute
        | DsAttr.ModelPathname -> DsModelPathNameAttribute() :> DsAttribute
        | DsAttr.Morph -> DsMorphAttribute() :> DsAttribute
        | DsAttr.Name -> DsNameAttribute() :> DsAttribute
        | DsAttr.SystemObject -> DsSystemObjectAttribute() :> DsAttribute
        | DsAttr.Directory -> DsDirectoryAttribute() :> DsAttribute
        | DsAttr.Pathname -> DsPathNameAttribute() :> DsAttribute
        | DsAttr.Percent -> DsPercentAttribute() :> DsAttribute
        | DsAttr.Position -> DsPositionAttribute() :> DsAttribute
        | DsAttr.Scale -> DsScaleAttribute() :> DsAttribute
        | DsAttr.ScriptPathname -> DsScriptPathNameAttribute() :> DsAttribute
        | DsAttr.Showclockmode -> DsShowClockModeAttribute() :> DsAttribute
        | DsAttr.String -> DsStringAttribute() :> DsAttribute
        | DsAttr.CommandString -> DsCommandStringAttribute() :> DsAttribute
        | DsAttr.TerrainPathname -> DsTerrainPathNameAttribute() :> DsAttribute
        | DsAttr.TextureName -> DsTextureNameAttribute() :> DsAttribute
        | DsAttr.Timecode -> DsTimecodeAttribute() :> DsAttribute
        | DsAttr.Uint32 -> DsUint32Attribute() :> DsAttribute
        | DsAttr.Uint8 -> DsUint8Attribute() :> DsAttribute
        | DsAttr.VideoPathname -> DsVideoPathNameAttribute() :> DsAttribute
        | DsAttr.Array -> DsArrayAttribute() :> DsAttribute
        | DsAttr.SlidesetPathname -> DsSlidesetPathNameAttribute() :> DsAttribute
        | DsAttr.RangeMap -> DsRangeMapAttribute() :> DsAttribute
        | DsAttr.Handle -> DsHandleAttribute() :> DsAttribute
        | _ -> raise (Exception(
                       "DsCreateAttributeFromType - Could not create unknown attribute type"))
