
# RBCPSuite

A remote plugin for Digistar implementing a variety of utilities and
effects.  RBCPSuite is a WPF application written in F#.

## Usage

When started, RBCPSuite minimizes to the system tray.  Clicking its system
tray icon will show or hide the main window.  It also globally binds the
Scroll Lock key on the keyboard to show and hide its main window.  To exit
the program, right click the system tray icon and select Exit from the
context menu.

### Key Bindings

 - **F9**: ChromaCove blooms
 - **F10**: ChromaCove white
 - **F11**: ChromaCove blackout
 - **F12**: Digistar system fadestopreset
