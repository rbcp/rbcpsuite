﻿namespace RBCPSuite

module DigistarConnection =

    open System
    open System.Runtime.InteropServices
    open System.Windows.Threading
    open ES.Ds.Common
    open DsFsharp

    let rbcpClass_attributes = [
        ("command", DsF.DsClassAttrType.Scalar DsAttr.String, DsAttrFlag.FlagNone, []);
    ]

    let rbcpClass_commands = []

    let mutable command_handlers = []

    let addCommandHandler f =
        command_handlers <- f :: command_handlers

    let onCommand (dsobName, attrName, attrIdx) =
        try
            let dsob = DsF.GetObject dsobName
            let commandStr = (dsob.GetAttr("command") :?> DsStringAttribute).String
            if commandStr <> "" then
                let parsed = LSON.parse ("(" + commandStr + ")")
                command_handlers
                |> List.tryFind (fun f -> f parsed)
                |> ignore
                gui.mainWindow.log(dsobName+" command "+commandStr)
        with
        | :? DsAPIErrorException as e ->
            gui.mainWindow.log(e)

    let onAPIConnected c =
        try
            if c then
                gui.mainWindow.log("Connected to Digistar")
            else
                gui.mainWindow.log("Not connected to Digistar")
        with
        | :? DsAPIErrorException as e ->
            gui.mainWindow.log(e)


    let onAPIReady () =
        try
            gui.mainWindow.log("API Ready")
            let c = DsF.GetClass("rbcpClass")
            c.Ensure DsClassType.Permanent
                     rbcpClass_attributes
                     rbcpClass_commands |> ignore
            Plugin.DigistarInitHook()
        with
        | :? DsAPIErrorException as e ->
            gui.mainWindow.log(e)
        | e ->
            gui.mainWindow.log(e)


    let ConnectToDsAPI pluginName =
        try
            DsF.Connect pluginName onAPIConnected onAPIReady
        with
        | :? DsAPIErrorException as e ->
              gui.mainWindow.log(e)

    let setup () =
        let c = DsF.GetClass("rbcpClass")
        c.GetAttribute("command").OnUpdate.Add(onCommand)
        c.OnLock.Add(fun dsclass ->
            try
                let r = DsF.GetObject("rbcp")
                r.Create dsclass DsObjectType.Undefined |> ignore
            with
            | :? DsAPIErrorException as e ->
                gui.mainWindow.log(e))
