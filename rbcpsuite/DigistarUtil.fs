﻿namespace RBCPSuite

module DigistarUtil =

    open FParsec

    open ES.Ds.Common
    open DsFsharp
    open System.Linq

    // a util that will parse a string to either a script to run,
    // or a js script to run.
    // digistar scripts end in .ds
    // js scripts end in .js, but may also have "|{expr}" after
    // the filename.

    // we want to be able to put variable expansions into the call,
    // and perhaps we can use {var} format for this.

    // for example, for Clickers onStart, we will want to be able to
    // splice the clickersVoteClass object into the js call.

    type DsCallable =
        | DsScript of string
        | JsScript of string * string

    let private dsScript = regex ".*?\\.ds" |>> DsScript

    let private jsScript = regex ".*?\\.js" |>> (fun x -> JsScript (x,""))

    let parseDsCallable s =
        match run (dsScript <|> jsScript .>> eof) s with
        | Success(result, _, _) -> result
        | Failure(errorMsg, e, s) -> failwith errorMsg

    let dsCallablePlayDs path =
        let c = DsF.GetObject "script"
        let v = ScriptPathNameAttribute path
        c.SetAttr "play" v

    let dsCallablePlayJs path expr =
        let c = DsF.GetObject "js"
        let v = JsPathNameAttribute path expr false
        c.SetAttr "play" v

    let dsCallablePlay c =
        match c with
        | DsScript path -> dsCallablePlayDs path
        | JsScript (path, expr) -> dsCallablePlayJs path expr



    //
    // Scriptlet Minilang
    //

    type UserState = unit // doesn't have to be unit, of course
    type Parser<'t> = Parser<'t, UserState>

    let hspaces : Parser<_> = many1 (anyOf " \t")

    let attributeValue = spaces

    let attributeName : Parser<_>  =
        let allowedPunctuation = "_-!@%^&*()=+`~'<>,|[]{}"
        let allowedPunctuationContinue = "#;"
        let isAsciiIdStart    = fun c -> isLetter c || isAnyOf allowedPunctuation c
        let isAsciiIdContinue = fun c -> isLetter c || isDigit c
                                          || isAnyOf allowedPunctuation c
                                          || isAnyOf allowedPunctuationContinue c
        identifier (IdentifierOptions(
                        isAsciiIdStart = isAsciiIdStart,
                        isAsciiIdContinue = isAsciiIdContinue))
    
    let commandName : Parser<_>  =
        let allowedPunctuation = "_-!@%^&*()=+`~'<>,|[]{}"
        let allowedPunctuationContinue = "#;"
        let isAsciiIdStart    = fun c -> isLetter c || isAnyOf allowedPunctuation c
        let isAsciiIdContinue = fun c -> isLetter c || isDigit c
                                          || isAnyOf allowedPunctuation c
                                          || isAnyOf allowedPunctuationContinue c
        identifier (IdentifierOptions(
                        isAsciiIdStart = isAsciiIdStart,
                        isAsciiIdContinue = isAsciiIdContinue))
    
    let objectName : Parser<_>  =
        let allowedPunctuation = "_-!@%^&*()=+`~'<>,|[]{}"
        let allowedPunctuationContinue = "#;"
        let isAsciiIdStart    = fun c -> isLetter c || isAnyOf allowedPunctuation c
        let isAsciiIdContinue = fun c -> isLetter c || isDigit c
                                          || isAnyOf allowedPunctuation c
                                          || isAnyOf allowedPunctuationContinue c
        identifier (IdentifierOptions(
                        isAsciiIdStart = isAsciiIdStart,
                        isAsciiIdContinue = isAsciiIdContinue))

    let command = hspaces >>. objectName .>> hspaces >>.
                  (attributeName <|> commandName) .>>
                  hspaces .>> eof

