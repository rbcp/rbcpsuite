﻿namespace RBCPSuite

module ChromaCove =

    open System.Net.Sockets
    open System.Text
    open System.Windows.Input

    let chromacove_cove_connection = new UdpClient(8888)
    let chromacove_upper_connection = new UdpClient(8889)

    chromacove_cove_connection.Connect("192.168.2.245", 8888)
    chromacove_upper_connection.Connect("192.168.2.245", 8889)

    let send_to_cove (command:string) =
        let msg = Encoding.ASCII.GetBytes(command)
        chromacove_cove_connection.Send(msg, msg.Length) |> ignore

    let send_to_upper (command:string) =
        let msg = Encoding.ASCII.GetBytes(command)
        chromacove_upper_connection.Send(msg, msg.Length) |> ignore



    let general_cove_setting cove_command = async {
        send_to_cove("RUN_CUE 9000001")
        do! Async.Sleep(100)
        send_to_cove(cove_command)
        send_to_upper("SET_RGB 0,0,0,3")
        }

    let onMainWindowLoaded (w:gui.UIWindow) =
        w.InputBindings.Add(
            InputBinding(gui.CommandCallThunk(fun _ -> Async.RunSynchronously (general_cove_setting "RUN_CUE 300000,6")),
                         KeyGesture(Key.F9))) |> ignore
        w.InputBindings.Add(
            InputBinding(gui.CommandCallThunk(fun _ -> Async.RunSynchronously (general_cove_setting "SET_RGB 0.5,0.5,0.5,6")),
                         KeyGesture(Key.F10))) |> ignore
        w.InputBindings.Add(
            InputBinding(gui.CommandCallThunk(fun _ -> Async.RunSynchronously (general_cove_setting "SET_RGB 0,0,0,6")),
                         KeyGesture(Key.F11))) |> ignore
        // Tray Icon Context Menu items
        gui.trayIcon.InsertSeparator()
        gui.trayIcon.Insert("Cove: Blooms", fun _ -> Async.RunSynchronously (general_cove_setting "RUN_CUE 300000,6"))
        gui.trayIcon.Insert("Cove: White 100", fun _ -> Async.RunSynchronously (general_cove_setting "SET_RGB 1.0,1.0,1.0,6"))
        gui.trayIcon.Insert("Cove: White 50", fun _ -> Async.RunSynchronously (general_cove_setting "SET_RGB 0.5,0.5,0.5,6"))
        gui.trayIcon.Insert("Cove: Off", fun _ -> Async.RunSynchronously (general_cove_setting "SET_RGB 0,0,0,6"))
        gui.trayIcon.InsertSeparator()
        gui.trayIcon.Insert("Upper: White 100", fun _ -> send_to_upper("SET_RGB 1.0,1.0,1.0,6") |> ignore)
        gui.trayIcon.Insert("Upper: Off", fun _ -> send_to_upper("SET_RGB 0,0,0,6") |> ignore)


    let register config =
        Plugin.Register
            { Plugin.defaultPlugin with
                MainWindowLoaded = onMainWindowLoaded }
