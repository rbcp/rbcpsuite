﻿namespace RBCPSuite

module Clickers =

    open System
    open System.Collections.ObjectModel
    open System.IO
    open System.Net
    open System.Runtime.InteropServices
    open System.Text
    open System.Text.RegularExpressions
    open System.Threading
    open System.Windows
    open System.Windows.Data
    open System.Windows.Interop
    open Chiron
    open ES.Ds.Common
    open DsFsharp
    open Utils

    let mutable receiverPort = "" // port spec like "hid://00000000"
    let mutable resultsPostURL = "" // post url for results

    type Showing = {
        id: string
        show: string
        history: string list
    }
    let mutable current_showing = { Showing.id = "";
                                    Showing.show = "";
                                    Showing.history = [] }

    // The only information we get from Digistar is the vote objects themselves.
    // If we want to group them into shows and showings, we need a rule that
    // says how to do that.  So when is it time to generate a new showingID?
    //
    // - When this show is not the same as the previous show.
    // - When it's a repeat question.
    //
    // If a show were to have to be restarted in the middle, and if a question
    // got legitimately repeated, we will have to do repair work to our statistics,
    // as that is the one case where a single showing would be split into two.
    //
    let updateCurrentShowing voteDsob show question =
        let q = voteDsob + "/" + question // reasonably uniquely refer to a question
        if show = current_showing.show &&
           not (List.exists ((=) q) current_showing.history) then
           // add to current showing
           current_showing <- {
               current_showing with
                 Showing.history = q :: current_showing.history
               }
        else
           // new showing
           let showing_id = show + "-" + System.DateTime.UtcNow.ToString("o")
           current_showing <- {
               Showing.id = showing_id;
               Showing.show = show;
               Showing.history = [q]
               }

    let clickersVoteClass_attributes = [
        ("question", DsF.DsClassAttrType.Scalar DsAttr.String, DsAttrFlag.FlagNone, []);
        ("answers",  DsF.DsClassAttrType.Array DsAttr.String, DsAttrFlag.FlagNone, []);
        ("correctAnswer", DsF.DsClassAttrType.Scalar DsAttr.Int16, DsAttrFlag.FlagNone,
         [DsF.AttributeDefault (Int16Attribute -1s :> DsAttribute)]);
        ("responses", DsF.DsClassAttrType.Scalar DsAttr.String, DsAttrFlag.FlagNone, []);
        ("show", DsF.DsClassAttrType.Scalar DsAttr.String, DsAttrFlag.FlagNone, []);
        ("startTime", DsF.DsClassAttrType.Scalar DsAttr.String,
         DsAttrFlag.FlagPlugin ||| DsAttrFlag.FlagAdvanced, []); //XXX: read only
        // ideally, status would be read only, but it can be useful to be able to set
        // it from Digistar for testing purposes when a receiver is not connected to
        // the computer.
        ("status", DsF.DsClassAttrType.Scalar DsAttr.Bool, DsAttrFlag.FlagNone, [])
    ]

    let clickersVoteClass_commands = [
        "delete";
        "start";
        "stop"
    ]

    type ClickersVoteError (msg:string) =
        inherit Exception("ClickersVoteError: "+msg)

    type ClickersPostResultsError (msg:string) =
        inherit Exception("ClickersPostResultsError: "+msg)

    let ClickersPostResultsErrorFromHtml html =
        let m = Regex.Match(html, "<body.*?>.*")
        let msg = match m.Groups.Count with
                    | 1 -> Regex.Replace(m.Groups.[0].Value, "<.*?>", "")
                    | _ -> html
        ClickersPostResultsError msg

    let clickersLogStatus msg =
        DsFsharp.DsSendMessage DsMesgLevel.Status msg
        gui.mainWindow.log msg

    let findAvailableReceivers () =
        let sn = rf21x.getHidSerialNumber()
        sn |> Seq.map(fun s -> "hid://"+s) |> Seq.toList

    type ClickersVoteLog (dsob) =
        let collection = new ObservableCollection<Tuple<int,int,string,string>>()
        let _lock = new obj()
        do BindingOperations.EnableCollectionSynchronization(collection, _lock)

        member this.dsob = dsob
        member this.responses = collection
        member this.Add(keypadId:int, response:int) =
            //XXX should we log if an invalid response comes in?  It does happen occasionally...
            if response > 0 && response <= 7 then
                let label = [|" ";"A";"B";"C";"D";"E";"F";"G"|].[response]
                let color = [|"none";"#007300";"#FF0000";"#FFFFFF";
                              "#FFFF00";"#00BFFF";"#FF8C00";"#000000"|].[response]
                let t = new Tuple<int,int,string,string>(keypadId, response, label, color)
                if collection.Count < keypadId then
                    for i = collection.Count + 1 to keypadId do
                        collection.Add(new Tuple<int,int,string,string>(i, 0, " ", "none"))
                collection.Item(keypadId - 1) <- t

        static member clickersVoteLogTemplate =
            gui.xaml_to_datatemplate ("<ItemsControl ItemsSource=\"{Binding responses}\">"+
                                      "  <ItemsControl.ItemTemplate>"+
                                      "    <DataTemplate>"+
                                      "      <TextBlock Text=\"{Binding Item3}\" "+
                                      "                 Foreground=\"Black\" Background=\"{Binding Item4}\" "+
                                      "                 Height=\"16\" Width=\"16\" TextAlignment=\"Center\" "+
                                      "                 FontSize=\"9\" FontWeight=\"Bold\" Padding=\"0,2,0,0\">"+
                                      "        <TextBlock.ToolTip>"+
                                      "          <ToolTip Content=\"{Binding Item1}\" ContentStringFormat=\"Clicker #{0}\"/>"+
                                      "        </TextBlock.ToolTip>"+
                                      "      </TextBlock>"+
                                      "    </DataTemplate>"+
                                      "  </ItemsControl.ItemTemplate>"+
                                      "  <ItemsControl.ItemsPanel>"+
                                      "    <ItemsPanelTemplate><WrapPanel/></ItemsPanelTemplate>"+
                                      "  </ItemsControl.ItemsPanel>"+
                                      "</ItemsControl>")

    let responsesDecode (encoded:string) =
        encoded
        |> Seq.collect (fun group ->
                          let g = uint16 group
                          [| (g &&& (7us));
                             (g &&& (7us <<< 3)) >>> 3;
                             (g &&& (7us <<< 6)) >>> 6;
                             (g &&& (7us <<< 9)) >>> 9;
                             (g &&& (7us <<< 12)) >>> 12 |])

    let responsesToJson (responses:seq<uint16>) =
        responses
        |> Seq.choosei (fun i x ->
                          if x > 0us then
                              Some ((i + 1).ToString(), Number <| decimal (x - 1us))
                          else
                              None)
        |> Map.ofSeq
        |> Object

    let resultsToJson dsobName showing =
        let dsob = DsF.GetObject(dsobName)
        let question = dsob.GetAttr("question") :?> DsStringAttribute
        let answers = dsob.GetAttr("answers") :?> DsArrayAttribute
        let answersList = answers |> Seq.map (fun x -> String <| (x :?> DsStringAttribute).Value)
        let correctAnswer = dsob.GetAttr("correctAnswer") :?> DsInt16Attribute
        let responses = dsob.GetAttr("responses") :?> DsStringAttribute
        let startTime = dsob.GetAttr("startTime") :?> DsStringAttribute
        Object <| Map.ofList
          [ "tag", String dsobName
            "showingID", String showing.id
            "system", String System.Environment.MachineName
            "startTime", String (startTime.String)
            "stopTime", String (System.DateTime.UtcNow.ToString("o"))
            "question", String (question.String)
            "answers", Array <| List.ofSeq answersList
            "correctAnswer", Number (decimal correctAnswer.Value)
            "responses", responsesToJson (responsesDecode responses.String)
            ]

    let postResults resultsJson showing (url:string) = async {
        try
            let req = HttpWebRequest.Create(url) :?> HttpWebRequest
            req.ProtocolVersion <- HttpVersion.Version10
            req.Method <- "POST"
            let postjson = Json.format resultsJson
            let postBytes = Encoding.ASCII.GetBytes(postjson)
            req.ContentType <- "application/json"
            req.ContentLength <- int64 postBytes.Length
            let reqStream = req.GetRequestStream()
            reqStream.Write(postBytes, 0, postBytes.Length)
            reqStream.Close()
            let resp = req.GetResponse() :?> HttpWebResponse
            let stream = resp.GetResponseStream()
            let reader = new StreamReader(stream)
            let responsejson = reader.ReadToEnd()
            gui.mainWindow.log(resp.ContentType)
            if resp.StatusCode = HttpStatusCode.OK then
                if resp.ContentType.StartsWith("text/html") then
                    raise (ClickersPostResultsErrorFromHtml responsejson)
                else
                    clickersLogStatus ("Posted vote results. Response from server: "+responsejson)
            else
                raise (ClickersPostResultsError (resp.StatusCode.ToString()+": "+responsejson))
        with
        | :? WebException as e ->
            gui.mainWindow.log(e)
        | :? ClickersPostResultsError as e ->
            gui.mainWindow.log(e)
        }

    let listenForVotes (dsob:DsF.DsObject) = async {
        try
            let availableReceivers = findAvailableReceivers()
            if not (availableReceivers |> List.exists(fun x -> x = receiverPort)) then
                raise (ClickersVoteError("receiver not found: "+receiverPort+" (available: "+(String.concat " " availableReceivers)+")"))
            let receiver = new RF21xDevice()
            let opened = receiver.``open``(rf21x.RF21X_DT_RF219, receiverPort, 1, 200);
            if opened then
                clickersLogStatus "Clickers: receiver opened"
            else
                raise (ClickersVoteError "failed to open receiver")
            let nAnswers = dsob.GetArraySize "answers"
            if not (receiver.startQuiz(rf21x.RF21X_QT_Single, int nAnswers)) then
                raise (ClickersVoteError "failed to start quiz")
            try
                let voteLog = ClickersVoteLog(dsob.name)
                gui.mainWindow.log(voteLog)
                dsob.SetAttr "status" (BoolAttribute true)
                // Responses are encoded into a character array.  Each clicker is represented
                // by 3 bits for a total of 8 unique values (7 buttons and nul).  One character
                // represents 5 responses in its first 15 bits, and the last bit is set true
                // so we always have a non-nul character.  400 total clickers are thus represented
                // in 80 characters.  Clicker indexes are decremented because they start at 1.
                //
                let response_initial = char (1us <<< 15)
                let responses : char[] = Array.init 80 (fun _ -> response_initial) // 80 chars * 5 responses per char = 400 responses
                let mutable dirty = true
                let message = new RF21xMessage()
                while true do
                    if receiver.getMessage(message) then
                        if message.messageType = rf21x.RF21X_MT_Teacher then
                            gui.mainWindow.log("Teacher")
                        else
                            dirty <- true
                            let response_raw = Encoding.ASCII.GetBytes(message.data).[0]
                            let response_for_str = uint16 (response_raw - 64uy)
                            let response_char_idx = (message.keypadId - 1) / 5
                            let response_bitpos = ((message.keypadId - 1) % 5) * 3
                            let response_encoded = response_for_str <<< response_bitpos
                            let prev = uint16 responses.[response_char_idx]
                            let prev_del_old = prev &&& ~~~(7us <<< response_bitpos) // zero out previous value
                            responses.[response_char_idx] <- char (prev_del_old ||| response_encoded)
                            System.Windows.Application.Current.Dispatcher.Invoke(
                                System.Windows.Threading.DispatcherPriority.Normal,
                                Action(fun () -> voteLog.Add(message.keypadId, int response_for_str))) |> ignore
                            //gui.mainWindow.log("Student "+message.keypadId.ToString()+" " + message.data)
                    else
                        if dirty then
                            dirty <- false
                            dsob.SetAttr "responses" (StringAttribute(new String(responses)))
                        do! Async.Sleep(100)
            finally
                receiver.stopQuiz() |> ignore
                receiver.close() |> ignore
                clickersLogStatus "Clickers: receiver closed"
        with
        | e -> gui.mainWindow.log(e)
        }

    let onClickerVoteCreate (sender, id) =
        let o = DsF.GetObjectByID id
        gui.mainWindow.log("clickersVoteClass object created: "+o.name)

    let mutable cancellationSource = new CancellationTokenSource()

    let onClickerVoteStart ((dsclass:DsF.DsClass), obName, command) =
        cancellationSource.Cancel() // end any previously running vote
        if (DsGetObjectID obName).IsSome then
            let dsob = DsF.GetObject obName
            dsob.SetAttr "startTime" (StringAttribute (System.DateTime.UtcNow.ToString("o")))
            // start listening
            cancellationSource <- new CancellationTokenSource()
            Async.Start(listenForVotes dsob, cancellationSource.Token)
            gui.mainWindow.log("clickersVoteClass "+dsob.name+" start")
        else
            gui.mainWindow.log("clickersVoteClass start failed because object doesn't exist")


    let onClickerVoteStop ((dsclass:DsF.DsClass), dsobName, attr) =
        cancellationSource.Cancel()
        let dsob = (DsF.GetObject dsobName) : DsF.DsObject
        let status = dsob.GetAttr "status" :?> DsBoolAttribute
        if status.Value then
            dsob.SetAttr "status" (BoolAttribute false)
            let show = (dsob.GetAttr "show" :?> DsStringAttribute).Value
            let question = (dsob.GetAttr "question" :?> DsStringAttribute).Value
            updateCurrentShowing dsobName show question
            if show <> "" && resultsPostURL <> "" then
                let resultsJson = resultsToJson dsobName current_showing
                Async.Start(postResults resultsJson current_showing resultsPostURL)
            gui.mainWindow.log("clickersVoteClass "+dsob.name+" stop")

    let onClickerVoteDelete ((dsclass:DsF.DsClass), dsobName, attr) =
        cancellationSource.Cancel()

    let onDigistarInit () =
        try
            let c = DsF.GetClass("clickersVoteClass")
            c.Ensure DsClassType.Temporary
                     clickersVoteClass_attributes
                     clickersVoteClass_commands |> ignore
        with
        | :? DsAPIErrorException as e ->
            gui.mainWindow.log(e)
        | e ->
            gui.mainWindow.log(e)

    let onMainWindowLoaded _ =
        gui.AddDataView typeof<ClickersVoteLog> ClickersVoteLog.clickersVoteLogTemplate
        if resultsPostURL <> "" then
            gui.mainWindow.log("Clickers: Results will be posted to: "+resultsPostURL)
        else
            gui.mainWindow.log("Clickers: Results will not be recorded. (See resultsPostURL in the configuration.)")

    let register (config:Map<string,string>) =
        if config.ContainsKey("receiverPort") then
            receiverPort <- config.Item("receiverPort")
        else
            raise (exn "Clickers receiverPort not found in config")
        if config.ContainsKey("resultsPostURL") then
            resultsPostURL <- config.Item("resultsPostURL")
        let c = DsF.GetClass("clickersVoteClass")
        c.OnCreate.Add(onClickerVoteCreate)
        c.GetCommand("start").OnCommand.Add(onClickerVoteStart)
        c.GetCommand("stop").OnCommand.Add(onClickerVoteStop)
        c.GetCommand("delete").OnCommand.Add(onClickerVoteDelete)
        Plugin.Register
            { Plugin.defaultPlugin with
                DigistarInit = onDigistarInit
                MainWindowLoaded = onMainWindowLoaded }
