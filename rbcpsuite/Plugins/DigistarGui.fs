﻿namespace RBCPSuite

module DigistarGui =

    open System.Windows.Input
    open DsFsharp


    let onMainWindowLoaded (w:gui.UIWindow) =
        w.InputBindings.Add(
            InputBinding(gui.CommandCallThunk(fun _ ->
                                               try
                                                   DsSendStringCommand "system fadestopreset"
                                               with
                                               | :? DsAPIErrorException as e ->
                                                   gui.mainWindow.log(e)),
                         KeyGesture(Key.F12))) |> ignore


    let register config =
        Plugin.Register
            { Plugin.defaultPlugin with
                MainWindowLoaded = onMainWindowLoaded }
