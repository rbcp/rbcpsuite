﻿
namespace RBCPSuite

module Main =

    open System
    open System.Collections
    open System.Collections.Specialized
    open System.Configuration
    open System.IO
    open System.Security.Principal
    open System.Text
    open System.Threading
    open System.Windows
    open Microsoft.FSharp.Control
    open ES.Ds.Common
    open DsFsharp
    open Utils

    //type DsAPIErrorExceptionView (e:DsAPIErrorException) =
    //    inherit Windows.Controls.TextBlock ()
    //    do base.Text <- e.Message.ToString()
    //       for entry in e.Data |> Seq.cast<DictionaryEntry> do
    //           base.Text <- base.Text + "\n" + entry.Key.ToString() + " = " + entry.Value.ToString()
    //       base.Text <- base.Text + "\n" + e.ToString()
    //       base.Foreground <- new Windows.Media.SolidColorBrush(Windows.Media.Color.FromRgb(byte 255,byte 0,byte 0))

    //XXX: how do we get e.Data into the message in a reasonable way?

    let DsAPIErrorExceptionView = gui.xaml_to_datatemplate "<TextBlock Text='{Binding Message}' />"

    // Startup
    //
    let mainWindowStartup (mainWindow:Window) =
        GlobalHotkeys.registerTheHotkey mainWindow |> ignore
        Plugin.MainWindowLoadedHook(mainWindow :?> gui.UIWindow)
        DigistarConnection.setup()
        DigistarConnection.ConnectToDsAPI "RBCPSuite"


    let application_start () =
        // Read config
        //
        [("Almanac", Almanac.register);
         ("ChromaCove", ChromaCove.register);
         ("Clickers", Clickers.register);
         ("DigistarGui", DigistarGui.register)] |>
          Seq.iter(fun x ->
                     let name, reg = x
                     let sec = System.Configuration.ConfigurationManager.GetSection(name) :?> NameValueCollection
                     if sec <> null then
                         let config = mapOfNameValueCollection sec
                         //XXX: instead of plugins registering themselves in an imperative way,
                         //     we should just have a list of known plugins, and each one should
                         //     have 'onEnable' and 'onDisable' or the like.
                         //
                         //     Registration is basically just reading configuration and setting
                         //     mutables with it.
                         if config.ContainsKey("enable") && config.Item("enable") = "true" then
                             reg config)

        // GUI
        //
        gui.Init()
        gui.AddDataView typeof<DsFsharp.DsAPIErrorException> DsAPIErrorExceptionView
        gui.AddDataView typeof<StringBuilder> (gui.xaml_to_datatemplate "<TextBlock Text='{Binding}' />")

        // Application
        //
        let application = new Application()

        gui.trayIcon.Add("E&xit", fun () -> application.Shutdown())

        application.ShutdownMode <- ShutdownMode.OnExplicitShutdown
        application.Startup |> Event.add(fun _ -> mainWindowStartup application.MainWindow)
        application.Run(gui.mainWindow)


    [<STAThread>]
    [<EntryPoint>]
    let main argv =
        // Allow only one copy to be running
        //
        let mutable ok = false
        let str_mutex = WindowsIdentity.GetCurrent().Name.ToString().Split('\\').[1] + "rbcpsuite"
        let m = new Mutex(true, str_mutex, &ok)
        if ok then
            application_start()
        else
            MessageBox.Show("An instance of RBCPSuite is already running.  If you want to "+
                            "restart RBCPSuite, exit the running instance and start it again.",
                            "RBCPSuite") |> ignore
            1
