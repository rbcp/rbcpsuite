﻿namespace RBCPSuite

module Utils =
    open System.Collections.Specialized
    open System.Windows.Media

    // Sequences / Collections
    //
    module Seq =
        // variant of Seq.choose that also passes index to the predicate (like Seq.mapi)
        let choosei f =
            Seq.mapi f >> Seq.choose id

    let mapOfNameValueCollection (collection : NameValueCollection) =
        (Map.empty, collection.AllKeys)
        ||> Array.fold (fun map key ->
            let valueSet =
                match collection.[key] with
                | null -> ""
                | values -> values
            Map.add key valueSet map)

    // Color
    //
    let hsl_to_rgb h s l =
        let hue2rgb p q t =
            let t = match t with
                    | t when t < 0.0f -> t + 1.0f
                    | t when t > 1.0f -> t - 1.0f
                    | _ -> t
            match t with
            | t when t < 1.0f/6.0f -> p + (q - p) * 6.0f * t
            | t when t < 1.0f/2.0f -> q
            | t when t < 2.0f/3.0f -> p + (q - p) * (2.0f/3.0f - t) * 6.0f
            | _ -> p
        if s = 0.0f then
            Color.FromScRgb(1.0f, 1.0f, 1.0f, 1.0f) // achromatic
        else
            let q = if l < 0.5f then l * (1.0f + s) else l + s - l * s
            let p = 2.0f * l - q;
            Color.FromScRgb(1.0f,
                            hue2rgb p q (h + 1.0f/3.0f),
                            hue2rgb p q h,
                            hue2rgb p q (h - 1.0f/3.0f))
