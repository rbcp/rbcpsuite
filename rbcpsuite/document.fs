﻿namespace RBCPSuite

open System.Collections.Generic

module document =

    open System.ComponentModel
    open System.Collections.ObjectModel
    open System.Windows.Data

    type Document (name) =
        member this.name = name

    type MessagesDocument (name) =
        inherit Document(name)
        let msgs = new ObservableCollection<obj>()
        let _lock = new obj()
        do BindingOperations.EnableCollectionSynchronization(msgs, _lock)

        member this.messages = msgs

        member this.Add(msg:obj) =
            msgs.Add(msg)


    let documents = new Dictionary<string,Document>()

    let make_document (doc:Document) =
        documents.Add(doc.name, doc)
        doc
