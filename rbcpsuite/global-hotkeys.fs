﻿
namespace RBCPSuite

module GlobalHotkeys =
    open System
    open System.Runtime.InteropServices
    open System.Windows
    open System.Windows.Interop

    // OS level hotkey
    //
    [<DllImport("user32.dll")>]
    extern bool RegisterHotKey(IntPtr hWnd, int id, uint32 fsModifiers, uint32 vk);

    [<DllImport("user32.dll")>]
    extern bool UnregisterHotKey(IntPtr hWnd, int id);

    let HOTKEY_ID = 9000

    //Modifiers:
    let MOD_NONE = uint32 0x0000 //[NONE]
    let MOD_ALT = uint32 0x0001 //ALT
    let MOD_CONTROL = uint32 0x0002 //CTRL
    let MOD_SHIFT = uint32 0x0004 //SHIFT
    let MOD_WIN = uint32 0x0008 //WINDOWS
                   //CAPS LOCK:
    let VK_CAPITAL = uint32 0x14
    let VK_F12 = uint32 0x7b
    let VK_SCROLL = uint32 0x91

    let HwndHook (mainWindow:Window) hwnd msg wParam lParam (handled:bool byref) =
        let WM_HOTKEY = 0x0312
        if msg = WM_HOTKEY && (wParam:nativeint).ToInt32() = HOTKEY_ID then
            let vkey = ((int lParam) >>> 16) &&& 0xffff
            if mainWindow.IsActive then
                mainWindow.Hide()
            else
                mainWindow.Show()
                mainWindow.Activate() |> ignore
                mainWindow.WindowState <- WindowState.Normal
            handled <- true
            IntPtr.Zero
        else
            IntPtr.Zero

    let registerTheHotkey mainWindow =
        let wi = new WindowInteropHelper(mainWindow)
        let handle = wi.EnsureHandle()
        let source = HwndSource.FromHwnd(handle)
        source.AddHook(new HwndSourceHook(
                        fun hwnd msg wParam lParam (handled : bool byref) ->
                            HwndHook mainWindow hwnd msg wParam lParam &handled))
        RegisterHotKey(handle, HOTKEY_ID, MOD_NONE, VK_SCROLL)
