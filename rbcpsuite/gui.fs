﻿
namespace RBCPSuite

open System.Windows.Markup
open System.IO
open System.Xml
open System
open System.Diagnostics

module gui =
 
    open FParsec

    open System.Collections.Specialized
    open System.Reflection
    open System.Resources
    open System.Windows
    open System.Windows.Controls
    open System.Windows.Input
    open System.Windows.Media
    open System.Windows.Threading
    open Utils

    open document

    // Parsing
    //
    type WindowGeometry =
        int * int * int * int

    let windowGeometryParser =
        pipe4 (pfloat .>> spaces)
              (pfloat .>> spaces)
              (pfloat .>> spaces)
              (pfloat)
              (fun w h x y -> (w, h, x, y))

    let parseWindowGeometry s =
        match run windowGeometryParser s with
        | Success(result, _, _) -> result
        | Failure(errorMsg, e, s) -> failwith errorMsg

    // Key binding utils
    //
    type CommandCallThunk (thunk:(unit -> unit)) =
        let CanExecuteChangedEvent = new Event<_,_>()
        interface ICommand with
            member self.Execute (argument:obj) = thunk() |> ignore
            member self.CanExecute (argument:obj) = true
            [<CLIEvent>]
            member self.CanExecuteChanged =
                CanExecuteChangedEvent.Publish


    let xaml_to_datatemplate templatexaml =
        let sr = new StringReader("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'"+
                                  "              xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'"+
                                  "              xmlns:System='clr-namespace:System;assembly=mscorlib'"+
                                  "              xmlns:local='clr-namespace:rbcpsuite'>"+
                                  templatexaml+
                                  "</DataTemplate>")
        let xr = XmlReader.Create(sr)
        XamlReader.Load(xr) :?> System.Windows.DataTemplate


    // Buffer to View
    //
    type UnknownView (o) =
        inherit TextBlock ()
        do base.Text <- "[Undisplayable object of type: "+(o.GetType().FullName)+"]"

    let views = new System.Collections.Generic.Dictionary<Type, DataTemplate>()

    let AddDataView t v =
        views.Add(t, v)

    type TheSelector () =
        inherit DataTemplateSelector()
        member this.SelectTemplateFromType(itype) =
            match views.TryGetValue(itype) with
            | true, x -> x
            | _ ->
                let basetype = itype.BaseType
                if basetype = typeof<obj> then
                    null //XXX silent failure baaaad
                else
                    this.SelectTemplateFromType(basetype)

        override this.SelectTemplate(item, container) =
            let itype = item.GetType()
            this.SelectTemplateFromType(itype)


    // GUI Controls
    //
    type IDocumentView =
        abstract member name: string


    type ListenerView (doc:Document) as this =
        inherit ScrollViewer ()
        let mutable _height = this.ExtentHeight
        let ic = ItemsControl()
        do ic.ItemTemplateSelector <- TheSelector()
           ic.ItemsSource <- (doc :?> MessagesDocument).messages
           this.Content <- ic
           ic.Background <- new SolidColorBrush(Color.FromRgb(byte 20,byte 20,byte 20))
           ic.Foreground <- new SolidColorBrush(Color.FromRgb(byte 200,byte 200,byte 200))
           this.LayoutUpdated |> Event.add(fun _ ->
                                            //XXX testing height isn't a great test because
                                            //    we will be truncating the list.  also, we
                                            //    want to respect the user's manual scrolling.
                                            if this.ExtentHeight <> _height then
                                                this.ScrollToBottom()
                                                _height <- this.ExtentHeight)

        interface IDocumentView with
            member this.name = doc.name


    type UIWindow () as this =
        inherit Window ()
        let mutable messages = MessagesDocument("temp")
        let tabControl = TabControl()

        do base.Content <- tabControl
           base.Title <- "RBCPSuite"
        let res = ResourceManager("Resources", Assembly.GetExecutingAssembly())
        let icon = res.GetObject("RBCPSuiteIcon") :?> Drawing.Icon
        let icon_handle = icon.ToBitmap().GetHbitmap()
        let icon_source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                            icon_handle, IntPtr.Zero, Int32Rect.Empty,
                            System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions())
        do base.Icon <- icon_source
           let sec = System.Configuration.ConfigurationManager.GetSection("gui") :?> NameValueCollection
           if sec <> null then
               let config = mapOfNameValueCollection sec
               if config.ContainsKey("saveWindowGeometry") && config.Item("saveWindowGeometry") = "true" then
                   if config.ContainsKey("windowGeometry") then
                       let g = config.Item("windowGeometry")
                       let w, h, x, y = parseWindowGeometry g
                       base.Width <- w
                       base.Height <- h
                       base.Left <- x
                       base.Top <- y

           // hide window by default
           //
           this.Closing |> Event.add(fun evArgs ->
                                         evArgs.Cancel <- true
                                         this.Hide())
           this.StateChanged |> Event.add(fun e ->
                                            if this.WindowState = WindowState.Minimized then
                                               this.Hide())
           messages <- downcast make_document(MessagesDocument("Messages"))
           let listenerView = ListenerView(messages)
           this.addTab(listenerView)
           this.Loaded |> Event.add(fun _ -> this.Visibility <- Visibility.Hidden)

        member this.log (msg:obj) =
            messages.Add(msg) |> ignore
            //XXX handle truncating at 100

        member this.addTab (view:IDocumentView) =
            let tabItem = TabItem()
            tabItem.Header <- view.name
            tabItem.Content <- view
            tabControl.Items.Insert(tabControl.Items.Count, tabItem)


    type TrayIcon (window:Window) =
        let mainWindow = window
        let icon = new System.Windows.Forms.NotifyIcon()
        let contextMenu = new System.Windows.Forms.ContextMenu()
        let icon_Click (e:Forms.MouseEventArgs) =
            if e.Button = Forms.MouseButtons.Left then
                if mainWindow.IsVisible then
                    mainWindow.Hide()
                else
                    mainWindow.Show()
                    mainWindow.Activate() |> ignore
                    mainWindow.WindowState <- WindowState.Normal
        let res = ResourceManager("Resources", Assembly.GetExecutingAssembly())
        do icon.Icon <- res.GetObject("RBCPSuiteIcon") :?> Drawing.Icon
           icon.MouseClick |> Event.add(icon_Click)
           icon.Visible <- true
           icon.ContextMenu <- contextMenu

        member this.Add (text, onClick) =
            let item = new Forms.MenuItem()
            item.Text <- text
            item.Click |> Event.add(fun _ -> onClick())
            contextMenu.MenuItems.Add(item) |> ignore

        member this.Insert (text, onClick) =
            let item = new Forms.MenuItem()
            item.Text <- text
            item.Click |> Event.add(fun _ -> onClick())
            contextMenu.MenuItems.Add(0, item) |> ignore

        member this.InsertSeparator () =
            contextMenu.MenuItems.Add(0, new Forms.MenuItem("-")) |> ignore


    // GUI references
    //
    let mainWindow = UIWindow()
    let trayIcon = TrayIcon(mainWindow)

    let Init () =
        AddDataView typeof<string> (xaml_to_datatemplate "<TextBlock Text='{Binding}' />")
        AddDataView typeof<exn> (xaml_to_datatemplate "<TextBlock Foreground='Red' Text='{Binding Message}' />")
