﻿namespace RBCPSuite

module Plugin =

    open System
    open System.Collections.Generic

    type Plugin =
        { DigistarInit : (unit -> unit)
          MainWindowLoaded : (gui.UIWindow -> unit) }

    let defaultPlugin =
        { DigistarInit = ignore;
          MainWindowLoaded = ignore
        }

    let private plugins = new List<Plugin>()

    let DigistarInitHook () =
        plugins |> Seq.iter(fun p -> p.DigistarInit())

    let MainWindowLoadedHook (w:gui.UIWindow) =
        plugins |> Seq.iter(fun p -> p.MainWindowLoaded w)

    let Register p =
        plugins.Add(p)
